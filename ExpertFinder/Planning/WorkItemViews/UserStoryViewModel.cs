﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.Util;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScrumSupporter.Planning.WorkItemViews
{
    public class UserStoryViewModel : WorkItemViewModel
    {
        private UserStoryModel userStoryModel;

        public UserStoryViewModel(WorkItemModel model)
            :base(model)
        {
            userStoryModel = model as UserStoryModel;
        }

        public String Description
        {
            get
            {
                return userStoryModel.Description;
            }
            set
            {
                userStoryModel.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public double StoryPoints
        {
            get
            {
                return userStoryModel.StoryPoints;
            }
            set
            {
                userStoryModel.StoryPoints = value;
                NotifyOfPropertyChange(() => StoryPoints);
            }
        }

        public String Risk
        {
            get
            {
                return userStoryModel.Risk;
            }
            set
            {
                userStoryModel.Risk = value;
                NotifyOfPropertyChange(() => Risk);
            }
        }


        protected override Point CalculatePosition(Random random)
        {
            Point position = new Point();
            if (random != null)
            {
                position.X = random.Next((Constants.BACKLOGWIDTH / 2) + 100, (Constants.BACKLOGWIDTH - 100));
                position.Y = random.Next((Constants.BACKLOGHEIGHT / 2) + 100, (Constants.BACKLOGHEIGHT - 100));
                return position;
            }
            position.X = (Constants.BACKLOGWIDTH / 2) + 100;
            position.Y = (Constants.BACKLOGHEIGHT / 2) + 100;
            return position;
        }

        /// <summary>
        /// Looks up the DataTemplate in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplate()
        {
            return Application.Current.Resources["UserstoryViewModelTemplate"] as DataTemplate;
        }

        /// <summary>
        /// Looks up the DataTemplate for the singleIteration view in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplateForSingleIterationView()
        {
            return Application.Current.Resources["UserStoryViewModelTemplateForSingleIterationView"] as DataTemplate;
        }

        /// <summary>
        /// Creates a UserStoryView in a ScatterViewItem and sets itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public override ScatterViewItem GetView()
        {
            ScatterViewItem svi = GetScatterViewItemConfigsOfTheView();
            UserStoryView view = new UserStoryView();
            view.DataContext = this;
            //view.DisableAnalyzeButton();
            svi.Content = view;

            return svi;
        }

        protected override Style GetStyleOfCursorVisual()
        {
            return Application.Current.Resources["CursorStyleUserStoryBacklogView"] as Style;
        }

        protected override SurfaceUserControl GetSpecificEditingView()
        {
            return new UserStoryEditingView();
        }
    }
}
