﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.ComponentModel;
using System.Windows.Input;

namespace ScrumSupporter.LoadProject
{
    /// <summary>
    /// The view model of LoadProjectView. 
    /// </summary>
    public class LoadProjectViewModel : BaseViewModel
    {
        private LoadProjectView view;
        private EnterCredentialsViewModel TfsEnterCredViewModel;

        private bool isTFSSelected;

        public bool IsTFSSelected
        {
            get { return isTFSSelected; }
            set
            {
                isTFSSelected = value;
                TfsEnterCredViewModel.IsEnterDetailsVisible = isTFSSelected;
                NotifyOfPropertyChange(() => IsTFSSelected);
            }
        }

        public LoadProjectViewModel(Object parentDataContext)
        {
            TfsEnterCredViewModel = new EnterCredentialsViewModel(parentDataContext);
            IsTFSSelected = false;
            
        }

        public LoadProjectView CreateView()
        {
            var v = view ?? (view = new LoadProjectView());
            v.DataContext = this;
            v.TFSEnterDetails.DataContext = TfsEnterCredViewModel;

            return v;
        }

    }
}
