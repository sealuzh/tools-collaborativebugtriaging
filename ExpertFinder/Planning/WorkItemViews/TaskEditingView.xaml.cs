﻿using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using System;
using ScrumSupporter.Planning;
using System.IO;

namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Interaktionslogik für TaskEditingView.xaml
    /// </summary>
    public partial class TaskEditingView : SurfaceUserControl
    {
        public TaskEditingView()
        {
            InitializeComponent();
        }

        private void OnOpenTriagingView(object sender, RoutedEventArgs e)
        {
            WorkItemViewModel dataContext = this.DataContext as WorkItemViewModel;
            dataContext.OnOpenTriaging(FindResource("sviStyle") as Style);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            String strokesPath = "Resources/Strokes/" + (this.DataContext as TaskViewModel).Id + ".isf";
            StrokesHandler strokesHandler = new StrokesHandler(strokesPath, inkCanvas, SaveInkButton, EraseInkButton, BackButton);
            try
            {
                strokesHandler.LoadStrokes();
            }
            catch (FileLoadException e1)
            {
                System.Console.WriteLine(e1.Message);
            }

            OpenInkCanvasButton.Click += strokesHandler.OnOpenInkCanvas;
        }
    }
}
