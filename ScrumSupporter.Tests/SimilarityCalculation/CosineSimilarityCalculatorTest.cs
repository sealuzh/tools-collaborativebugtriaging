﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using System.Collections.ObjectModel;
using ScrumSupporter.Exceptions;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class CosineSimilarityCalculatorTest
    {
        private CosineSimilarityCalculator calulator;

        [TestFixtureSetUp]
        public void SetUp()
        {
            calulator = new CosineSimilarityCalculator();
        }

        private Collection<double> MockVector1()
        {
            Collection<double> vector1 = new Collection<double>() { 1, 2, 3, 4 };
            return vector1;
        }

        private Collection<double> MockVector2()
        {
            Collection<double> vector2 = new Collection<double>() { 5, 6, 7, 8 };
            return vector2;
        }

        private Collection<double> MockVector3()
        {
            Collection<double> vector3 = new Collection<double>() { 1, 2, 3, 4, 5 };
            return vector3;
        }

        [Test]
        public void CalculateCosineSimilarityTest()
        {
            double resultingSimilarity = calulator.CalculateCosineSimilarity(MockVector1(), MockVector2());
            resultingSimilarity = Math.Round(resultingSimilarity, 2);

            double expected = (79 / (Math.Sqrt(30) * Math.Sqrt(219)));
            expected = Math.Round(expected, 2);

            Assert.AreEqual(expected, resultingSimilarity);
        }

        [Test]
        public void CalculateCosineSimilarityVector1NullTest()
        {
            Assert.Throws(typeof(ArgumentNullException), new TestDelegate(CreateVector1NullCosineSimilarity));
        }

        private void CreateVector1NullCosineSimilarity()
        {
            calulator.CalculateCosineSimilarity(null, MockVector3());
        }

        [Test]
        public void CalculateCosineSimilarityVector2NullTest()
        {
            Assert.Throws(typeof(ArgumentNullException), new TestDelegate(CreateVector2NullCosineSimilarity));
        }

        private void CreateVector2NullCosineSimilarity()
        {
            calulator.CalculateCosineSimilarity(MockVector1(), null);
        }


        [Test]
        public void CalculateCosineSimilarityExceptionTest()
        {
            Assert.Throws(typeof(InnerVectorProductException), new TestDelegate(ExceptionTest));
        }

        private void ExceptionTest()
        {
            calulator.CalculateCosineSimilarity(MockVector1(), MockVector3());
        }
    }
}
