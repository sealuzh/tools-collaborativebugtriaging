﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.CollaborationPlatform.Exceptions
{
    /// <summary>
    /// This exception is thrown if an invalid work item ID is provided.
    /// </summary>
    class InvalidWorkItemIdException: ApplicationException
    {
        public InvalidWorkItemIdException() { }
        public InvalidWorkItemIdException(string message): base(message) { }
        public InvalidWorkItemIdException(string message, System.Exception inner):base(message, inner) { }

        // Constructor needed for serialization 
        protected InvalidWorkItemIdException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context):base(info, context) { }
    }
}
