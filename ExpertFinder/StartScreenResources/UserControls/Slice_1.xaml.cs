﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media.Animation;


namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// Interaktionslogik für Slice_1.xaml
    /// </summary>
    public partial class Slice_1 : SurfaceUserControl
    {
        public Slice_1()
        {
            InitializeComponent();
        }


        private void Slice_1_ContactHoldGesture(object sender, ContactEventArgs e)
        {
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sb_Completed);
            Animation animation = new Animation();
            sb = animation.getSliceAnimation(slice1, text1, sb);

            slice1.BeginStoryboard(sb);
            
        }


        public void sb_Completed(object sender, EventArgs e) 
        {
            System.Console.WriteLine("Completed");
        }
    }
}
