﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using TfsLink;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.CollaborationPlatform.Exceptions;
using ScrumSupporter.Exceptions;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Implements the WorkItemService of the Team Foundation Server.
    /// </summary>
    public class TFSWorkItemService: IWorkItemService
    {
        private ITfs tfsConnection;

        public TFSWorkItemService(ITfs TfsConnection)
        {
            this.tfsConnection = TfsConnection;
        }


        public WorkItemModel GetWorkItem(int id)
        {

            WorkItem item = tfsConnection.GetWorkItem(id);
            if (item != null)
            {
                return AdaptToWorkItemModel(item);
            }
            else
            {
                throw new InvalidWorkItemIdException();
            }
            
        }

        public ICollection<WorkItemModel> GetAllWorkItems()
        {
            ICollection<WorkItemModel> allWorkItems = new List<WorkItemModel>();

            ICollection<WorkItem> workItems = tfsConnection.GetAllWorkItems();

            foreach (WorkItem w in workItems)
            {
                allWorkItems.Add(AdaptToWorkItemModel(w));
            }
            return allWorkItems;
        }

        public ICollection<WorkItemModel> GetWorkItemsAssignedTo(string developersName)
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();
            ICollection<WorkItem> assignedWorkItems = tfsConnection.GetAssignedWorkItems(developersName);
            foreach (WorkItem w in assignedWorkItems)
            {
                items.Add(AdaptToWorkItemModel(w));
            }
            return items;
        }

        public bool UploadWorkItems(ICollection<WorkItemModel> workItems)
        {
            if (workItems != null)
            {
                ICollection<WorkItem> workItemsToUpload = new List<WorkItem>();
                foreach (WorkItemModel model in workItems)
                {
                    workItemsToUpload.Add(AdaptToTFSWorkItem(model));
                }

                bool success = tfsConnection.UploadWorkItems(workItemsToUpload);
                return success;
            }
            else return false;
        }

        public ICollection<WorkItemModel> GetWorkItemsOfIteration(string iterationName)
        {
            ICollection<WorkItemModel> workItemsOfIteration = new List<WorkItemModel>();

            ICollection<WorkItem> workItems = tfsConnection.GetWorkItemsOfIteration(iterationName);

            foreach (WorkItem workitem in workItems)
            {
                workItemsOfIteration.Add(AdaptToWorkItemModel(workitem));
            }

            return workItemsOfIteration;
        }

        #region adapt to WorkItemModel
        private WorkItemModel AdaptToWorkItemModel(WorkItem w)
        {
            if (w.Type.Name.Equals("Bug"))
            {
                BugModel bug = new BugModel(){Title=w.Title, Area = w.AreaPath, ChangedDate = w.ChangedDate, Id = w.Id, 
                StepsToReproduce =GetReproSteps(w), Severity = GetSeverity(w), AssignedTo = GetAssignedTo(w), Iteration = w.IterationPath,
                Reason = w.Reason, State = w.State, Priority = GetPriority(w), ResolvedReason = GetResolvedReason(w), StackRank = GetStackRank(w)};
                return bug;
            }
            else if (w.Type.Name.Equals("User Story"))
            {
                UserStoryModel userStory = new UserStoryModel() {Title =w.Title, Risk = GetRisk(w), StoryPoints= GetStoryPoints(w), StackRank= GetStackRank(w),
                    Area = w.AreaPath, ChangedDate = w.ChangedDate, Id = w.Id, Reason = w.Reason, State = w.State, Description = w.Description,
                    AssignedTo = GetAssignedTo(w), Iteration = w.IterationPath
                };

                return userStory;
            }
            else if (w.Type.Name.Equals("Issue"))
            {
                IssueModel issue = new IssueModel() {Title = w.Title, DueDate= GetDueDate(w), Priority= GetPriority(w), StackRank = GetStackRank(w), AssignedTo = GetAssignedTo(w),
                Area = w.AreaPath, ChangedDate= w.ChangedDate, Description = w.Description, State = w.State, Id = w.Id, Iteration = w.IterationPath, Reason = w.Reason};

                return issue;
            }
            else if (w.Type.Name.Equals("Task"))
            {
                TaskModel task = new TaskModel() { Title = w.Title, Completed = GetCompleted(w), Remaining= GetRemainingWork(w), OriginalEstimate = GetOriginalEstimate(w),
                Iteration = w.IterationPath, Activity = GetActivity(w), Area = w.AreaPath, AssignedTo = GetAssignedTo(w), ChangedDate = w.ChangedDate,
                Id = w.Id, Description = w.Description, Priority = GetPriority(w), Reason = w.Reason, StackRank = GetStackRank(w), State = w.State};

                return task;
            }
            else
            {
                throw new WorkItemTypeNotFoundException("WorkItem Type is not recognized");
            }
        }

        private String GetAssignedTo(WorkItem w)
        {
            String assignedTo = "";
            if (w.Fields["Assigned To"].Value != null)
            {
                assignedTo = (String)w.Fields["Assigned To"].Value;
            }
            return assignedTo;
        }

        private Double GetStackRank(WorkItem w)
        {
            double rank = 0;
            if (w.Fields["Stack Rank"].Value != null)
            {
                rank = (Double)w.Fields["Stack Rank"].Value;
            }
            return rank;
        }

        private String GetResolvedReason(WorkItem w)
        {
            String resolvedReason = "";
            if (w.Fields["Resolved Reason"].Value != null)
            {
                resolvedReason = (String)w.Fields["Resolved Reason"].Value;
            }
            return resolvedReason;
        }

        private int GetPriority(WorkItem w)
        {
            int priority = 0;
            if (w.Fields["Priority"].Value != null)
            {
                priority = (int)w.Fields["Priority"].Value;
            }
            return priority;
        }

        private String GetSeverity(WorkItem w)
        {
            String severity = "";
            if (w.Fields["Severity"].Value != null)
            {
                severity = (String)w.Fields["Severity"].Value;
            }
            return severity;
        }

        private String GetReproSteps(WorkItem w)
        {
            String steps = "";
            if (w.Fields["Repro Steps"].Value != null)
            {
                steps = (String)w.Fields["Repro Steps"].Value;
            }
            String stepsWithoutHTMLTag = "";
            string[] stepsWithoutHTMLTags = Regex.Split(steps, "&nbsp;");
            foreach (String s in stepsWithoutHTMLTags)
            {
                stepsWithoutHTMLTag += s;
            }
            return stepsWithoutHTMLTag;
        }

        private double GetStoryPoints(WorkItem w)
        {
            double points = 0;
            if (w.Fields["Story Points"].Value != null)
            {
                points = (double)w.Fields["Story Points"].Value;
            }
            return points;
        }

        private String GetRisk(WorkItem w)
        {
            String risk = "";
            if (w.Fields["Risk"].Value != null)
            {
                risk = (String)w.Fields["Risk"].Value;
            }
            return risk;
        }

        private DateTime GetDueDate(WorkItem w)
        {
            DateTime dueDate = new DateTime();
            if (w.Fields["Due Date"].Value != null)
            {
                dueDate = (DateTime)w.Fields["Due Date"].Value;
            }
            return dueDate;
        }

        private String GetActivity(WorkItem w)
        {
            String activity = "";
            if (w.Fields["Activity"].Value != null)
            {
                activity = (String)w.Fields["Activity"].Value;
            }
            return  activity;
        }

        private double GetOriginalEstimate(WorkItem w)
        {
            double originalEstimate = 0;
            if (w.Fields["Original Estimate"].Value != null)
            {
                originalEstimate = (double)w.Fields["Original Estimate"].Value;
            }
            return originalEstimate;
        }

        private double GetRemainingWork(WorkItem w)
        {
            double remaining = 0;
            if (w.Fields["Remaining Work"].Value != null)
            {
                remaining = (double)w.Fields["Remaining Work"].Value;
            }
            return remaining;
        }

        private double GetCompleted(WorkItem w)
        {
            double completed = 0;
            if (w.Fields["Completed Work"].Value != null)
            {
                completed = (double)w.Fields["Completed Work"].Value;
            }
            return completed;
        }
        #endregion

        private WorkItem AdaptToTFSWorkItem(WorkItemModel model)
        {
            WorkItemStore workItemStore = tfsConnection.GetWorkItemStore();
            Project teamProject = workItemStore.Projects["MP1201"];

            if (model.Id == 0)
            {
                WorkItem w = model.AdaptNewWorkItemToTfs(teamProject);
                return w;
            }
            else
            {
                WorkItem workItem = tfsConnection.GetWorkItem(model.Id);
                WorkItem w = model.AdaptExistingWorkItemToTfs(workItem);
                return w;
            }
        }


        public ICollection<WorkItemModel> GetBacklogWorkItems()
        {
            ICollection<WorkItemModel> backlogWorkItems = new List<WorkItemModel>();

            ICollection<WorkItem> workItems = tfsConnection.GetBacklogWorkItems();

            foreach (WorkItem w in workItems)
            {
                backlogWorkItems.Add(AdaptToWorkItemModel(w));
            }
            return backlogWorkItems;
        }


        public ICollection<WorkItemModel> GetWorkItemsContainingString(string queryString)
        {
            ICollection<WorkItemModel> possibleWorkItems = new List<WorkItemModel>();
            ICollection<WorkItem> workItems = tfsConnection.GetWorkItemsContainingStringInTitle(queryString);

            foreach (WorkItem w in workItems)
            {
                possibleWorkItems.Add(AdaptToWorkItemModel(w));
            }

            return possibleWorkItems;
        }
    }
}
