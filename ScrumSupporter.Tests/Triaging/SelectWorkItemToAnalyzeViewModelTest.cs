﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Moq;
using ScrumSupporter.Triaging;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform.Exceptions;
using ScrumSupporter.Planning.WorkItemViews;
using ScrumSupporter.Planning;


namespace ScrumSupporter.Tests.Triaging
{
    [TestFixture]
    public class SelectWorkItemToAnalyzeViewModelTest
    {
        private SelectWorkItemToAnalyzeViewModel viewModel;
        private SelectWorkItemToAnalyzeViewModel unavailableViewModel;
        private ICollaborationPlatform platform;
        private ICollaborationPlatform unavailablePlatform;
        private WorkItemModel wi;

        [TestFixtureSetUp]
        public void SetUp()
        {
            platform = CreateMockCollaborationPlatform();
            unavailablePlatform = CreateUnavailableMockCollaborationPlatform();
            viewModel = new SelectWorkItemToAnalyzeViewModel(platform);
            unavailableViewModel = new SelectWorkItemToAnalyzeViewModel(unavailablePlatform);

        }

        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            mockCollaborationPlatform.Setup(m => m.IsCollaborationPlatformAvailable()).Returns(true);

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetWorkItem(1)).Returns(GetMockedWorkItem());
            mockWorkItemService.Setup(m => m.GetWorkItemsContainingString(It.IsAny<String>())).Returns(GetMockedWorkItems());

            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);


            return mockCollaborationPlatform.Object;
        }

        private ICollaborationPlatform CreateUnavailableMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            mockCollaborationPlatform.Setup(m => m.IsCollaborationPlatformAvailable()).Returns(false);

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetWorkItem(1)).Returns(GetMockedWorkItem());
            mockWorkItemService.Setup(m => m.GetWorkItemsContainingString(It.IsAny<String>())).Returns(GetMockedWorkItems());

            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);


            return mockCollaborationPlatform.Object;
        }

        private WorkItemModel GetMockedWorkItem()
        {
            wi = new BugModel() {Title = "this is the title of the bug", StepsToReproduce = "These are the steps to reproduce", Id = 1 };
            return wi;
        }

        private ICollection<WorkItemModel> GetMockedWorkItems()
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();
            BugModel bug = new BugModel();
            items.Add(bug);
            TaskModel task = new TaskModel();
            items.Add(task);
            return items;
        }

        [Test]
        public void FindWorkItemByIdTest()
        {
            WorkItemModel result = viewModel.FindWorkItemById("1");
            Assert.AreEqual(wi, result);
        }

        [Test]
        public void FindWorkItemByIdExceptionTest()
        {
            Assert.Throws(typeof(CollaborationPlatformUnavailableException), new TestDelegate(GenerateExceptionFindWorkItemById));
        }

        private void GenerateExceptionFindWorkItemById()
        {
            unavailableViewModel.FindWorkItemById("1");
        }

        [Test]
        public void SetPossibleWorkItemsTest()
        {
            viewModel.SetPossibleWorkItems("queryString", null);
            Assert.AreEqual(2, viewModel.WorkItemsToSelect.Count);

            Assert.IsInstanceOf(typeof(BugViewModel), viewModel.WorkItemsToSelect.First());
            Assert.IsInstanceOf(typeof(TaskViewModel), viewModel.WorkItemsToSelect.Last());
        }

        [Test]
        public void SetPossibleWorkItemsExceptionTest()
        {
            Assert.Throws(typeof(CollaborationPlatformUnavailableException), new TestDelegate(GenerateExceptionSetPossibleWorkItems));
        }

        private void GenerateExceptionSetPossibleWorkItems()
        {
            unavailableViewModel.SetPossibleWorkItems("queryString", null);
        }

        [Test]
        public void ArePossibleWorkItemsFoundTest()
        {
            viewModel.WorkItemsToSelect.Clear();
            Assert.AreEqual(false, viewModel.ArePossibleWorkItemsFound());

            WorkItemModel model = new BugModel();
            WorkItemViewModel wiViewModel = new BugViewModel(model);

            viewModel.WorkItemsToSelect.Add(wiViewModel);
            Assert.AreEqual(true, viewModel.ArePossibleWorkItemsFound());
        }



    }
}
