﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface.Presentation.Manipulations;
using System.Windows;

namespace ScrumSupporter.Util
{
    public class ManipulationsUtil
    {

        public Affine2DInertiaProcessor GetInertiaProcessor(Thickness bounds, Affine2DOperationCompletedEventArgs e)
        {
            if (e != null)
            {
                if (bounds != null)
                {
                    Affine2DInertiaProcessor inertiaProcessor = new Affine2DInertiaProcessor();
                    inertiaProcessor.InitialOrigin = e.ManipulationOrigin;

                    inertiaProcessor.DesiredAngularDeceleration = .0001;
                    inertiaProcessor.DesiredDeceleration = .0001;
                    inertiaProcessor.DesiredExpansionDeceleration = .0010;
                    inertiaProcessor.Bounds = bounds;
                    inertiaProcessor.ElasticMargin = new Thickness(20);

                    inertiaProcessor.InitialVelocity = e.Velocity;
                    inertiaProcessor.InitialExpansionVelocity = e.ExpansionVelocity;
                    inertiaProcessor.InitialAngularVelocity = e.AngularVelocity;

                    return inertiaProcessor;
                }
                else throw new ArgumentNullException("bounds", "is null");
            }
            else throw new ArgumentNullException("e","is null");
        }
    }
}
