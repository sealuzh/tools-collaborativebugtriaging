﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.Profile
{
    /// <summary>
    /// Interaktionslogik für FlipItem.xaml
    /// </summary>
    public partial class FlipItem : SurfaceUserControl
    {
        public FlipItem()
        {
            InitializeComponent();
        }


        // Set the "back" to invisible.
        // You need to remove the back side  from the visual tree otherwise you will see a perf hit
        // when the front  side of the SVI is resized and WPF has to also calculate the layout for the back  elements.

        protected override void OnInitialized(EventArgs e)
        {
            backsideGrid.Visibility = Visibility.Hidden;
            containerGrid.Children.Remove(backsideGrid);
            base.OnInitialized(e);
        }

        /// <summary>
        /// Flip album to back side.
        /// </summary>
        private void SwitchToBack(object sender, EventArgs e)
        {
            Storyboard goToBack = this.FindResource("GoToBackStoryboard") as Storyboard;
            if (goToBack != null)
            {
                goToBack.Begin(this);
            }

            UpdateUIState(false);
        }

        /// <summary>
        /// Filp album to front side.
        /// </summary>
        private void SwitchToFront(object sender, EventArgs e)
        {
            Storyboard goToFront = this.FindResource("GoToFrontStoryboard") as Storyboard;
            if (goToFront != null)
            {
                goToFront.Begin(this);
            }

            UpdateUIState(true);
        }

        /// <summary>
        /// Update UI status on this control.
        /// </summary>
        /// <param name="isFront">if true, controls on front side are updated. if false, controls on back side are updated.</param>
        private void UpdateUIState(bool isFront)
        {
            if (isFront)
            {
                frontsideGrid.Visibility = Visibility.Visible;
                backsideGrid.Visibility = Visibility.Hidden;

                // To improve performance, remove backsideGrid from Visual tree.
                containerGrid.Children.Remove(backsideGrid);
            }
            else
            {
                // Add the backside back to the SVI
                containerGrid.Children.Add(backsideGrid);
                backsideGrid.Visibility = Visibility.Visible;
                frontsideGrid.Visibility = Visibility.Hidden;
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }


    }
}
