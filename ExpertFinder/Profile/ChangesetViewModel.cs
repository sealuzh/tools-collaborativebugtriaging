﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using ExpertFinder.Model;

namespace ExpertFinder.Profile
{
    public class ChangesetViewModel: BaseViewModel
    {


        private Changeset_Local model;

        public ChangesetViewModel(Changeset_Local model)
        {
            this.model = model;
        }

        public int Id
        {
            get
            {
                return model.Id;
            }
            set
            {
                model.Id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public String Comment
        {
            get
            {
                return model.Comment;
            }
            set
            {
                model.Comment = value;
                NotifyOfPropertyChange(() => Comment);
            }
        }

        public String Committer
        {
            get
            {
                return model.Committer;
            }
            set
            {
                model.Committer = value;
                NotifyOfPropertyChange(() => Committer);
            }
        }

        public DateTime CreationDate
        {
            get
            {
                return model.CreationDate;
            }
            set
            {
                model.CreationDate = value;
                NotifyOfPropertyChange(() => CreationDate);
            }
        }


    }
}
