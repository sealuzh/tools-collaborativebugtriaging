﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Planning;
using System.IO;


namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Interaktionslogik für UserStoryEditingView.xaml
    /// </summary>
    public partial class UserStoryEditingView : SurfaceUserControl
    {
        public UserStoryEditingView()
        {
            InitializeComponent();
        }

        private void OnOpenTriagingView(object sender, RoutedEventArgs e)
        {
            WorkItemViewModel dataContext = this.DataContext as WorkItemViewModel;
            dataContext.OnOpenTriaging(FindResource("sviStyle") as Style);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            String strokesPath = "Resources/Strokes/" + (this.DataContext as UserStoryViewModel).Id + ".isf";
            StrokesHandler strokesHandler = new StrokesHandler(strokesPath, inkCanvas, SaveInkButton, EraseInkButton, BackButton);
            try
            {
                strokesHandler.LoadStrokes();
            }
            catch (FileLoadException e1)
            {
                System.Console.WriteLine(e1.Message);
            }

            OpenInkCanvasButton.Click += strokesHandler.OnOpenInkCanvas;
        }
    }
}
