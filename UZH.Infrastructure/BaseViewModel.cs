// --------------------------------------------------------------------------------------------------------------------- 
// <summary>
//   Defines the BaseViewModel class.
// </summary>
// ---------------------------------------------------------------------------------------------------------------------

#region Change History
// YYYY-MM-DD Author: Change description
#endregion

using System.ComponentModel;
using System.Linq.Expressions;
using System;

namespace UZH.Infrastructure
{
   public class BaseViewModel : INotifyPropertyChanged
   {
      #region Fields

      #endregion

      #region Constructors

      #endregion

      #region Events
      public event PropertyChangedEventHandler PropertyChanged;
      #endregion

      #region Enums

      #endregion

      #region Properties
      public static bool SuspendPropertyNotifications { get; set; }
      #endregion

      #region Public Methods
      public virtual void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property)
      {
         var name = GetName(property);
         NotifyOfPropertyChange(name);
      }

      public virtual void NotifyOfPropertyChange(object sender, string name)
      {
         if (SuspendPropertyNotifications) return;

         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
         }
      }

      public virtual void NotifyOfPropertyChange(string name)
      {
         NotifyOfPropertyChange(null, name);
      }

      public virtual void Refresh()
      {
         NotifyOfPropertyChange(null, string.Empty);
      }

      public string GetName<TProperty>(Expression<Func<TProperty>> property)
      {
         var lambda = (LambdaExpression)property;

         MemberExpression memberExpression;
         if (lambda.Body is UnaryExpression)
         {
            var unaryExpression = (UnaryExpression)lambda.Body;
            memberExpression = (MemberExpression)unaryExpression.Operand;
         }
         else
            memberExpression = (MemberExpression)lambda.Body;

         return memberExpression.Member.Name;
      }
      #endregion

      #region Internal Methods

      #endregion

      #region Protected Methods

      #endregion

      #region Private Methods

      #endregion
   }
}
