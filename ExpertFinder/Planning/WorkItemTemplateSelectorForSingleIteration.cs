﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;

namespace ScrumSupporter.Planning
{
    public class WorkItemTemplateSelectorForSingleIteration : DataTemplateSelector
    {
        /// <summary>
        /// Looks up the DataTemplate for the single iteration view depending on the work item type.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return (item as WorkItemViewModel).GetTemplateForSingleIterationView();
        }
    }
}
