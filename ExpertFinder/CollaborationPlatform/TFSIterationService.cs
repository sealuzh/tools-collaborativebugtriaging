﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using TfsLink;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Implements the IterationService of the Team Foundation Server.
    /// </summary>
    public class TFSIterationService : IIterationService
    {
        private ITfs tfsConnection;

        public TFSIterationService(ITfs con)
        {
            this.tfsConnection = con;
        }

        public ICollection<string> GetAllIterationNames()
        {
            ICollection<string> iterationNames = new List<string>();
            IList<Node> iterations = tfsConnection.GetIterations();

            foreach (Node node in iterations)
            {
                iterationNames.Add(node.Path);
            }

            return iterationNames;
        }
    }
}
