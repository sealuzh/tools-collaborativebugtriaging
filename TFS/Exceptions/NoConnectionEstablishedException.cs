﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TfsLink.Exceptions
{
    /// <summary>
    /// This Exception is thrown when the connection to the TFS is not established.
    /// </summary>
    [Serializable]
    public class NoConnectionEstablishedException : System.ApplicationException
    {
        public NoConnectionEstablishedException(): base() { }
        public NoConnectionEstablishedException(string message): base(message) { }
        public NoConnectionEstablishedException(string message, System.Exception inner): base(message, inner) { }

        // Constructor needed for serialization 
        protected NoConnectionEstablishedException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context): base(info, context) { }

    }
}
