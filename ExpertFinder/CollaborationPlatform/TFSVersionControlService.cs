﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Collections.ObjectModel;
using TfsLink;
using ScrumSupporter.Model;
using ScrumSupporter.Triaging;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Implements the VersionControlService of the Team Foundation Server.
    /// </summary>
    public class TFSVersionControlService: IVersionControlService
    {

        private ITfs tfsConnection;

        public TFSVersionControlService(ITfs TfsConnection)
        {
            this.tfsConnection = TfsConnection;
        }

        /// <summary>
        /// Gets all ChangesetsModel of a Work item which is identified by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ICollection<ChangesetModel> GetChangesetsOfWorkItem(int id)
        {
            ICollection<ChangesetModel> changesetModels = new List<ChangesetModel>();

            foreach (Changeset changeset in tfsConnection.GetChangesetsOfWorkItem(id))
            {
                changesetModels.Add(AdaptToChangesetModel(changeset));
            }
            return changesetModels;
        }

        /// <summary>
        /// Adapts the Changeset of the TFS into the ChangesetModel.
        /// </summary>
        /// <param name="changeset"></param>
        /// <returns>The converetd ChangesetModel.</returns>
        private ChangesetModel AdaptToChangesetModel(Changeset changeset)
        {
            ChangesetModel model = new ChangesetModel() {Comment = changeset.Comment, Committer = changeset.Committer,
            Id = changeset.ChangesetId, CreationDate = changeset.CreationDate};

            foreach (Change ch in changeset.Changes)
            {
                if(ch.Item.ServerItem.EndsWith(".cs")){
                    int index = ch.Item.ServerItem.LastIndexOf("/");
                    String className = ch.Item.ServerItem.Substring(index+1);
                    ChangeViewModel changeName = new ChangeViewModel();
                    changeName.ClassName = className;
                    model.changes.Add(changeName);
                }
            }

            return model;
        }


        public ICollection<ChangesetModel> GetChangesetsOfDeveloper(string name)
        {
            ICollection<ChangesetModel> changesetModels = new List<ChangesetModel>();
            ICollection<Changeset> changesets = tfsConnection.GetAllChangesetsFromDeveloper(name);

            foreach (Changeset ch in changesets)
            {
                changesetModels.Add(AdaptToChangesetModel(ch));
            }

            return changesetModels; 
        }


        public string GetChangesOfChangeset(int id)
        {
            String changes = tfsConnection.GetChangesOfChangeset(id);
            return changes;
        }


        public IList<String> DownloadCurrentAndPreviousVersion(int changesetId, string className)
        {
            return tfsConnection.DownloadCurrAndPreviousFile(changesetId, className);
        }




        public string DownloadFile(int changesetId, string className)
        {
            return tfsConnection.DownloadFile(changesetId, className);
        }
    }
}
