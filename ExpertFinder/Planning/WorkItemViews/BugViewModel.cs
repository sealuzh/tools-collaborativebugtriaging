﻿using System;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;
using ScrumSupporter.Util;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScrumSupporter.Planning.WorkItemViews
{
    public class BugViewModel : WorkItemViewModel
    {
        BugModel bugModel;

        public BugViewModel(WorkItemModel model)
            : base(model)
        {
            bugModel = model as BugModel;
        }

        public String ResolvedReason
        {
            get
            {
                return bugModel.ResolvedReason;
            }
            set
            {
                bugModel.ResolvedReason = value;
                NotifyOfPropertyChange(() => ResolvedReason);
            }
        }

        public String Severity
        {
            get
            {
                return bugModel.Severity;
            }
            set
            {
                bugModel.Severity = value;
                NotifyOfPropertyChange(() => Severity);
            }
        }

        public String StepsToReproduce
        {
            get
            {
                return bugModel.StepsToReproduce;
            }
            set
            {
                bugModel.StepsToReproduce = value;
                NotifyOfPropertyChange(() => StepsToReproduce);
            }
        }

        public int Priority
        {
            get
            {
                return bugModel.Priority;
            }
            set
            {
                bugModel.Priority = value;
                NotifyOfPropertyChange(() => Priority);
            }
        }


        protected override Point CalculatePosition(Random random)
        {
            Point position = new Point();
            if (random != null)
            {
                position.X = random.Next(100, (Constants.BACKLOGWIDTH / 2) - 100);
                position.Y = random.Next(100, (Constants.BACKLOGHEIGHT / 2) - 100);
                return position;
            }
            position.X = 100;
            position.Y = 100;
            return position;
        }


        /// <summary>
        /// Looks up the DataTemplate in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplate()
        {
            return Application.Current.Resources["BugViewModelTemplate"] as DataTemplate;
        }

        /// <summary>
        /// Looks up the DataTemplate for the singleIteration view in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplateForSingleIterationView()
        {
            return Application.Current.Resources["BugViewModelTemplateForSingleIterationView"] as DataTemplate;
        }

        /// <summary>
        /// Creates a BugView in a ScatterViewItem and sets itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public override ScatterViewItem GetView()
        {
            ScatterViewItem svi = GetScatterViewItemConfigsOfTheView();
            BugView view = new BugView();
            //view.DisableAnalyzeButton();
            view.DataContext = this;
            svi.Content = view;

            return svi;
        }

        protected override Style GetStyleOfCursorVisual()
        {
            return Application.Current.Resources["CursorStyleBugBacklogView"] as Style;
        }

        protected override SurfaceUserControl GetSpecificEditingView()
        {
            return new BugEditingView();
        }
    }
}
