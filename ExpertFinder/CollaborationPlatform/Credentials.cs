﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Credentials to log in to the collaboration platform.
    /// </summary>
    public class Credentials
    {
        public Credentials(String um, String pw, String dom, String u, String pName)
        {
            Username = um;
            Password = pw;
            Domain = dom;
            Uri = u;
            ProjectName = pName;
        }

        private String username;

        public String Username
        {
            get { return username; }
            set { username = value; }
        }
        private String password;

        public String Password
        {
            get { return password; }
            set { password = value; }
        }
        private String domain;

        public String Domain
        {
            get { return domain; }
            set { domain = value; }
        }

        private String uri;

        public String Uri
        {
            get { return uri; }
            set { uri = value; }
        }

        private String projectName;

        public String ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }


    }
}
