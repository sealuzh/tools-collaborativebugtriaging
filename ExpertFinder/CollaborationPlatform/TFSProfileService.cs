﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TfsLink;
using ScrumSupporter.Model;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Implements the ProfileService of the Team Foundation Server.
    /// </summary>
    public class TFSProfileService: IProfileService
    {

        private ITfs tfsConnection;

        public TFSProfileService(ITfs TfsConnection)
        {
            this.tfsConnection = TfsConnection;
        }



        public ICollection<ProfileModel> GetAllProfiles()
        {
            ICollection<ProfileModel> profiles = new List<ProfileModel>();

            IList<String> developersNames = tfsConnection.GetPersons();

            foreach (String name in developersNames)
            {
                ProfileModel profile = new ProfileModel() { TfsAccountName = name};
                profiles.Add(profile);
            }


            return profiles;
        }
    }
}
