﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Collections.ObjectModel;
using ExpertFinder.Model;

namespace ExpertFinder.Profile
{
    public class UpcomingEventsContainerViewModel: BaseViewModel
    {

        public ObservableCollection<UpcomingEventViewModel> UpcomingEvents { get; set; }
        private UpcomingEventsContainerView view;

        public UpcomingEventsContainerViewModel()
        {
            UpcomingEvents = new ObservableCollection<UpcomingEventViewModel>();
        }

        public void LoadEvents(Collection<UpcomingEventViewModel> events)
        {
            foreach (var e in events)
            {
                UpcomingEvents.Add(e);
            }
        }

        public UpcomingEventsContainerView CreateView()
        {
            view = new UpcomingEventsContainerView();
            view.DataContext = this;
            return view;
        }


    }
}
