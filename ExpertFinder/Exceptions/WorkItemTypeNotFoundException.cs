﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.Exceptions
{
    /// <summary>
    /// This exception is thrown when checking for an unkown work item type.
    /// </summary>
    [Serializable]
    public class WorkItemTypeNotFoundException: System.Exception
    {
        public WorkItemTypeNotFoundException() { }
        public WorkItemTypeNotFoundException(string message):base(message) { }
        public WorkItemTypeNotFoundException(string message, System.Exception inner) :base(message, inner){ }

        // Constructor needed for serialization 
        protected WorkItemTypeNotFoundException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context):base(info, context) { }
    }
}
