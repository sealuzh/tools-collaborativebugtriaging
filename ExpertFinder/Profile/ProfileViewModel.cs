﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Collections.ObjectModel;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter.Profile
{
    /// <summary>
    /// The ViewModel of the ProfileView. 
    /// </summary>
    public class ProfileViewModel : BaseViewModel
    {

        private ProfileModel model;

        private Dictionary<WorkItemModel, int> connectedWorkItems;

        public Dictionary<WorkItemModel, int> ConnectedWorkItems
        {
            get { return connectedWorkItems; }
        }

        private ObservableCollection<ChangesetModel> associatedChangesets = new ObservableCollection<ChangesetModel>();

        private ObservableCollection<WorkItemViewModel> assignedWorkItems = new ObservableCollection<WorkItemViewModel>();
 

        private int assignedWorkItemsCount;

        public int AssignedWorkItemsCount
        {
            get { return assignedWorkItemsCount; }
            set
            {
                assignedWorkItemsCount = value;
                NotifyOfPropertyChange(() => AssignedWorkItemsCount);
            }
        }

        private int associatedChangesetsCount;

        public int AssociatedChangesetsCount
        {
            get { return associatedChangesetsCount; }
            set
            {
                associatedChangesetsCount = value;
                NotifyOfPropertyChange(() => AssociatedChangesetsCount);
            }
        }

        public ProfileModel GetModel()
        {
            return model;
        }


        public ProfileViewModel(ProfileModel profileModel, ICollaborationPlatform platform)
        {
            if (platform != null)
            {
                if (profileModel != null)
                {
                    this.model = profileModel;
                    connectedWorkItems = model.ConnectedWorkItems;
                    SetAssignedWorkItems(platform, model.TfsAccountName);
                    SetAssociatedChangesets(platform, model.TfsAccountName);
                }else throw new ArgumentNullException("profileModel", "is null");
            }else throw new CollaborationPlatformUnavailableException("platform is null");
        }

        public ObservableCollection<WorkItemViewModel> GetAssignedWorkItems()
        {
            return assignedWorkItems;
        }

        public ObservableCollection<ChangesetModel> GetAssociatedChangesets()
        {
            return associatedChangesets;
        }
        

        private ScatterView associatedChangesetsContainer;

        public ScatterView AssociatedChangesetsContainer
        {
            get { return associatedChangesetsContainer; }
            set { associatedChangesetsContainer = value; }
        }

        private ScatterView associatedWorkItemsContainer;

        public ScatterView AssociatedWorkItemsContainer
        {
            get { return associatedWorkItemsContainer; }
            set { associatedWorkItemsContainer = value; }
        }

        private ScatterView detailWorkItemContainer;

        public ScatterView DetailWorkItemContainer
        {
            get { return detailWorkItemContainer; }
            set { detailWorkItemContainer = value; }
        }

        private void SetAssociatedChangesets(ICollaborationPlatform platform, String devName)
        {
            ICollection<ChangesetModel> changesets = platform.GetVersionControlService().GetChangesetsOfDeveloper(devName);
            foreach (ChangesetModel changesetModel in changesets)
            {
                associatedChangesets.Add(changesetModel);
            }
            associatedChangesetsCount = associatedChangesets.Count;
        }


        private void SetAssignedWorkItems(ICollaborationPlatform platform, String devName)
        {
            ICollection<WorkItemModel> items = platform.GetWorkItemService().GetWorkItemsAssignedTo(devName);
            foreach (WorkItemModel workItemModel in items)
            {
                assignedWorkItems.Add(workItemModel.CreateWorkItemViewModel());
            }
            assignedWorkItemsCount = assignedWorkItems.Count;
        }


        public void SetTriagingContainer(ScatterView container)
        {
            foreach (WorkItemViewModel workItemViewModel in assignedWorkItems)
            {
                workItemViewModel.TriagingContainer = container;
            }
        }


        public String TfsAccountName
        {
            get
            {
                return model.TfsAccountName;
            }
            set
            {
                model.TfsAccountName = value;
                NotifyOfPropertyChange(() => TfsAccountName);
            }
        }


        public String EmailAddress
        {
            get
            {
                return model.EmailAddress;
            }
            set
            {
                model.EmailAddress = value;
                NotifyOfPropertyChange(() => EmailAddress);
            }
        }

    }
}
