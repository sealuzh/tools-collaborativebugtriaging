﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using ExpertFinder.Model;

namespace ExpertFinder.Profile
{
    public class UpcomingEventViewModel: BaseViewModel
    {
        private UpcomingEvent model;


        public UpcomingEventViewModel(UpcomingEvent model)
        {
            this.model = model;
        }


        public String Title
        {
            get
            {
                return model.Title;
            }
            set
            {
                model.Title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        public String Description
        {
            get
            {
                return model.Description;
            }
            set
            {
                model.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public DateTime StartTime
        {
            get
            {
                return model.StartTime;
            }
            set
            {
                model.StartTime = value;
                NotifyOfPropertyChange(() => StartTime);
            }
        }

        public DateTime EndTime
        {
            get
            {
                return model.EndTime;
            }
            set
            {
                model.EndTime = value;
                NotifyOfPropertyChange(() => EndTime);
            }
        }

        public String Location
        {
            get
            {
                return model.Location;
            }
            set
            {
                model.Location = value;
                NotifyOfPropertyChange(() => Location);
            }
        }



    }
}
