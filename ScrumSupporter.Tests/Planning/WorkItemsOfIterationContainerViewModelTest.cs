﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ScrumSupporter.Planning;
using ScrumSupporter.Model;
using ScrumSupporter.Planning.WorkItemViews;


namespace ScrumSupporter.Tests.Planning
{
    [TestFixture]
    public class WorkItemsOfIterationContainerViewModelTest
    {
        private WorkItemsOfIterationContainerViewModel viewModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            viewModel = new WorkItemsOfIterationContainerViewModel(MockUpWorkItems());
        }

        private IList<WorkItemViewModel> MockUpWorkItems()
        {
            IList<WorkItemViewModel> workItems = new List<WorkItemViewModel>();

            WorkItemModel bugModel = new BugModel() { Iteration = "iter1"};
            WorkItemViewModel bug = new BugViewModel(bugModel);
            workItems.Add(bug);

            WorkItemModel taskModel = new TaskModel() { Iteration = "iter1" };
            WorkItemViewModel task = new TaskViewModel(taskModel);
            workItems.Add(task);

            WorkItemModel issueModel = new IssueModel() { Iteration = "iter1" };
            WorkItemViewModel issue = new IssueViewModel(issueModel);
            workItems.Add(issue);

            WorkItemModel userStoryModel = new UserStoryModel() { Iteration = "iter1" };
            WorkItemViewModel userStory = new UserStoryViewModel(userStoryModel);
            workItems.Add(userStory);

            return workItems;
        }

        [Test]
        public void setStackRankTest()
        {
            viewModel.SetStackRank();

            Assert.AreEqual(viewModel.Header, "iter1");
            Assert.LessOrEqual(viewModel.WorkItemList.First().StackRank, viewModel.WorkItemList.Last().StackRank);
        }

    }
}
