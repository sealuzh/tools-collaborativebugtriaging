﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;


namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the version control of the collaboration platorm.
    /// </summary>
    public interface IVersionControlService
    {
        /// <summary>
        /// Queries all changesets which are associated to the given id of a work item.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ICollection<ChangesetModel> GetChangesetsOfWorkItem(int id);

        /// <summary>
        /// Queries all changesets which were commited by the developer.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ICollection<ChangesetModel> GetChangesetsOfDeveloper(String name);

        String GetChangesOfChangeset(int id);

        IList<String> DownloadCurrentAndPreviousVersion(int changesetId, String className);

        String DownloadFile(int changesetId, String className);

    }
}
