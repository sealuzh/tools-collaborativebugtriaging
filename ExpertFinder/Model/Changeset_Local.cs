﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpertFinder.Model
{
    public class Changeset_Local
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        

        private DateTime creationDate;

        public DateTime CreationDate
        {
            get { return creationDate; }
            set { creationDate = value; }
        }

        private String comment;

        public String Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        private String committer;

        public String Committer
        {
            get { return committer; }
            set { committer = value; }
        }
        
        
        
    }
}
