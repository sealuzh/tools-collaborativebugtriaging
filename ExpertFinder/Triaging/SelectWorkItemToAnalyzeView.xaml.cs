﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.Surface.Presentation.Manipulations;
using System.Windows.Media;
using Microsoft.Surface.Presentation;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;
using ScrumSupporter.Util;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// Interaktionslogik für SelectWorkItemToAnalyzeView.xaml
    /// </summary>
    public partial class SelectWorkItemToAnalyzeView : SurfaceUserControl
    {
        private WorkItemModel model;
        private bool toAnalyze = false;

        private DateTime oldTime;

        private const int LEFTMARGIN = 122;
        private const int RIGHTMARGIN = 677;

        //Containers to hold different views.
        private ScatterView containerForDetailWorkItemView;
        private ScatterView triagingContainer;
        private ScatterView selectWorkItemContainer;

        private Affine2DManipulationProcessor manipulationProcessorDetailWorkItemView;


        public SelectWorkItemToAnalyzeView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;

            DialogInvalidWorkItemID.OKButton.Click += DialogInvalidWorkItemIDOnOKButtonClicked;
            DialogCollaborationPlatformUnavailable.OKButton.Click += DialogCollaborationPlatformUnavailableOnOKButtonClicked;
            DialogNoMatchingWorkItemFound.OKButton.Click += DialogNoMatchingWorkItemFoundOnOKButtonClicked;
        }

        /// <summary>
        /// Sets the container for the Triaging View.
        /// </summary>
        /// <param name="container"></param>
        public void SetTriagingContainer(ScatterView container)
        {
            triagingContainer = container;
        }


        /// <summary>
        /// Opens the Work Item Analyze View or the view to select a work item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void toAnalyzeButton_Click(object sender, RoutedEventArgs e)
        {

            if (!(idTextBox.Text.Equals(String.Empty)))
            {
                HandleIdInput();
            }
            else if (titleContainsTextBox.IsEnabled)
            {
                HandleTitleTextBoxInput();
            }
            containerForDetailWorkItemView.Items.Clear();
        }

        /// <summary>
        /// Processes the input of the id textbox.
        /// </summary>
        private void HandleIdInput()
        {
            SelectWorkItemToAnalyzeViewModel viewModel = this.DataContext as SelectWorkItemToAnalyzeViewModel;
            try
            {
                model = viewModel.FindWorkItemById(idTextBox.Text);
                CreateTriagingView(model);
                selectWorkItemContainer.Items.Clear();
            }
            catch (InvalidWorkItemIdException e1)
            {
                //Show dialog to the user that a invalid work item ID was provided 
                System.Console.WriteLine(e1.Message);
                DialogInvalidWorkItemID.Visibility = Visibility.Visible;
            }
            catch (CollaborationPlatformUnavailableException e2)
            {
                //Show dialog to the user that the collaboration platform is not available
                System.Console.WriteLine(e2.Message);
                DialogCollaborationPlatformUnavailable.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Creates and styles the ScatterViewItem which holds the TriagingView. 
        /// </summary>
        /// <param name="model"></param>
        private void CreateTriagingView(WorkItemModel model)
        {
            try
            {
                CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
                ICollaborationPlatform platform = factory.CollaborationPlatform;

                TriagingContainerViewModel containerViewModel = new TriagingContainerViewModel(model.GetViewModelType(), platform);
                ScatterViewItem sviTriagingView = containerViewModel.GetViewWrappedInScatterView(FindResource("sviStyle") as Style, triagingContainer);

                triagingContainer.Items.Add(sviTriagingView);
            }
            catch (ArgumentNullException e)
            {
                System.Console.WriteLine(e.Message);
                //Show dialog
            }
        }


        /// <summary>
        /// Processes the input of the title textbox.
        /// </summary>
        private void HandleTitleTextBoxInput()
        {
            SelectWorkItemToAnalyzeViewModel viewModel = this.DataContext as SelectWorkItemToAnalyzeViewModel;

            if (toAnalyze)
            {
                if (model != null)
                {
                    CreateTriagingView(model);
                    selectWorkItemContainer.Items.Clear();
                }
            }
            else
            {
                try
                {
                    viewModel.SetPossibleWorkItems(titleContainsTextBox.Text, triagingContainer);
                    if (viewModel.ArePossibleWorkItemsFound())
                    {
                        PossibleWorkItemsContainer.Visibility = Visibility.Visible;
                        sviPossibleWorkItems.Center = new Point(this.ActualWidth / 2, this.ActualHeight / 2);
                    }
                    else
                    {
                        //show dialog that no work items are found with the given words.
                        DialogNoMatchingWorkItemFound.Visibility = Visibility.Visible;
                    }
                }
                catch (CollaborationPlatformUnavailableException e1)
                {
                    //Show dialog to the user that the collaboration platform is not available
                    System.Console.WriteLine(e1.Message);
                    DialogCollaborationPlatformUnavailable.Visibility = Visibility.Visible;
                }
            }
        }


        /// <summary>
        /// Hides the dialog "Invalid work item ID provided" and drains the id textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DialogInvalidWorkItemIDOnOKButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogInvalidWorkItemID.Visibility = Visibility.Hidden;
            idTextBox.Text = "";
        }

        /// <summary>
        /// Hides the dialog "CollaborationPlatform unavailable"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DialogCollaborationPlatformUnavailableOnOKButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogCollaborationPlatformUnavailable.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Hides the dialog "DialogNoMatchingWorkItemFound".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DialogNoMatchingWorkItemFoundOnOKButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogNoMatchingWorkItemFound.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Sets the TitleContains TextBox to disabled, if a valid ID is entered. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(idTextBox.Text.Equals("")))
            {
                titleContainsTextBox.IsEnabled = false;
                if (!(ValidateId(idTextBox.Text)))
                {
                    idTextBox.Text = "";
                }
            }
            else
            {
                titleContainsTextBox.IsEnabled = true;
            }


        }

        /// <summary>
        /// Sets the ID textbox to disabled, if valid text is entered. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void titleContainsTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(titleContainsTextBox.Text.Equals("")))
            {
                idTextBox.IsEnabled = false;
            }
            else
            {
                idTextBox.IsEnabled = true;
            }
            toAnalyze = false;
        }

        /// <summary>
        /// Validates if the text is a number. 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool ValidateId(String text)
        {
            int result;
            return Int32.TryParse(text, out result);
        }

        /// <summary>
        /// Handles click event on the ListBox. If one click occured the work item is selected and the model is set. If a double 
        /// click occurs the detailed view is loaded. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkItemsToSelectListbox_PreviewContactDown(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            DateTime now = DateTime.Now;
            TimeSpan interval = now.Subtract(oldTime);

            if (workItemsToSelectListbox.Items.CurrentItem is WorkItemViewModel)
            {
                WorkItemViewModel viewModel = workItemsToSelectListbox.Items.CurrentItem as WorkItemViewModel;
                titleContainsTextBox.Text = viewModel.Title;
                model = viewModel.GetModel();
                toAnalyze = true;

            }

            //if a double click occurs the Detail work item view opens.
            if (interval.TotalMilliseconds < Constants.DOUBLECLICKLIMIT)
            {
                if (workItemsToSelectListbox.Items.CurrentItem is WorkItemViewModel)
                {
                    if (containerForDetailWorkItemView.HasItems)
                    {
                        containerForDetailWorkItemView.Items.Clear();
                    }
                    WorkItemViewModel model = workItemsToSelectListbox.Items.CurrentItem as WorkItemViewModel;

                    ScatterViewItem sviDetailWorkItem = model.GetView();

                    sviDetailWorkItem.PreviewContactDown += SviDetailWorkItemContactDown;
                    manipulationProcessorDetailWorkItemView = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviDetailWorkItem);

                    manipulationProcessorDetailWorkItemView.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviDetailWorkItemViewManipulationCompleted);

                    containerForDetailWorkItemView.Items.Add(sviDetailWorkItem);
                }
            }
            oldTime = DateTime.Now;
        }

        /// <summary>
        /// Evaluates if the DetailWorkItem view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemViewManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor = 
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, this.ActualWidth, this.ActualHeight), e);
                    

                inertiaProcessor.Begin();
                inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviDetailWorkItemViewInertiaCompleted);
            }
        }

        /// <summary>
        /// Removes the DetailWorkItemView from the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemViewInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
             containerForDetailWorkItemView.Items.Clear();
        }

        /// <summary>
        /// Starts the manipulation processor of the DetailWorkItem view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorDetailWorkItemView.BeginTrack(e.Contact);
        }


        /// <summary>
        /// Sets the container for the detail view of a work item. 
        /// </summary>
        /// <param name="container"></param>
        public void SetContainerForDetailWorkItemView(ScatterView container)
        {
            containerForDetailWorkItemView = container;
        }

        /// <summary>
        /// Sets the container for this user control
        /// </summary>
        /// <param name="container"></param>
        public void SetSelectWorkItemToAnalyzeContainer(ScatterView container)
        {
            selectWorkItemContainer = container;
        }

        private void ClosePossibleWIButton_Click(object sender, RoutedEventArgs e)
        {
            PossibleWorkItemsContainer.Visibility = Visibility.Hidden;
        }



    }
}
