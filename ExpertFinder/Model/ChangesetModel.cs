﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Triaging;
using ScrumSupporter.CollaborationPlatform;
using System.Windows.Forms.Integration;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace ScrumSupporter.Model
{
    public class ChangesetModel: BaseViewModel
    {

        public ObservableCollection<ChangeViewModel> changes { get; set; }
        private Grid hostingGrid;

        public void SetHostingGrid(Grid g)
        {
            this.hostingGrid = g;
        }

        public Grid GetHostingGrid()
        {
            return hostingGrid;
        }

        public ChangesetModel()
        {
            changes = new ObservableCollection<ChangeViewModel>();
        }


        private int id;

        public int Id
        {
            get { return id; }
            set { id = value;
            NotifyOfPropertyChange(() => Id);
            }
        }
        

        private DateTime creationDate;

        public DateTime CreationDate
        {
            get { return creationDate; }
            set { creationDate = value;
            NotifyOfPropertyChange(() => CreationDate);
            }
        }

        private String comment;

        public String Comment
        {
            get { return comment; }
            set { comment = value;
            NotifyOfPropertyChange(() => Comment);
            }
        }

        private String committer;

        public String Committer
        {
            get { return committer; }
            set { committer = value;
            NotifyOfPropertyChange(() => Committer);
            }
        }


        public ScatterViewItem GetChangesView()
        {
            ScatterViewItem svi = new ScatterViewItem();
            ChangesetChangeView changeView = new ChangesetChangeView();

            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;
            if (platform.IsCollaborationPlatformAvailable())
            {
               String changes =  platform.GetVersionControlService().GetChangesOfChangeset(id);
               changeView.changesTextBlock.Text = changes;
            }

            svi.Content = changeView;

            return svi;
        }


        public WindowsFormsHost GetChangesViewInWinMerge(String nameOfClass)
        {
            try
            {
                //External exe inside WPF Window 
                System.Windows.Forms.Panel _pnlSched = new System.Windows.Forms.Panel();
                _pnlSched.MaximumSize = new System.Drawing.Size(700, 400);
                WindowsFormsHost windowsFormsHost1 = new WindowsFormsHost();
                windowsFormsHost1.Child = _pnlSched;
                windowsFormsHost1.MaxWidth = 700;
                windowsFormsHost1.MaxHeight = 400;
                windowsFormsHost1.Margin = new Thickness(200, 368, 124, 0);
                

                //ProcessStartInfo psi = new ProcessStartInfo(@"C:\Program Files\WinMerge\WinMergeU.exe");
                //psi.WindowStyle = ProcessWindowStyle.Normal;
                //Process PR = Process.Start(psi);

                CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
                ICollaborationPlatform platform = factory.CollaborationPlatform;

                if (platform.IsCollaborationPlatformAvailable())
                {
                    IList<String> location = platform.GetVersionControlService().DownloadCurrentAndPreviousVersion(this.Id, nameOfClass);
                    Process PR = Process.Start(@"C:\Program Files\WinMerge\WinMergeU.exe",
                                        String.Format("{0}{1} {0}{2}", System.IO.Path.GetTempPath(),
                                                      location.ElementAt(0), location.ElementAt(1)));

                    //Process PR = Process.Start(@"C:\Program Files\WinMerge\WinMergeU.exe",
                    //                    String.Format("{0}{1} {0}{2}", System.IO.Path.GetTempPath(),
                    //                                  @"\54ProfileContainerView.xaml.cs", @"\55ProfileContainerView.xaml.cs"));

                    PR.WaitForInputIdle(); // true if the associated process has reached an idle state.

                    System.Threading.Thread.Sleep(3000);

                    IntPtr hwd = PR.MainWindowHandle;
                    SetParent(PR.MainWindowHandle, _pnlSched.Handle);  // loading exe to the wpf window.

                    return windowsFormsHost1;




                }
                else return null;

            }
            catch (Exception ex)
            {
                //Nothing...
                return null;
            }
        }

   

        public IntPtr MainWindowHandle { get; set; }


        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        
        
    }
}
