﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Shapes;
using System.Windows;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// Converts the X1,X2,Y1,Y2 Property of Line into the middlepoint of a Line.
    /// </summary>
    public class MidpointConverter: IValueConverter
    {
        /// <summary>
        /// Converts the X1,X2,Y1,Y2 of a Line into the middlepoint.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType,
        object parameter, CultureInfo culture)
        {
            var line = value as Line;
            Point point = new Point((line.X1 + line.X2)/2, (line.Y1+line.Y2)/2);


            return point;
        }

        public object ConvertBack(object value, Type targetType,
        object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
