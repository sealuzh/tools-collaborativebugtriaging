﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows;

namespace ScrumSupporter.Util
{
    public class Screenshot
    {
        public static void TakeScreenshot(Visual target)
        {
            String fileName = "Screenshot-" + DateTime.UtcNow.ToString().Replace(" ", "-").Replace(".", "_").Replace(":", "_") + ".tiff";
            FileStream stream = new FileStream(fileName, FileMode.Create);
            TiffBitmapEncoder encoder = new TiffBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(GetScreenShot(target)));
            encoder.Save(stream);
            stream.Flush();
            stream.Close();
        }

        private static BitmapSource GetScreenShot(Visual target)
        {
            Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
            RenderTargetBitmap bitmap = new RenderTargetBitmap(Convert.ToInt32(bounds.Width), Convert.ToInt32(bounds.Height), 96, 96, PixelFormats.Pbgra32);
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext context = drawingVisual.RenderOpen())
            {
                context.DrawRectangle(new VisualBrush(target), null, new Rect(new Point(), bounds.Size));
                context.Close();
            }

            bitmap.Render(drawingVisual);
            return bitmap;
        }


    }
}
