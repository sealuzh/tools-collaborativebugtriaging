﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.Util
{
    /// <summary>
    /// Definition of Constants.
    /// </summary>
    public static class Constants
    {

        public const int TBLWIDTH = 1024;
        public const int TBLHEIGHT = 768;
        public const int BACKLOGWIDTH = 913;
        public const int BACKLOGHEIGHT = 756;

        public const int DOUBLECLICKLIMIT = 2000;
    }
}
