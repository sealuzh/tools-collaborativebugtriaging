﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using UZH.Infrastructure;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Planning;

namespace ScrumSupporter.LoadProject
{
    /// <summary>
    /// The View Model of the Enter Credentials View. 
    /// </summary>
    public class EnterCredentialsViewModel : BaseViewModel
    {
        private Object parentViewModel;

        public EnterCredentialsViewModel(Object viewModel)
        {
            IsLoadFromLocalSourceVisible = false;
            this.parentViewModel = viewModel;
        }


        private String username = String.Empty;

        public String Username
        {
            get { return username; }
            set
            {
                username = value;
                IsEnabled = ShouldConnectButtonBeEnabled();
                Feedback = String.Empty;
                NotifyOfPropertyChange(() => Username);
            }
        }

        private String password = String.Empty;

        public String Password
        {
            get { return password; }
            set
            {
                password = value;
                IsEnabled = ShouldConnectButtonBeEnabled();
                Feedback = String.Empty;
                NotifyOfPropertyChange(() => Password);
            }
        }

        private String domain = String.Empty;

        public String Domain
        {
            get { return domain; }
            set
            {
                domain = value;
                IsEnabled = ShouldConnectButtonBeEnabled();
                Feedback = String.Empty;
                NotifyOfPropertyChange(() => Domain);
            }
        }

        private String url = String.Empty;

        public String Url
        {
            get { return url; }
            set
            {
                url = value;
                IsEnabled = ShouldConnectButtonBeEnabled();
                Feedback = String.Empty;
                NotifyOfPropertyChange(() => Url);
            }
        }

        private String projectName = String.Empty;

        public String ProjectName
        {
            get { return projectName; }
            set
            {
                projectName = value;
                IsEnabled = ShouldConnectButtonBeEnabled();
                Feedback = String.Empty;
                NotifyOfPropertyChange(() => ProjectName);
            }
        }

        private Boolean isEnabled;

        public Boolean IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;

                NotifyOfPropertyChange(() => IsEnabled);
            }
        }

        private String feedback;

        public String Feedback
        {
            get { return feedback; }
            set
            {
                feedback = value;
                NotifyOfPropertyChange(() => Feedback);
            }
        }

        private String loadFromLocalSourceFeedback;

        public String LoadFromLocalSourceFeedback
        {
            get { return loadFromLocalSourceFeedback; }
            set
            {
                loadFromLocalSourceFeedback = value;
                NotifyOfPropertyChange(() => LoadFromLocalSourceFeedback);
            }
        }

        private Boolean ShouldConnectButtonBeEnabled()
        {
            if (!Username.Equals(String.Empty) && !Password.Equals(String.Empty) && !Domain.Equals(String.Empty) && !Url.Equals(String.Empty) && !ProjectName.Equals(String.Empty))
            {
                return true;
            }
            return false;
        }

        private bool isEnterDetailsVisibile;

        public bool IsEnterDetailsVisible
        {
            get { return isEnterDetailsVisibile; }
            set
            {
                isEnterDetailsVisibile = value;
                NotifyOfPropertyChange(() => IsEnterDetailsVisible);
            }
        }

        private bool isLoadFromLocalSourceVisible;

        public bool IsLoadFromLocalSourceVisible
        {
            get { return isLoadFromLocalSourceVisible; }
            set
            {
                isLoadFromLocalSourceVisible = value;
                NotifyOfPropertyChange(() => IsLoadFromLocalSourceVisible);
            }
        }




        public ICommand Connect
        {
            get { return new RelayCommand(ConnectExecute, CanConnectExecute); }
        }

        void ConnectExecute()
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory(ScrumSupporter.CollaborationPlatform.Type.TFS);
            ICollaborationPlatform collPlatform = factory.CollaborationPlatform;
            Credentials cred = new Credentials(Username, Password, Domain, Url, ProjectName);
            bool loginResult = collPlatform.Login(cred);
            if (loginResult == true)
            {
                Feedback = "Connection successfully established.";
            }
            else
            {
                Feedback = "Not able to establish connection.";
                IsEnterDetailsVisible = false;
                IsLoadFromLocalSourceVisible = true;
            }
            DisplayLoginResultInParentDisplay(loginResult);
        }

        private void DisplayLoginResultInParentDisplay(bool loginResult)
        {
            if (loginResult == true)
            {
                if (parentViewModel is StartWindowViewModel)
                {
                    (parentViewModel as StartWindowViewModel).IsProfilesControlEnabled = true;
                    (parentViewModel as StartWindowViewModel).IsWorkItemMgmtControlEnabled = true;
                    (parentViewModel as StartWindowViewModel).IsAnalyzeWorkItemEnabled = true;
                    (parentViewModel as StartWindowViewModel).OpacityProfilesControl = 1;
                    (parentViewModel as StartWindowViewModel).OpacityWorkItemMgmtControl = 1;
                    (parentViewModel as StartWindowViewModel).OpacityAnalyzeWorkItemControl = 1;
                }
            }
            else
            {
                (parentViewModel as StartWindowViewModel).IsWorkItemMgmtControlEnabled = true;
                (parentViewModel as StartWindowViewModel).OpacityWorkItemMgmtControl = 1;
            }
        }
        bool CanConnectExecute()
        {
            return true;
        }


        public ICommand EnterMyCreds
        {
            get { return new RelayCommand(EnterMyCredsExecute, CanEnterMyCredsExecute); }
        }
        void EnterMyCredsExecute()
        {
            Username = "kevic";
            Password = "Reverse!";
            Domain = "arktos";
            Url = "http://arktos.ifi.uzh.ch:8080/tfs";
            ProjectName = "MP1201";
        }
        bool CanEnterMyCredsExecute()
        {
            return true;
        }



        public ICommand ReturnToEnterDetailsCommand
        {
            get { return new RelayCommand(ReturnToEnterDetailsCommandExecute, CanReturnToEnterDetailsCommandExecute); }
        }
        void ReturnToEnterDetailsCommandExecute()
        {
            IsLoadFromLocalSourceVisible = false;
            IsEnterDetailsVisible = true;

        }
        bool CanReturnToEnterDetailsCommandExecute()
        {
            return true;
        }



    }
}
