﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Ink;
using System.IO;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;

namespace ScrumSupporter.Planning.WorkItemViews
{
    class StrokesHandler
    {
        private String strokesPath;
        private SurfaceInkCanvas inkCanvas;
        private SurfaceButton saveInkButton;
        private SurfaceButton eraseInkButton;
        private SurfaceButton backInkButton;

        public StrokesHandler(String path, SurfaceInkCanvas canvas, SurfaceButton saveButton, 
            SurfaceButton eraseButton, SurfaceButton backButton)
        {
            this.strokesPath = path;
            this.inkCanvas = canvas;
            this.saveInkButton = saveButton;
            this.eraseInkButton = eraseButton;
            this.backInkButton = backButton;

            this.saveInkButton.Click += OnSaveStrokes;
            this.eraseInkButton.Click += OnEraseButton;
            this.backInkButton.Click += OnBackButton;

        }

        /// <summary>
        /// Loads the already drawn strokes from external source.
        /// </summary>
        public void LoadStrokes()
        {
            StrokeCollection Strokes = new StrokeCollection();
            if (File.Exists(strokesPath))
            {
                FileStream fs = new FileStream(strokesPath, FileMode.Open, FileAccess.Read);
                StrokeCollection strokes = new StrokeCollection(fs);
                inkCanvas.Strokes = strokes;
                fs.Close();
            }
            else throw new FileLoadException("Not able to load strokes");
        }

        private void OnSaveStrokes(object sender, RoutedEventArgs e)
        {
            using (FileStream fs = new FileStream(strokesPath, FileMode.Create))
            {
                inkCanvas.Strokes.Save(fs);
                fs.Close();
            }
        }

        private void OnBackButton(object sender, RoutedEventArgs e)
        {
            inkCanvas.Visibility = Visibility.Hidden;
            saveInkButton.Visibility = Visibility.Hidden;
            eraseInkButton.Visibility = Visibility.Hidden;
            backInkButton.Visibility = Visibility.Hidden;
        }

        private void OnEraseButton(object sender, RoutedEventArgs e)
        {
            inkCanvas.Strokes.Clear();
        }

        public void OnOpenInkCanvas(object sender, RoutedEventArgs e)
        {
            inkCanvas.Visibility = Visibility.Visible;
            saveInkButton.Visibility = Visibility.Visible;
            eraseInkButton.Visibility = Visibility.Visible;
            backInkButton.Visibility = Visibility.Visible;
        }
    }
}
