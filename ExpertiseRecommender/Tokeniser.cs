﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections;

namespace ExpertiseRecommender
{
    public class Tokeniser
    {

        public Tokeniser()
        {
        }

        public Collection<String> getTokens(string sentence)
        {
            string lowerCasedSentece = sentence.ToLower();
            

            Collection<String> tokens = new Collection<string>();

            string[] tokenArray = lowerCasedSentece.Split(' ', ',' , '.', '(', ')', '?', '!', '"');


            foreach (string s in tokenArray)
            {
                if (!s.Equals(""))
                {
                    tokens.Add(s);
                }
                
            }

            return tokens;
        }

        public bool IsStopWord(String word)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            string webData = wc.DownloadString("http://www.d.umn.edu/~tpederse/Group01/WordNet/words.txt");

            string[] words = webData.Split('\n');
            return words.Contains(word);

        }


    }
}
