﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class TextProcessorTest
    {
        private TextProcessor textProcessor;

        [TestFixtureSetUp]
        public void SetUp()
        {
            textProcessor = new TextProcessor();
        }

        [Test]
        public void GetTokensTest()
        {
            ICollection<String> tokens = textProcessor.GetTokens("This is my masterproject which supports developers in a scrum meeting.");

            CollectionAssert.DoesNotContain(tokens, ".");
            CollectionAssert.DoesNotContain(tokens, " ");
            CollectionAssert.DoesNotContain(tokens, ",");
            CollectionAssert.DoesNotContain(tokens, "(");
            CollectionAssert.DoesNotContain(tokens, ")");
            CollectionAssert.DoesNotContain(tokens, "?");
            CollectionAssert.DoesNotContain(tokens, "!");
            CollectionAssert.DoesNotContain(tokens, "\"");

            foreach (String token in tokens)
            {
                Assert.That(!token.Contains("."));
                Assert.That(!token.Contains(" "));
                Assert.That(!token.Contains(","));
                Assert.That(!token.Contains("("));
                Assert.That(!token.Contains(")"));
                Assert.That(!token.Contains("?"));
                Assert.That(!token.Contains("!"));
                Assert.That(!token.Contains("\""));
            }

            CollectionAssert.DoesNotContain(tokens, "is");
            CollectionAssert.DoesNotContain(tokens, "this");
            CollectionAssert.DoesNotContain(tokens, "which");
            CollectionAssert.DoesNotContain(tokens, "in");
            CollectionAssert.DoesNotContain(tokens, "a");
            CollectionAssert.DoesNotContain(tokens, "my");

            CollectionAssert.DoesNotContain(tokens, "supports");

            CollectionAssert.Contains(tokens, "masterproject");
            CollectionAssert.Contains(tokens, "support");
            CollectionAssert.Contains(tokens, "developer");
            CollectionAssert.Contains(tokens, "scrum");
            CollectionAssert.Contains(tokens, "meeting");
            

        }

        [Test]
        public void GetEmptyTokensTest()
        {
            ICollection<String> tokens = textProcessor.GetTokens("");
            CollectionAssert.IsEmpty(tokens);
        }

        [Test]
        public void GetTokensNullTest()
        {
            ICollection<String> tokens = textProcessor.GetTokens(null);
            CollectionAssert.IsEmpty(tokens);
        }
    }
}
