﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.WorkItemViews;
using ScrumSupporter.Exceptions;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    public abstract class WorkItemModel
    {

        private DateTime changedDate;

        public DateTime ChangedDate
        {
            get { return changedDate; }
            set
            {
                changedDate = value;
            }
        }

        private String title;

        public String Title
        {
            get { return title; }
            set
            {
                title = value;
            }
        }

        private String assignedTo;

        public String AssignedTo
        {
            get { return assignedTo; }
            set { assignedTo = value; }
        }

        private String state;

        public String State
        {
            get { return state; }
            set { state = value; }
        }

        private String reason;

        public String Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        private double stackRank;

        public double StackRank
        {
            get { return stackRank; }
            set
            {
                stackRank = value;
            }
        }

        private String area;

        public String Area
        {
            get { return area; }
            set { area = value; }
        }

        private String iteration;

        public String Iteration
        {
            get { return iteration; }
            set { iteration = value; }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public abstract WorkItemViewModel GetViewModelType();

        public WorkItemViewModel CreateWorkItemViewModel()
        {
            if (this is BugModel)
            {
                return new BugViewModel(this);
            }
            else if (this is UserStoryModel)
            {
                return new UserStoryViewModel(this);
            }
            else if (this is IssueModel)
            {
                return new IssueViewModel(this);
            }
            else if (this is TaskModel)
            {
                return new TaskViewModel(this);
            }
            else
            {
                throw new WorkItemTypeNotFoundException();
            }
        }

        /// <summary>
        /// Return the free text of the work item.
        /// </summary>
        /// <returns></returns>
        public abstract String GetText();

        public override int GetHashCode()
        {
            return Id;
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as WorkItemModel);
        }
        public bool Equals(WorkItemModel obj)
        {
            return obj != null && obj.Id == this.Id;
        }

        public abstract WorkItem AdaptNewWorkItemToTfs(Project teamProject);

        public abstract WorkItem AdaptExistingWorkItemToTfs(WorkItem workItem);

        protected WorkItem SetCommonFields(WorkItem workItem)
        {
            if (workItem != null)
            {
                workItem.Title = this.Title;

                AllowedValuesCollection allowedAssignedTo = workItem.Fields["Assigned To"].AllowedValues;
                if (allowedAssignedTo.Contains(this.AssignedTo))
                {
                    workItem.Fields["Assigned To"].Value = this.AssignedTo;
                }

                workItem.State = this.State;
                workItem.Reason = this.Reason;

                AllowedValuesCollection allowedStackRankValues = workItem.Fields["Stack Rank"].AllowedValues;
                if (allowedAssignedTo.Contains(this.AssignedTo))
                {
                    workItem.Fields["Stack Rank"].Value = this.StackRank;
                }

                workItem.AreaPath = this.Area;
                workItem.IterationPath = this.Iteration;

                return workItem;
            }
            else throw new ArgumentNullException("workItem", "is null");
        }
        
    }
}
