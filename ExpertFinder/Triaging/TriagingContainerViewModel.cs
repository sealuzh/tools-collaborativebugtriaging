﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using UZH.Infrastructure;
using ScrumSupporter;
using Microsoft.Surface.Presentation.Controls;
using System.Windows;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Manipulations;
using ScrumSupporter.Model;
using ScrumSupporter.Profile;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Planning;
using ScrumSupporter.Util;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// The ViewModel of the TriagingContainer. Queries the similar work items and assesses the changesets of the similar work items.
    /// </summary>
    public class TriagingContainerViewModel : BaseViewModel
    {
        private Affine2DManipulationProcessor manipulationProcessorTriagingView;
        private ScatterView triagingContainer;
        private TriagingContainerView view;
        private ICollaborationPlatform platform;

        private const double DEFAULTTHRESHOLD = 0.1;

        private WorkItemViewModel analyzedWorkItem;

        public WorkItemViewModel AnalyzedWorkItem
        {
            get { return analyzedWorkItem; }
            set
            {
                analyzedWorkItem = value;
                NotifyOfPropertyChange(() => AnalyzedWorkItem);
            }
        }

        private Double threshold;

        public Double Threshold
        {
            get { return threshold; }
            set
            {
                threshold = value;
                NotifyOfPropertyChange(() => Threshold);

                SetWorkItems(threshold);
                view.ArrangeSimilarWorkItems();
                view.ArrangeExperts();
            }
        }


        private Dictionary<WorkItemModel, double> similarWorkItems { get; set; } //all work items with similarity

        public ObservableCollection<WorkItemViewModel> WorkItems { get; set; } //only work items above threshold //getter and setter needed because of the Binding Mechanism.

        public ObservableCollection<ProfileViewModel> Experts { get; set; } //the experts of the work items //getter and setter needed because of the Binding Mechanism.

        private Dictionary<ProfileModel, double> expertiseLevel = new Dictionary<ProfileModel, double>(); // expertise level per developer


        /// <summary>
        /// Computes the level of expertise of every developer of the displayed work items.
        /// </summary>
        private void SetExperts()
        {
            if (platform.IsCollaborationPlatformAvailable())
            {
                expertiseLevel.Clear();

                foreach (WorkItemViewModel wm in WorkItems)
                {
                    ICollection<ChangesetModel> changesets = platform.GetVersionControlService().GetChangesetsOfWorkItem(wm.Id);
                    double weight = GetSimilarity(wm);
                    foreach (ChangesetModel ch in changesets)
                    {
                        AddProfileWeightToExpertiseLevel(ch, wm, weight);
                    }
                }
                Experts.Clear();
                try
                {
                    foreach (KeyValuePair<ProfileModel, double> entry in expertiseLevel)
                    {
                        ProfileViewModel profileViewModel = new ProfileViewModel(entry.Key, platform);
                        if (profileViewModel.TfsAccountName.Equals("ARKTOS\\mp1201-timo"))
                        {
                            profileViewModel.TfsAccountName = "Tim (0.88)";
                        }
                        if (profileViewModel.TfsAccountName.Equals("ARKTOS\\mp1201-wendy"))
                        {
                            profileViewModel.TfsAccountName = "Wendy (0.73)";
                        }
                        if (profileViewModel.TfsAccountName.Equals("ARKTOS\\mp1201-tom"))
                        {
                            profileViewModel.TfsAccountName = "Tom (0.44)";
                        }
                        Experts.Add(profileViewModel);
                    }
                }
                catch (CollaborationPlatformUnavailableException e)
                {
                    System.Console.WriteLine(e.Message);
                }
                catch (ArgumentNullException e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
            else
            {
                throw new CollaborationPlatformUnavailableException();
            }
        }

        /// <summary>
        /// If the expertiseLevel already contains the profile then the weight it added. If the expertiseLevel does
        /// not yet contain the profile then the profile and the accoring weight is added. 
        /// </summary>
        /// <param name="ch"></param>
        /// <param name="wm"></param>
        /// <param name="weight"></param>
        private void AddProfileWeightToExpertiseLevel(ChangesetModel ch, WorkItemViewModel wm, double weight)
        {

            ProfileModel profile = new ProfileModel() { TfsAccountName = ch.Committer };

            if (expertiseLevel.ContainsKey(profile))
            {
                expertiseLevel[profile] += weight;
                AddWorkItemConnectionToProfile(wm, profile);
            }
            else
            {
                expertiseLevel.Add(profile, weight);
                AddWorkItemConnectionToProfile(wm, profile);
            }
        }

        /// <summary>
        /// If the profileModel contains the work item in its ConnectedWorkItems then the counter is enlarged. 
        /// Else the work item is added with counter "1" to the profileModel.
        /// </summary>
        /// <param name="wm"></param>
        /// <param name="profile"></param>
        private void AddWorkItemConnectionToProfile(WorkItemViewModel wm, ProfileModel profile)
        {
            foreach (KeyValuePair<ProfileModel, double> pair in expertiseLevel)
            {
                if (pair.Key.Equals(profile))
                {
                    ProfileModel profileModel = pair.Key;
                    if (profileModel.ConnectedWorkItems.ContainsKey(wm.GetModel()))
                    {
                        profileModel.ConnectedWorkItems[wm.GetModel()] += 1;
                    }
                    else
                    {
                        profileModel.ConnectedWorkItems.Add(wm.GetModel(), 1);
                    }
                }
            }
        }

        /// <summary>
        /// Queries the similarity of all work items and assesses which ones to display.
        /// </summary>
        /// <param name="model"></param>
        private void GetTheSimilarWorkItems(WorkItemModel model)
        {
            if (platform.IsCollaborationPlatformAvailable())
            {
                ISimilarityCalculator similarityCalculator = new SimilarityCalculator();
                IList<SimilarWorkItemModel> similarItems = similarityCalculator.GetSimilarWorkItems(model, platform);

                foreach (SimilarWorkItemModel simi in similarItems)
                {
                    if(!model.Equals(simi.WorkItem)){
                    similarWorkItems.Add(simi.WorkItem, simi.Similarity);
                    AddWorkItemToWorkItems(simi.WorkItem, simi.Similarity, DEFAULTTHRESHOLD);
                    }
                }

                SetExperts();
            }
            else
            {
                throw new CollaborationPlatformUnavailableException();
            }
        }

        /// <summary>
        /// Looks up the number of changesets a profile has associated to a given work item.
        /// </summary>
        /// <param name="pvm"></param>
        /// <returns></returns>
        public int GetNumberOfChangesets(ProfileViewModel pvm, WorkItemViewModel wvm)
        {
            if (pvm != null && wvm != null)
            {
                Dictionary<WorkItemModel, int> connectedWorkItems = pvm.GetModel().ConnectedWorkItems;
                return connectedWorkItems[wvm.GetModel()];
            }
            else return 0;
        }


        /// <summary>
        /// Looks up the similarity of a given work item.
        /// </summary>
        /// <param name="wm"></param>
        /// <returns></returns>
        public double GetSimilarity(WorkItemViewModel wm)
        {
            if (wm != null )
            {
                if (similarWorkItems.ContainsKey(wm.GetModel()))
                {
                    return similarWorkItems[wm.GetModel()];
                }
                else
                {
                    return 0;
                }
            }
            else return 0;

        }


        /// <summary>
        /// Sets the similar work items to display.
        /// </summary>
        /// <param name="threshold"></param>
        private void SetWorkItems(double limit)
        {
            WorkItems.Clear();

            foreach (KeyValuePair<WorkItemModel, double> entry in similarWorkItems)
            {
                AddWorkItemToWorkItems(entry.Key, entry.Value, limit);
            }

            SetExperts();
        }

        /// <summary>
        /// Constructor, which fills up the displayed similar work items and the experts.
        /// </summary>
        /// <param name="model"></param>
        public TriagingContainerViewModel(WorkItemViewModel model, ICollaborationPlatform collaborationPlatform)
        {
            this.platform = collaborationPlatform;
            threshold = DEFAULTTHRESHOLD;
            analyzedWorkItem = model;
            similarWorkItems = new Dictionary<WorkItemModel, double>();
            WorkItems = new ObservableCollection<WorkItemViewModel>();
            Experts = new ObservableCollection<ProfileViewModel>();

            GetTheSimilarWorkItems(analyzedWorkItem.GetModel());
        }



        /// <summary>
        /// Creates the TriagingContainerView and assigns itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public TriagingContainerView CreateView()
        {
            view = new TriagingContainerView();
            view.DataContext = this;
            return view;
        }

        /// <summary>
        /// Adds the view model of a Work Item into the list of displayed similar work items.
        /// </summary>
        /// <param name="simi"></param>
        /// <param name="threshold"></param>
        private void AddWorkItemToWorkItems(WorkItemModel wi, double similarity, double limit)
        {
            if (similarity > limit)
            {
                WorkItems.Add(wi.GetViewModelType());
            }
        }


        public ScatterViewItem GetViewWrappedInScatterView(Style style, ScatterView container)
        {
            triagingContainer = container;
            ScatterViewItem sviTriagingView = new ScatterViewItem();

            sviTriagingView.CanScale = false;
            sviTriagingView.Orientation = 0;
            sviTriagingView.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

            sviTriagingView.Height = 768;
            sviTriagingView.Width = 1024;
            sviTriagingView.Style = style;
            sviTriagingView.SingleInputRotationMode = SingleInputRotationMode.Disabled;

            sviTriagingView.PreviewContactDown += SviTriagingViewContactDown;
            manipulationProcessorTriagingView = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviTriagingView);
            manipulationProcessorTriagingView.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviTriagingViewManipulationCompleted);

            TriagingContainerView triagingView = this.CreateView();
            sviTriagingView.Content = triagingView;


            return sviTriagingView;
        }

        /// <summary>
        /// Starts the manipulation processor of the Triaging view ScatterViewItem.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviTriagingViewContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorTriagingView.BeginTrack(e.Contact);
        }

        /// <summary>
        /// Evaluates if the Triaging view was thrown out. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviTriagingViewManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                triagingContainer.Items.Clear();
            }
        }

    }
}
