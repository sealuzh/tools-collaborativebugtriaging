﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using System.Threading;

namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// Interaktionslogik für Slice_2.xaml
    /// </summary>
    /// <remarks>
    /// author: Katja Kevic
    /// date (yyyy mm dd): 2012 08 09
    /// </remarks>
    public partial class Slice_2 : SurfaceUserControl
    {
        public Slice_2()
        {
            InitializeComponent();
        }


        /// <summary>
        /// EventHandler for the hold gesture in order to start the aninmation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slice2_ContactHoldGesture(object sender, ContactEventArgs e)
        {
            //Animation
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sb_Completed);

            Animation ani = new Animation();
            sb = ani.getSliceAnimation(slice2, text2 , sb);
           
            slice2.BeginStoryboard(sb);
        }

        /// <summary>
        /// EventHandler for the Completed event of the storyboard. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void sb_Completed(object sender, EventArgs e)
        {
            var planningWindowViewModel = new Planning.PlanningWindowViewModel();

            var view = planningWindowViewModel.CreateView();
            view.Show();
        }
    }
}
