﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace ScrumSupporter.Planning
{
    public class WorkItemTemplateSelector: DataTemplateSelector
    {

        /// <summary>
        /// Selects depending on the type of the work item which DataTemplate is applied.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return (item as WorkItemViewModel).GetTemplate();
        }
    }
}
