﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using System.Collections;
using ScrumSupporter.Util;
using ScrumSupporter.Planning;
using ScrumSupporter.Profile;
using ScrumSupporter.Model;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Manipulations;
using System.Diagnostics;
using System.Windows.Forms.Integration;
using System.Runtime.InteropServices;
using ExpertFinder.Util;
using System.Net.Mail;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// Interaktionslogik für TriagingContainerView.xaml
    /// </summary>
    public partial class TriagingContainerView : SurfaceUserControl
    {
        private Affine2DManipulationProcessor manipulationProcessorChangesetsToSelect;
        private Affine2DManipulationProcessor manipulationProcessorChangesetChange;
        private DateTime oldTime;
        private SurfaceListBox changesetsToSelectListBox;

        public TriagingContainerView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;

        }

        /// <summary>
        /// Creates a Binding between two ScatterViewItems and a Line. 
        /// </summary>
        /// <param name="line"></param>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        private void BindLineToScatterViewItems(Line line, ScatterViewItem origin, ScatterViewItem destination)
        {
            // Bind line.(X1,Y1) to origin.ActualCenter
            BindingOperations.SetBinding(line, Line.X1Property, new Binding { Source = origin, Path = new PropertyPath("ActualCenter.X") });
            BindingOperations.SetBinding(line, Line.Y1Property, new Binding { Source = origin, Path = new PropertyPath("ActualCenter.Y") });

            // Bind line.(X2,Y2) to destination.ActualCenter
            BindingOperations.SetBinding(line, Line.X2Property, new Binding { Source = destination, Path = new PropertyPath("ActualCenter.X") });
            BindingOperations.SetBinding(line, Line.Y2Property, new Binding { Source = destination, Path = new PropertyPath("ActualCenter.Y") });

            if (origin.Name.Equals("itemToAnalyze"))
            {
                LineHost.Children.Add(line);
            }
            else
            {
                ExpertLineHost.Children.Add(line);
            }
        }

        /// <summary>
        /// Binds the TextBlock (in a ScatterViewItem) to the middle of a Line.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="text"></param>
        private void BindTextToLine(Line line, TextBlock text)
        {
            TextHost.Items.Add(CreateTextHolder(line, text, false));
        }

        /// <summary>
        /// Binds the Expert text (in a ScatterViewItem) to the middle of a Line.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="text"></param>
        private void BindExpertTextToLine(Line line, TextBlock text)
        {
            ExpertTextHost.Items.Add(CreateTextHolder(line, text, true));
        }

        /// <summary>
        /// Bind the text in a ScatetrViewItem to the middle of the line.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        private ScatterViewItem CreateTextHolder(Line line, TextBlock text, bool isExpertLine)
        {
            ScatterViewItem svi = new ScatterViewItem();
            svi.MaxHeight = 100;
            svi.MaxWidth = 100;
            svi.Background = new SolidColorBrush(Colors.Transparent);
            text.Foreground = new SolidColorBrush(Colors.White);
            svi.Content = text;
            svi.Orientation = 0;
            if (isExpertLine)
            {
                svi.BorderBrush = new SolidColorBrush(Colors.Black);
                svi.DataContext = line.DataContext;
                svi.PreviewContactDown += ChangesetsOfExpertsContactDown;
            }
            else
            {
                svi.Style = FindResource("sviStyle") as Style;
            }
            

            svi.CanMove = false;
            svi.CanRotate = false;

            MidpointConverter x = new MidpointConverter();
            BindingOperations.SetBinding(svi, ScatterViewItem.CenterProperty, new Binding { Source = line, Converter = x });

            return svi;
        }
        private ScatterViewItem sviChangesetsToSelect;
        private void ChangesetsOfExpertsContactDown(object sender, ContactEventArgs e)
        {
            ChangesetOfDeveloperPerWorkItem changesetOfDev = (sender as ScatterViewItem).DataContext as ChangesetOfDeveloperPerWorkItem;
             sviChangesetsToSelect = CreateChangesetsToSelectScatterViewItem(changesetOfDev);

            sviChangesetsToSelect.PreviewContactDown += SviChangesetsToSelectContactDown;
            manipulationProcessorChangesetsToSelect = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviChangesetsToSelect);
            manipulationProcessorChangesetsToSelect.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetsToSelectManipulationCompleted);


            ChangesetToSelectContainer.Items.Clear();
            ChangesetToSelectContainer.Items.Add(sviChangesetsToSelect);
        }

        private void SviChangesetsToSelectManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor =
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, ChangesetToSelectContainer.ActualWidth, ChangesetToSelectContainer.ActualHeight), e);

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetsInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    ChangesetToSelectContainer.Items.Clear();
                }

            }
        }
        private void SviChangesetsInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            ChangesetToSelectContainer.Items.Clear();
        }

        private void SviChangesetsToSelectContactDown(object sender, ContactEventArgs e)
        {
            if (e.Contact.IsTagRecognized)
            {
                Screenshot.TakeScreenshot(sviChangesetsToSelect);
            }

            manipulationProcessorChangesetsToSelect.BeginTrack(e.Contact);


        }

        private ScatterViewItem CreateChangesetsToSelectScatterViewItem(ChangesetOfDeveloperPerWorkItem changesetOfDevPerWorkItem)
        {
            Color neutralColor = (Color)FindResource("NeutralColor");
            DialogContainerConfigurer config = new DialogContainerConfigurer();

            ScatterViewItem item = config.GetScatterViewItemContainer();
            item.Center = new Point(150, 450);
            item.Orientation = -20;

            changesetsToSelectListBox = config.GetListBoxConfigsForChangesets(neutralColor);

            ChangesetsToSelectViewModel changesetsToSelectViewModel = new ChangesetsToSelectViewModel();
            changesetsToSelectViewModel.SetHostingContainer(grid);
            changesetsToSelectViewModel.SetChangesetsToSelect(changesetOfDevPerWorkItem.GetChangesetsToSelect());
            

            changesetsToSelectListBox.ItemsSource = changesetsToSelectViewModel.GetChangesetsToSelect();

            FrameworkElementFactory dataTemplateFactoryPanel = new FrameworkElementFactory(typeof(ChangesetToSelectView));
            DataTemplate dataTemplate = new DataTemplate();
            dataTemplate.VisualTree = dataTemplateFactoryPanel;
            changesetsToSelectListBox.ItemTemplate = dataTemplate;

            //changesetsToSelectListBox.PreviewContactDown += ChangesetsToSelectListBoxPreviewContactDown;

            item.ScatterManipulationDelta += SviManipulationDelta;

            item.Content = config.GetContectOfScatterViewItem(neutralColor, changesetsToSelectListBox);

            return item;
        }

        private WindowsFormsHost host;
        private void ChangesetsToSelectListBoxPreviewContactDown(object sender, ContactEventArgs e)
        {
            DateTime now = DateTime.Now;
            TimeSpan interval = now.Subtract(oldTime);

            if (interval.TotalMilliseconds < Constants.DOUBLECLICKLIMIT)
            {
                if (changesetsToSelectListBox.Items.CurrentItem is ChangesetModel)
                {
                    ChangesetContaienr.Items.Clear();
                    ChangesetModel model = changesetsToSelectListBox.Items.CurrentItem as ChangesetModel;

    
                    //host = model.GetChangesViewInWinMerge();
                   // grid.Children.Add(host);


                    //ScatterViewItem sviChangesetChanges = model.GetChangesView();

                    //sviChangesetChanges.PreviewContactDown += SviChangesetChangesContactDown;
                    //manipulationProcessorChangesetChange = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviChangesetChanges);
                    //manipulationProcessorChangesetChange.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetChangesManipulationCompleted);

                    //sviChangesetChanges.Height = 500;
                    //sviChangesetChanges.Width = 700;
                    //sviChangesetChanges.Center = new Point(Constants.TBLWIDTH / 2, Constants.TBLHEIGHT / 2);
                    //sviChangesetChanges.Orientation = 0;

                    //ChangesetContaienr.Items.Add(sviChangesetChanges);

                }
            }
            oldTime = DateTime.Now;
        }





        private void SviChangesetChangesManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                Affine2DInertiaProcessor inertiaProcessor = new Affine2DInertiaProcessor();
                inertiaProcessor.InitialOrigin = e.ManipulationOrigin;

                inertiaProcessor.DesiredAngularDeceleration = .0001;
                inertiaProcessor.DesiredDeceleration = .0001;
                inertiaProcessor.DesiredExpansionDeceleration = .0010;
                inertiaProcessor.Bounds = new Thickness(0, 0, ChangesetContaienr.ActualWidth, ChangesetContaienr.ActualHeight);
                inertiaProcessor.ElasticMargin = new Thickness(20);

                inertiaProcessor.InitialVelocity = e.Velocity;
                inertiaProcessor.InitialExpansionVelocity = e.ExpansionVelocity;
                inertiaProcessor.InitialAngularVelocity = e.AngularVelocity;

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetChangesInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    ChangesetContaienr.Items.Clear();
                }

            }
        }

        private void SviChangesetChangesInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            ChangesetContaienr.Items.Clear();
        }

        private void SviChangesetChangesContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorChangesetChange.BeginTrack(e.Contact);
        }


        /// <summary>
        /// Prevents the ScatterViewItem from sliding out the reachable container. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviManipulationDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            if ((sender as ScatterViewItem).ActualCenter.X < 128)
            {
                (sender as ScatterViewItem).Center = new Point(128, (sender as ScatterViewItem).ActualCenter.Y);
            }
        }

        /// <summary>
        /// Arranges the similar work items when the scatter view is loaded. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimilarWorkItemScatterView_Loaded(object sender, RoutedEventArgs e)
        {
            ArrangeSimilarWorkItems();
        }

        /// <summary>
        /// Arranges the similar work items nicely and connects the work item to analyze to the similar work items.
        /// </summary>
        public void ArrangeSimilarWorkItems()
        {

            int numberOfSimilarWorkItems = SimilarWorkItemScatterView.Items.Count;
            double availableHeight = Constants.TBLHEIGHT - 90 - 90;

            double distance = availableHeight / numberOfSimilarWorkItems;

            Point position = new Point(450, 90);

            bool xCoordinateAlter = false;
            bool orientationAlter = false;

            LineHost.Children.Clear();
            TextHost.Items.Clear();

            foreach (WorkItemViewModel similarWorkItem in SimilarWorkItemScatterView.Items)
            {
                DependencyObject dp = SimilarWorkItemScatterView.ItemContainerGenerator.ContainerFromItem(similarWorkItem);
                ScatterViewItem item = dp as ScatterViewItem;

                #region arrangePositions
                item.Height = 150;
                item.Width = 180;
                item.Center = position;

                if (orientationAlter)
                {
                    item.Orientation = 20;
                    orientationAlter = false;
                }
                else
                {
                    item.Orientation = -20;
                    orientationAlter = true;
                }

                if (xCoordinateAlter)
                {
                    position = new Point(450, position.Y + distance);
                    xCoordinateAlter = false;
                }
                else
                {
                    position = new Point(500, position.Y + distance);
                    xCoordinateAlter = true;
                }
                #endregion

                Line line = new Line { Stroke = Brushes.Black, StrokeThickness = 2.0 };
                line.Stroke = new SolidColorBrush(Colors.White);
                BindLineToScatterViewItems(line, itemToAnalyze, dp as ScatterViewItem);

                double sim = (this.DataContext as TriagingContainerViewModel).GetSimilarity(similarWorkItem);
                sim = Math.Round(sim, 2);
                line.Opacity = sim;

                line.Loaded += LineLoaded;
                item.ScatterManipulationDelta += SimilarSVIManipulationDelta;
            }
        }

        /// <summary>
        /// Enables the moving of the text on the lines, when the similar work item is moved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimilarSVIManipulationDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            MidpointConverter x = new MidpointConverter();

            foreach (ScatterViewItem item in TextHost.Items)
            {
                BindingExpression exp = item.GetBindingExpression(ScatterViewItem.CenterProperty);
                Line line = exp.DataItem as Line;
                if (line.X2 == (sender as ScatterViewItem).ActualCenter.X)
                {
                    BindingOperations.SetBinding(item, ScatterViewItem.CenterProperty, new Binding { Source = line, Converter = x });
                }
            }

            foreach (Line l in ExpertLineHost.Children)
            {
                if (l.X1 == (sender as ScatterViewItem).ActualCenter.X)
                {
                    foreach (ScatterViewItem item in ExpertTextHost.Items)
                    {
                        BindingExpression exp = item.GetBindingExpression(ScatterViewItem.CenterProperty);
                        Line line = exp.DataItem as Line;
                        if (line.Equals(l))
                        {
                            BindingOperations.SetBinding(item, ScatterViewItem.CenterProperty, new Binding { Source = line, Converter = x });
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Styles the text and binds it to the middle of the line.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LineLoaded(object sender, RoutedEventArgs e)
        {
            TextBlock text = new TextBlock();

            text.Foreground = new SolidColorBrush(Colors.Black);

            text.Text = (sender as Line).Opacity.ToString();
            (sender as Line).Opacity = 1;
            BindTextToLine(sender as Line, text);
        }

        /// <summary>
        ///  Arranges the expert items nicely and connects the similar work items to the experts.
        /// </summary>
        public void ArrangeExperts()
        {
            #region count experts to arrange
            int numberOfExperts = ExpertsScatterView.Items.Count;
            double availableHeight = Constants.TBLHEIGHT - 90 - 90;

            double distance = availableHeight / numberOfExperts;

            Point position = new Point(800, 90);

            bool xCoordinateAlter = false;
            bool orientationAlter = false;

            ExpertLineHost.Children.Clear();
            ExpertTextHost.Items.Clear();
            #endregion

            foreach (ProfileViewModel expert in ExpertsScatterView.Items)
            {
                DependencyObject dp = ExpertsScatterView.ItemContainerGenerator.ContainerFromItem(expert);
                ScatterViewItem item = dp as ScatterViewItem;
                #region arrange experts
                item.Height = 150;
                item.Width = 150;
                item.Center = position;
                item.Style = FindResource("sviStyle") as Style;

                if (orientationAlter)
                {
                    item.Orientation = 20;
                    orientationAlter = false;
                }
                else
                {
                    item.Orientation = -20;
                    orientationAlter = true;
                }

                if (xCoordinateAlter)
                {
                    position = new Point(800, position.Y + distance);
                    xCoordinateAlter = false;
                }
                else
                {
                    position = new Point(850, position.Y + distance);
                    xCoordinateAlter = true;
                }
                #endregion

                item.ScatterManipulationDelta += ExpertManiDelta;
                item.PreviewContactDown += ProfileViewHelperWithEmailOnContactDown;
                

                foreach (KeyValuePair<WorkItemModel, int> pair in expert.ConnectedWorkItems)
                {
                    foreach (WorkItemViewModel wivm in SimilarWorkItemScatterView.Items)
                    {
                        if (wivm.GetModel().Equals(pair.Key))
                        {
                            Line line = new Line { Stroke = Brushes.Black, StrokeThickness = 2.0 };
                            line.Stroke = new SolidColorBrush(Colors.White);
                            
                            DependencyObject dep = SimilarWorkItemScatterView.ItemContainerGenerator.ContainerFromItem(wivm); //the similar work item
                            BindLineToScatterViewItems(line, dep as ScatterViewItem, item); //connect similar work item to expert
                            int numberOfCh = (this.DataContext as TriagingContainerViewModel).GetNumberOfChangesets(expert, wivm);
                            line.Opacity = numberOfCh;
                            line.DataContext = new ChangesetOfDeveloperPerWorkItem(wivm, expert);
                            line.Loaded += ExpertLineLoaded;
                        }
                    }
                }
            }
        }

        private void ProfileViewHelperWithEmailOnContactDown(object sender, ContactEventArgs e)
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;

            if (platform.IsCollaborationPlatformAvailable())
            {
                if (e.Contact.DirectlyOver is Rectangle)
                {
                    System.Console.WriteLine("Try to send email");

                    ScatterViewItem svi = sender as ScatterViewItem;
                    ProfileViewModel content = svi.Content as ProfileViewModel;

                    String emailTo = content.EmailAddress;
                    IList<String> files = new List<String>();

                    foreach (object item in changesetsToSelectListBox.Items)
                    {
                        ChangesetModel changesetModel = item as ChangesetModel;
                        foreach (ChangeViewModel changeViewModel in changesetModel.changes)
                        {
                            if (changeViewModel.Ticked == true)
                            {
                                String location = platform.GetVersionControlService().DownloadFile(changesetModel.Id, changeViewModel.ClassName);
                                files.Add(location);
                            }
                        }
                    }




                    SendEmail(emailTo, files);


                    System.Console.WriteLine("Email sent");
                }
            }

        }


        private void SendEmail(String emailTo, IList<String> files)
        {
            


            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.gmx.com");
            mail.From = new MailAddress("seal.seal@gmx.ch");
            mail.To.Add(emailTo);
            mail.Subject = "Work Item Assignment Notification";
            mail.Body = "Please consider the attachment as starting point for your work...";

            foreach (String file in files)
            {
                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(file);
                mail.Attachments.Add(attachment);
            }

            //System.Net.Mail.Attachment attachment;
            //attachment = new System.Net.Mail.Attachment(@"C:\Users\Katja\Documents\textfile.txt");
            //mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("seal.seal@gmx.ch", "sealseal");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);


            
        }


        /// <summary>
        /// Enables the moving of the text on the line, when the expert scatterview changes its position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpertManiDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            foreach (ScatterViewItem item in ExpertTextHost.Items)
            {
                MidpointConverter x = new MidpointConverter();

                BindingExpression exp = item.GetBindingExpression(ScatterViewItem.CenterProperty);
                Line line = exp.DataItem as Line;

                if (line.X2 == (sender as ScatterViewItem).ActualCenter.X)
                {
                    BindingOperations.SetBinding(item, ScatterViewItem.CenterProperty, new Binding { Source = line, Converter = x });
                }
            }
        }

        /// <summary>
        /// Sets the expert text on the line when the line is finished loading.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpertLineLoaded(object sender, RoutedEventArgs e)
        {
            TextBlock text = new TextBlock();

            text.Foreground = new SolidColorBrush(Colors.Black);

            text.Text = (sender as Line).Opacity.ToString();
            (sender as Line).Opacity = 1;
            BindExpertTextToLine(sender as Line, text);
        }

        /// <summary>
        /// Arranges the items in the Expert Scatter View nicely as soon as the source is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpertsScatterView_Loaded(object sender, RoutedEventArgs e)
        {
            ArrangeExperts();
        }

        /// <summary>
        /// Enables the moving of the text on the line when the item to analyze is moved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void itemToAnalyze_ScatterManipulationDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            MidpointConverter x = new MidpointConverter();

            foreach (Line l in LineHost.Children)
            {
                if (l.X1 == (sender as ScatterViewItem).ActualCenter.X)
                {
                    foreach (ScatterViewItem item in TextHost.Items)
                    {
                        BindingExpression exp = item.GetBindingExpression(ScatterViewItem.CenterProperty);
                        Line line = exp.DataItem as Line;
                        if (line.Equals(l))
                        {
                            BindingOperations.SetBinding(item, ScatterViewItem.CenterProperty, new Binding { Source = line, Converter = x });
                        }
                    }
                }
            }


        }

        private void SurfaceUserControl_ContactDown(object sender, ContactEventArgs e)
        {
            if (e.Contact.IsTagRecognized)
            {
                Screenshot.TakeScreenshot(this);
            }
        }

 

    }
}
