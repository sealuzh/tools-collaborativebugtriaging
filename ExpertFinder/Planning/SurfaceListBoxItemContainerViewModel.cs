﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ExpertFinder.Planning
{
    public class SurfaceListBoxItemContainerViewModel
    {

        public String Header { get; set; }


        public SurfaceListBoxItemContainerViewModel(IList<WorkItemViewModel> list, String header)
        {
            userStories = new ObservableCollection<WorkItemViewModel>();

            foreach (var model in list)
            {
                userStories.Add(model);
            }

            this.Header = header;
        }


        public ObservableCollection<WorkItemViewModel> userStories { get; set; }


        public void setStackRank()
        {
            int stackrank = 1;
            foreach (WorkItemViewModel userStory in userStories)
            {
                userStory.StackRank = stackrank;
                stackrank++;
            }

        }
    }
}
