﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Net;
using Microsoft.TeamFoundation;

namespace TfsLink
{
    /// <summary>
    /// This class provides the connection to the TFS. Given valid credentials.
    /// </summary>
    public class Connection : IConnection
    {
        private String username;
        private String password;
        private String domain;
        private Uri uri;

        /// <summary>
        /// This is the constructor of TFSConnection. In order to establish a connection to the TFS,
        /// a valid username and password are needed.
        /// </summary>
        /// <param name="username">username to log in to the TFS</param>
        /// <param name="password">password to log in to the TFS</param>
        /// <param name="domain">domain of the TFS</param> //"arktos"
        /// <param name="projectName">name of the project</param>
        public Connection(String un, String pw, String dom, Uri u)
        {
            this.username = un;
            this.password = pw;
            this.domain = dom;
            this.uri = u;
        }

        /// <summary>
        /// Tries to connect to the TFS server with the given credentials. 
        /// </summary>
        /// <returns>true, if the authetification passed.</returns>
        Boolean IConnection.Connect()
        {
            TfsTeamProjectCollection tfs = CreateTeamProject();
            if (tfs != null)
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Return the TfsTeamProjectCollection, if the connection is established, else the Exception NoConnectionEstablished is thrown. 
        /// </summary>
        TfsTeamProjectCollection IConnection.GetTeamProjectCollection()
        {
            return CreateTeamProject();
        }

        private TfsTeamProjectCollection CreateTeamProject()
        {
            NetworkCredential credentials = new NetworkCredential(username, password, domain);
            TfsTeamProjectCollection tfs = new TfsTeamProjectCollection(uri, credentials);
            try
            {
                tfs.Authenticate();
                return tfs;
            }
            catch (TeamFoundationServiceUnavailableException e)
            {
                System.Console.WriteLine("TFS Authetification failed: " + e.Message);
                return null;
            }
            catch (TeamFoundationServerUnauthorizedException e)
            {
                System.Console.WriteLine("TFS Authetification failed: " + e.Message);
                return null;
            }
        }
    }
}
