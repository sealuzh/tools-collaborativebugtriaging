﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Surface.Presentation;
using System.Windows;
using System.Threading;
using UZH.Infrastructure;

namespace ScrumSupporter
{
    public class StartWindowViewModel : BaseViewModel
    {
        public StartWindowViewModel()
        {
            IsProfilesControlEnabled = false;
            IsWorkItemMgmtControlEnabled = false;
            IsAnalyzeWorkItemEnabled = false;
            OpacityProfilesControl = 0.41;
            OpacityWorkItemMgmtControl = 0.41;
            OpacityAnalyzeWorkItemControl = 0.41;
        }

        private bool isProfilesControlEnabled;

        public bool IsProfilesControlEnabled
        {
            get { return isProfilesControlEnabled; }
            set
            {
                isProfilesControlEnabled = value;
                NotifyOfPropertyChange(() => IsProfilesControlEnabled);
            }
        }

        private bool isWorkItemMgmtControlEnabled;

        public bool IsWorkItemMgmtControlEnabled
        {
            get { return isWorkItemMgmtControlEnabled; }
            set
            {
                isWorkItemMgmtControlEnabled = value;
                NotifyOfPropertyChange(() => IsWorkItemMgmtControlEnabled);
            }
        }

        private bool isAnalyzeWorkItemEnabled;

        public bool IsAnalyzeWorkItemEnabled
        {
            get { return isAnalyzeWorkItemEnabled; }
            set
            {
                isAnalyzeWorkItemEnabled = value;
                NotifyOfPropertyChange(() => IsAnalyzeWorkItemEnabled);
            }
        }

        private double opacityProfilesControl;

        public double OpacityProfilesControl
        {
            get { return opacityProfilesControl; }
            set
            {
                opacityProfilesControl = value;
                NotifyOfPropertyChange(() => OpacityProfilesControl);
            }
        }

        private double opacityWorkItemMgmtControl;

        public double OpacityWorkItemMgmtControl
        {
            get { return opacityWorkItemMgmtControl; }
            set
            {
                opacityWorkItemMgmtControl = value;
                NotifyOfPropertyChange(() => OpacityWorkItemMgmtControl);
            }
        }

        private double opacityAnalyzeWorkItemControl;

        public double OpacityAnalyzeWorkItemControl
        {
            get { return opacityAnalyzeWorkItemControl; }
            set
            {
                opacityAnalyzeWorkItemControl = value;
                NotifyOfPropertyChange(() => OpacityAnalyzeWorkItemControl);
            }
        }










    }
}
