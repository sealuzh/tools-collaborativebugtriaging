﻿using System;
using System.Collections.Generic;
using NHunspell;




namespace ScrumSupporter
{
    /// <summary>
    /// This class enables the look up of synonyms of a word.
    /// </summary>
    public class SynonymFinder: IDisposable
    {
        MyThes thes;
        Hunspell hunspell;

        /// <summary>
        /// MyThes thes and Hunspell hunspell are initialized.
        /// </summary>
        public SynonymFinder()
        {
            string currentDir = Environment.CurrentDirectory;
            currentDir = currentDir.Replace("\\\\", "\\");

            string th_en_us_new = @currentDir.Substring(0, currentDir.IndexOf("bin")) + "Files\\" + "th_en_us_new.dat";
            thes = new MyThes(th_en_us_new);

            string en_us_aff = @currentDir.Substring(0, currentDir.IndexOf("bin")) + "Files\\" + "en_us.aff";
            string en_us_dic = @currentDir.Substring(0, currentDir.IndexOf("bin")) + "Files\\" + "en_us.dic";

            hunspell = new Hunspell(en_us_aff, en_us_dic);
            
        }

        /// <summary>
        /// This method finds the synonyms of a given word.
        /// </summary>
        /// <param name="word">The methods looks for synonyms of this word.</param>
        /// <returns>Returns a list of synonyms.</returns>
        public IList<String> FindSynonym(String word)
        {
            IList<String> synonyms = new List<string>();
            
            //MyThes thes = new MyThes("th_en_us_new.dat");
            //Hunspell hunspell = new Hunspell("en_us.aff", "en_us.dic");
            ThesResult tr = thes.Lookup(word, hunspell);

            if (tr != null)
            {
                foreach (ThesMeaning meaning in tr.Meanings)
                {
                    foreach (string synonym in meaning.Synonyms)
                    {
                       synonyms.Add(synonym);
                    }
                }
            }
            return synonyms;
        }

        public void Dispose()
        {
            hunspell.Dispose();
        }
    }
}
