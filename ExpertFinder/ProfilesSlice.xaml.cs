﻿using System;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media.Animation;
using System.Windows;
using Microsoft.Surface.Presentation.Manipulations;
using Microsoft.Surface.Presentation;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Profile;
using ScrumSupporter.Util;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter
{
    /// <summary>
    /// Interaktionslogik für ProfilesSlice.xaml
    /// </summary>
    public partial class ProfilesSlice : SurfaceUserControl
    {
        private ScatterView profilesContainer;

        public ProfilesSlice()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Starts the storybaord to indicate to the user that the gesture is recognised. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slice_ContactHoldGesture(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sbCompleted);
            Animation animation = new Animation();
            sb = animation.GetTextAnimation(profilesSliceText, sb) as Storyboard;

            profilesSlice.BeginStoryboard(sb);
        }

        public void SetProfileContainer(ScatterView container)
        {
            this.profilesContainer = container;
        }

        /// <summary>
        /// Opens the profiles view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void sbCompleted(object sender, EventArgs e)
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;

            if (platform.IsCollaborationPlatformAvailable())
            {
                try
                {
                    var profileContainerViewModel = new ProfileContainerViewModel(platform);
                    var view = profileContainerViewModel.CreateView();
                    profileContainerViewModel.LoadProfileData();

                    ScatterViewItem sviProfileView = new ScatterViewItem();

                    sviProfileView.CanScale = false;
                    sviProfileView.Orientation = 0;
                    sviProfileView.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

                    sviProfileView.Height = 768;
                    sviProfileView.Width = 1024;
                    sviProfileView.Style = FindResource("sviStyle") as Style;
                    sviProfileView.SingleInputRotationMode = SingleInputRotationMode.Disabled;
                    sviProfileView.CanMove = false;

                    view.backButton.PreviewContactDown += BackButtonPreviewContactDown;

                    sviProfileView.Content = view;
                    profilesContainer.Items.Clear();
                    profilesContainer.Items.Add(sviProfileView);
                }
                catch (CollaborationPlatformUnavailableException e1)
                {
                    System.Console.WriteLine(e1.Message);
                }
                catch (ArgumentNullException e1)
                {
                    System.Console.WriteLine(e1.Message);
                }
            }
        }

        private void BackButtonPreviewContactDown(object sender, ContactEventArgs e)
        {
            profilesContainer.Items.Clear();
        }

        
    }
}
