﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Markup;
using System.Windows.Media.Animation;
using System.Windows.Media.Media3D;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Timers;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Manipulations;
using ScrumSupporter.Util;


namespace ScrumSupporter.Profile
{
    /// <summary>
    /// Interaktionslogik für ProfileContainerView.xaml
    /// </summary>
    public partial class ProfileContainerView : SurfaceUserControl
    {
        private DateTime oldTime;
        private int contactCounter = 0;
        private const int PICTUREVIEWSIZE = 200;
        private Affine2DManipulationProcessor manipulationProcessor;

        public ProfileContainerView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;
        }

        private void RotateToTheBack(PictureView view)
        {
            view.IsFront = false;
            DoubleAnimation anim = new DoubleAnimation();
            anim.Duration = new Duration(TimeSpan.FromSeconds(2));
            anim.From = 0.0;
            anim.To = 180.0;

            view.rotationFrontSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationBackSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationLeftSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationRightSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
        }

        private void RotateToTheFront(PictureView view)
        {
            view.IsFront = true;
            DoubleAnimation anim = new DoubleAnimation();
            anim.Duration = new Duration(TimeSpan.FromSeconds(2));
            anim.From = 180;
            anim.To = 360;


            view.rotationFrontSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationBackSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationLeftSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
            view.rotationRightSide.BeginAnimation(AxisAngleRotation3D.AngleProperty, anim);
        }


        /// <summary>
        /// Arrange the items nicely in the available space.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScatterViewContainer_Loaded(object sender, RoutedEventArgs e)
        {
            ProfileContainerViewModel dataContext = (this.DataContext as ProfileContainerViewModel);

            double rows = 3.0;
            double d = dataContext.GetProfileList().Count / rows;

            double dl = Math.Ceiling(d);

            int itemsPerRow = Convert.ToInt32(dl);
            int itemcounter = 1;

            Point position = new Point(120, 120);
            int margin = 20;
            int distance = Convert.ToInt32( (Constants.TBLWIDTH-120)  / itemsPerRow);
            distance -= margin;
            int heightDistance = 250;



            foreach(ProfileViewModel profile in (this.DataContext as ProfileContainerViewModel).GetProfileList())
            {
                PictureView view = new PictureView();
                view.DataContext = profile;
                ScatterViewItem item = new ScatterViewItem();
                item.Content = view;
                item.Orientation = 0;
                
                item.MinHeight = PICTUREVIEWSIZE;
                item.MinWidth = PICTUREVIEWSIZE;
                item.PreviewContactDown += ScatterViewContainer_PreviewContactDown;
                item.PreviewContactUp += ScatterViewContainer_PreviewContactUp;
                item.Style = this.FindResource("sviStyle") as Style;

                if (itemcounter <= itemsPerRow)
                {
                    item.Center = position;
                    position = new Point(position.X + distance, position.Y);
                    itemcounter++;
                }
                else
                {
                    position = new Point (120, position.Y + heightDistance);
                    item.Center = position;
                    position = new Point(position.X + distance, position.Y);
                    itemcounter = 2;
                }
                ScatterViewContainer.Items.Add(item);
            }

        }

       

        /// <summary>
        /// Starts the rotation of the profile if a double click is recognized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScatterViewContainer_PreviewContactDown(object sender, ContactEventArgs e)
        {
            contactCounter += 1;
            DateTime time = DateTime.Now;
            TimeSpan interval = time.Subtract(oldTime);
            PictureView pView = (sender as ScatterViewItem).Content as PictureView;


            if (interval.TotalMilliseconds < 500 && contactCounter < 2)
            {
                if (pView.IsFront)
                {
                    (sender as ScatterViewItem).CanScale = true ;
                    ProfileViewModel model = pView.DataContext as ProfileViewModel;

                    ProfileView profileView = (pView.BackSide.Visual as ProfileView);
                    profileView.DataContext = model;

                    RotateToTheBack(pView);

                }
                else if (!pView.IsFront)
                {
                    (sender as ScatterViewItem).CanScale = false;
                    RotateToTheFront(pView);
                }
            }
            oldTime = time;
        }


        private void ScatterViewContainer_PreviewContactUp(object sender, ContactEventArgs e)
        {
            contactCounter -= 1; 
        }

        /// <summary>
        /// Opens the list with upcoming events of the team.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calendar_Button_Click(object sender, RoutedEventArgs e)
        {
            ScatterViewItem sviEvents = CreateEventsScatterViewItem();

            sviEvents.PreviewContactDown += SviEventsContactDown;
            manipulationProcessor = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviEvents);
            manipulationProcessor.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviEventsManipulationCompleted);

            EventsContainer.Items.Clear();
            EventsContainer.Items.Add(sviEvents);
            calendar_Button.IsEnabled = false;
            calendar_Button.Opacity = 0.6;
        }

        /// <summary>
        /// Evaluates if the EventsView is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviEventsManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor = 
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, EventsContainer.ActualWidth, EventsContainer.ActualHeight), e);

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviEventsInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    EventsContainer.Items.Clear();
                    calendar_Button.IsEnabled = true;
                    calendar_Button.Opacity = 1;
                }

            }
        }

        /// <summary>
        /// Removes the ScatterViewItem holding the EventsView from the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviEventsInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            EventsContainer.Items.Clear();
            calendar_Button.IsEnabled = true;
            calendar_Button.Opacity = 1;
        }

        /// <summary>
        /// Starts the manipulation processor of the EventsView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviEventsContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessor.BeginTrack(e.Contact);
        }

        /// <summary>
        /// Creates a SurfaceListBox in a ScatterViewItem which holds all the events of the team.
        /// </summary>
        /// <returns></returns>
        private ScatterViewItem CreateEventsScatterViewItem()
        {
            Color neutralColor = (Color)FindResource("NeutralColor");
            DialogContainerConfigurer dialogContainer = new DialogContainerConfigurer();

            ScatterViewItem item = dialogContainer.GetScatterViewItemContainer();

            SurfaceListBox listBox = dialogContainer.GetListBoxConfigs(neutralColor);
            listBox.ItemsSource = (this.DataContext as ProfileContainerViewModel).GetEventsList();

            FrameworkElementFactory dataTemplateFactoryPanel = new FrameworkElementFactory(typeof(EventView));
            DataTemplate dataTemplate = new DataTemplate();
            dataTemplate.VisualTree = dataTemplateFactoryPanel;
            listBox.ItemTemplate = dataTemplate;

            item.ScatterManipulationDelta += SviManipulationDelta;

            item.Content = dialogContainer.GetContectOfScatterViewItem(neutralColor, listBox);

            return item;
        }

        /// <summary>
        /// Prevents the ScatterViewItem from sliding out the reachable container. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviManipulationDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            if ((sender as ScatterViewItem).ActualCenter.X < 128)
            {
                (sender as ScatterViewItem).Center = new Point(128, (sender as ScatterViewItem).ActualCenter.Y);
            }
        }

    }
}
