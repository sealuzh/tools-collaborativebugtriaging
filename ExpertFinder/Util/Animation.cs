﻿using System;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Controls;

namespace ScrumSupporter.Util
{
    /// <summary>
    /// This is a Decrease and Increase animation of the text of a slice.
    /// </summary>
    public class Animation
    {
        public TimelineGroup GetTextAnimation(TextBlock text, TimelineGroup sb)
        {
            if (text != null)
            {
                if (sb != null)
                {
                    DoubleAnimation fontSizeAnimation = new DoubleAnimation();
                    fontSizeAnimation.From = text.FontSize;
                    fontSizeAnimation.To = text.FontSize - 4;
                    fontSizeAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
                    fontSizeAnimation.AutoReverse = true;

                    Storyboard.SetTarget(fontSizeAnimation, text);
                    Storyboard.SetTargetProperty(fontSizeAnimation, new PropertyPath(TextBlock.FontSizeProperty));

                    sb.Children.Add(fontSizeAnimation);

                    return sb;
                }
                else throw new ArgumentNullException("sb", "is null");
            }
            else throw new ArgumentNullException("text", "is null");
        }

    }
}
