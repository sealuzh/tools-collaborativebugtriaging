﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.Profile
{
    /// <summary>
    /// Interaktionslogik für ProfileEditingView.xaml
    /// </summary>
    public partial class ProfileEditingView : SurfaceUserControl
    {
        public ProfileEditingView()
        {
            InitializeComponent();
        }

        private void Done(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void imageUpload_Button_Click(object sender, RoutedEventArgs e)
        {
            UploadPicture_Dialog.Visibility = Visibility.Visible;
            UploadPicture_Dialog.DataContext = this.DataContext;


        }
    }
}
