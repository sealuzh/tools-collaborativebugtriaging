﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.WorkItemViews;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    public class TaskModel: WorkItemModel
    {
        private String desc;

        public String Description
        {
            get { return desc; }
            set { desc = value; }
        }

        private String activity;

        public String Activity
        {
            get { return activity; }
            set { activity = value; }
        }

        private int priority;

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        private double originalEstimate;

        public double OriginalEstimate
        {
            get { return originalEstimate; }
            set { originalEstimate = value; }
        }

        private double remaining;

        public double Remaining
        {
            get { return remaining; }
            set { remaining = value; }
        }

        private double completed;

        public double Completed
        {
            get { return completed; }
            set { completed = value; }
        }




        public override WorkItemViewModel GetViewModelType()
        {
            return new TaskViewModel(this);
        }

        /// <summary>
        /// Return the free text of the work item.
        /// </summary>
        /// <returns></returns>
        public override string GetText()
        {
            return this.Title + " " + this.Description;
        }


        public override WorkItem AdaptNewWorkItemToTfs(Project teamProject)
        {
            if (teamProject != null)
            {
                WorkItemType workItemType = teamProject.WorkItemTypes["Task"];
                WorkItem workItem = new WorkItem(workItemType);
                
                return SetTaskDetails(workItem);
            }
            else throw new ArgumentNullException("teamProject", "is null");
        }

        public override WorkItem AdaptExistingWorkItemToTfs(WorkItem workItem)
        {
            if (workItem != null)
            {
                return SetTaskDetails(workItem);
            }
            else throw new ArgumentNullException("workItem", "is null");
        }

        private WorkItem SetTaskDetails(WorkItem workItem)
        {
            workItem = SetCommonFields(workItem);
            workItem.Description = this.Description;

            AllowedValuesCollection allowedActivities = workItem.Fields["Activity"].AllowedValues;
            if (allowedActivities.Contains(this.Activity.ToString()))
            {
                workItem.Fields["Activity"].Value = this.Activity;
            }

            AllowedValuesCollection allowedPriorities = workItem.Fields["Priority"].AllowedValues;
            if (allowedPriorities.Contains(this.Priority.ToString()))
            {
                workItem.Fields["Priority"].Value = this.Priority;
            }

            AllowedValuesCollection allowedOriginalEstimates = workItem.Fields["Original Estimate"].AllowedValues;
            if (allowedOriginalEstimates.Contains(this.OriginalEstimate.ToString()))
            {
                workItem.Fields["Original Estimate"].Value = this.OriginalEstimate;
            }

            AllowedValuesCollection allowedRemainingWorks = workItem.Fields["Remaining Work"].AllowedValues;
            if (allowedRemainingWorks.Contains(this.Remaining.ToString()))
            {
                workItem.Fields["Remaining Work"].Value = this.Remaining;
            }

            AllowedValuesCollection allowedCompletedWorks = workItem.Fields["Completed Work"].AllowedValues;
            if (allowedCompletedWorks.Contains(this.Completed.ToString()))
            {
                workItem.Fields["Completed Work"].Value = this.Completed;
            }

            return workItem;
        }

    }
}
