﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// Interaktionslogik für ChangeView.xaml
    /// </summary>
    public partial class ChangeView : SurfaceUserControl
    {
        public ChangeView()
        {
            InitializeComponent();
        }

        private void SurfaceCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            (this.DataContext as ChangeViewModel).Ticked = true;
        }

        private void SurfaceCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            (this.DataContext as ChangeViewModel).Ticked = false;
        }
    }
}
