﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the team information of the collaboration platorm.
    /// </summary>
    public interface ITeamInformationService
    {
        /// <summary>
        /// Queries all events of the team which are associated to the project.
        /// </summary>
        /// <returns></returns>
        ICollection<UpcomingEvent> GetTeamEvents();
    }
}
