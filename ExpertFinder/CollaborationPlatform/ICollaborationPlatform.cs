﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the Collaboration Platform.
    /// </summary>
    public interface ICollaborationPlatform
    {
        /// <summary>
        /// Logs the user in to the collaboration platform. 
        /// </summary>
        /// <returns>true if the login passed and false if the login failed.</returns>
        bool Login(Credentials cred );

        /// <summary>
        /// Determines the username of the logged in person.
        /// </summary>
        /// <returns></returns>
        String GetLoggedInPerson();

        /// <summary>
        /// Determines if the collaboration platform is available. 
        /// </summary>
        /// <returns>true if the collaboration platform is available.</returns>
        bool IsCollaborationPlatformAvailable();

        /// <summary>
        /// Instatiates the service, which handles all functionalities
        /// related to work items.
        /// </summary>
        /// <returns>the workitem service of the collaboration platform.</returns>
        IWorkItemService GetWorkItemService();

        /// <summary>
        /// Instatiates the service, which handles all functionalities
        /// related to the iterations.
        /// </summary>
        /// <returns>the iteration service of the collaboration platform.</returns>
        IIterationService GetIterationService();

        /// <summary>
        /// Instatiates the service, which handles all functionalities
        /// related to profiles.
        /// </summary>
        /// <returns>the profile service of the collaboration platform.</returns>
        IProfileService GetProfileService();

        /// <summary>
        /// Instatiates the service, which handles all functionalities
        /// related to the team information.
        /// </summary>
        /// <returns>the team information service of the collaboration platform.</returns>
        ITeamInformationService GetTeamInformationService();

        /// <summary>
        /// Instatiates the service, which handles all functionalities
        /// related to the version control.
        /// </summary>
        /// <returns>the version control service of the collaboration platform.</returns>
        IVersionControlService GetVersionControlService();

    }
}
