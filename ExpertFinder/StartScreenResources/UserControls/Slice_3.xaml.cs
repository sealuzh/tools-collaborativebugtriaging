﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media.Animation;
using ExpertFinder.Profile;
using ExpertFinder.Util;

namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// Interaktionslogik für Slice_3.xaml
    /// </summary>
    public partial class Slice_3 : SurfaceUserControl
    {
        public Slice_3()
        {
            InitializeComponent();
        }

        private void slice3_ContactHoldGesture(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            //Animation
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sb_Completed);

            Animation ani = new Animation();
            sb = ani.getSliceAnimation(slice3, text3, sb);

            slice3.BeginStoryboard(sb);
        }

        /// <summary>
        /// EventHandler for the Completed event of the storyboard. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void sb_Completed(object sender, EventArgs e)
        {
            if (ConnectionToTFS.getInstance().IsRegistered() )
            {
                var profileContainerViewModel = new ProfileContainerViewModel();

                var view = profileContainerViewModel.CreateView();
                view.Show();
            }

        }
            
    }
}
