﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.Model
{
    public class SimilarWorkItemModel
    {

        public SimilarWorkItemModel(WorkItemModel model, double sim)
        {
            WorkItem = model;
            similarity = sim;
        }

        private WorkItemModel workItem;

        public WorkItemModel WorkItem
        {
            get { return workItem; }
            set { workItem = value; }
        }

        private double similarity;

        public double Similarity
        {
            get { return similarity; }
            set { similarity = value; }
        }
        
        
    }
}
