﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ExpertFinder.Dialogs;
using ExpertFinder.Planning;
using System.Windows;
using System.Threading;
using Microsoft.Surface.Presentation.Controls;
using ExpertFinder.Model;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using ExpertFinder.LocalTFS;
using ScrumSupporter.LoadProject;

namespace ExpertFinder.Util
{
    /// <summary>
    /// Workflow to register with the credentials. 
    /// </summary>
    public class LoaderWorkflow
    {

        private const String FILENAME = "outputfile.txt";
        private const String FILEUPLOADED = "uploaded.txt";
        private ScatterView Container;

        private Dialog_LoadDataLocally dialog_LoadDataLocally;

        public void start(ScatterView Container)
        {

            this.Container = Container;
            this.dialog_LoadDataLocally = null;



            if (IsLastUploadSuccessful() == true)
            {
                //try to load data from TFS
                LoadDataFromTFS();
            }
            else
            {
                //Load data locally and say it to the user
                LoadUserStoryDataLocally();
            }


        }

        /// <summary>
        /// Determines if the last upload to TFS was successful.
        /// </summary>
        /// <returns>true, if the last upload was successful.</returns>
        private Boolean IsLastUploadSuccessful()
        {
            bool result = false;

            IList<UploadingResult> results = Database.getInstance().DB.Query<UploadingResult>();

            foreach (UploadingResult r in results)
            {
                result = r.UploadedSuccessfully;
            }

            return result;

            //try
            //{
            //    String successful;
            //    using (StreamReader infile = new StreamReader(FILEUPLOADED))
            //    {
            //        successful = infile.ReadLine();
            //    }
            //    if (successful.Equals("1"))
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch (Exception e)
            //{
            //    System.Console.WriteLine(e.Message);
            //    return false;
            //}

        }

        private void LoadDataFromTFS()
        {
            if (ConnectionToTFS.getInstance().IsRegistered() != true)
            {
                //Show enter credentials view
                showEnterCredentialsView();
            }
            else
            {
                GetDataFromTFS();
            }

        }

        private void showEnterCredentialsView()
        {
            EnterCredentialsViewModel credViewmodel = new EnterCredentialsViewModel(null);
            EnterCredentialsView credView = new EnterCredentialsView();
            credView.DataContext = credViewmodel;

           // credView.CloseButton_EnterCredentialsView.Click += OnClose;


            ScatterViewItem switem = new ScatterViewItem();
            switem.Content = credView;
            switem.MinHeight = 440;
            switem.MinWidth = 580;

            Container.Items.Add(switem);
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            if ((sender as SurfaceButton).Name.Equals("CloseButton_dialog_LoadDataLocally"))
            {
                Container.Items.RemoveAt(0);
                Boolean loaded = LocalDataLoader.load(Datatypes.UserStory);
                if (loaded == false)
                {
                    Dialog_UnableToOpenLocalSource dialog_UnableToOpenLocalSource = new Dialog_UnableToOpenLocalSource();
                    dialog_UnableToOpenLocalSource.CloseButton.Click += OnClose_UnableToOpenLocalSource;
                    ScatterViewItem switem_unable = new ScatterViewItem();
                    switem_unable.Content = dialog_UnableToOpenLocalSource;
                    switem_unable.MinHeight = 300;
                    switem_unable.MinWidth = 500;

                    Container.Items.Add(switem_unable);

                }
            }
            else if ((sender as SurfaceButton).Name.Equals("CloseButton_EnterCredentialsView"))
            {
                Container.Items.RemoveAt(0);
            }
        }

        private void OnClose_UnableToOpenLocalSource(object sender, RoutedEventArgs e)
        {
            if (Container.Items.Count > 0)
            {
                Container.Items.RemoveAt(0);
            }
        }

        private void GetDataFromTFS()
        {
            if (ConnectionToTFS.getInstance().MyTfs.IsConnectionEstablished())
            {
                //get the data from TFS

                var userStoryContainerViewModel = new WorkItemContainerViewModel();
                var lst = new List<WorkItemModel>();

                Collection<WorkItem> workItems = ConnectionToTFS.getInstance().MyTfs.getUserStories();

                foreach (WorkItem w in workItems)
                {
                    double rank = 0;
                    String assignedTo = "";
                    double points = 0;
                    String risk = "";
                    if (w.Fields["Stack Rank"].Value != null)
                    {
                        rank = (Double)w.Fields["Stack Rank"].Value;
                    }
                    if (w.Fields["Assigned To"].Value != null)
                    {
                        assignedTo = (String)w.Fields["Assigned To"].Value;
                    }
                    if (w.Fields["Story Points"].Value != null)
                    {
                        points = (double)w.Fields["Story Points"].Value;
                    }
                    if (w.Fields["Risk"].Value != null)
                    {
                        risk = (String)w.Fields["Risk"].Value;
                    }

                    lst.Add(CreateNewWorkItem(w.Id, w.Title, rank, w.Description, assignedTo, w.State, w.Reason, points, risk, w.AreaPath, w.IterationPath));
                }
                userStoryContainerViewModel.LoadData();
                var view = userStoryContainerViewModel.CreateView();
                view.Show();
            }
            else
            {
                //TFS is not available. Show dialog to load data locally.
                dialog_LoadDataLocally = new Dialog_LoadDataLocally();

                dialog_LoadDataLocally.CloseButton_dialog_LoadDataLocally.Click += OnClose;

                ScatterViewItem switem_loadLocally = new ScatterViewItem();
                switem_loadLocally.Content = dialog_LoadDataLocally;
                switem_loadLocally.MinHeight = 300;
                switem_loadLocally.MinWidth = 500;

                Container.Items.Add(switem_loadLocally);
            }
        }

        private WorkItemModel CreateNewWorkItem(int id, string title, double rank, string desc, String assignedTo, String state,
            String reason, double storypoints, String risk, String area, String iteration)
        {
            return new UserStoryModel
            {
                Id = id,
                Title = title,
                StackRank = rank,
                Description = desc,
                AssignedTo = assignedTo,
                State = state,
                Reason = reason,
                StoryPoints = storypoints,
                Risk = risk,
                Area = area,
                Iteration = iteration
            };
        }

        /// <summary>
        /// Load the User Stories from a local source.
        /// </summary>
        private void LoadUserStoryDataLocally()
        {
            dialog_LoadDataLocally = new Dialog_LoadDataLocally();

            dialog_LoadDataLocally.CloseButton_dialog_LoadDataLocally.Click += OnClose;

            ScatterViewItem switem_loadLocally = new ScatterViewItem();
            switem_loadLocally.Content = dialog_LoadDataLocally;
            switem_loadLocally.MinHeight = 300;
            switem_loadLocally.MinWidth = 500;

            Container.Items.Add(switem_loadLocally);


        }

    }
}
