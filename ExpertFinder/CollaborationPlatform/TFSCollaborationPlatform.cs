﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TfsLink;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Provides the implementation of the interaction with the Team Foundation Server.
    /// </summary>
    public class TFSCollaborationPlatform: ICollaborationPlatform
    {
        private ITfs tfsConnection;
        private String userName;

        public bool Login(Credentials cred)
        {
            if (cred != null)
            {
                userName = cred.Username;
                this.tfsConnection = new Tfs(cred.Username, cred.Password, cred.Domain, new Uri(cred.Uri), cred.ProjectName);
                return tfsConnection.IsConnectionEstablished();
            }
            else
            {
                return false;
            }
        }

        public bool IsCollaborationPlatformAvailable()
        {
            return tfsConnection.IsTFSAvailable();
        }

        public IWorkItemService GetWorkItemService()
        {
            IWorkItemService workItemService = new TFSWorkItemService(tfsConnection);
            return workItemService;
        }

        public IIterationService GetIterationService()
        {
            IIterationService iterationService = new TFSIterationService(tfsConnection);
            return iterationService;
        }

        public IProfileService GetProfileService()
        {
            IProfileService profileService = new TFSProfileService(tfsConnection);
            return profileService;
        }

        public ITeamInformationService GetTeamInformationService()
        {
            ITeamInformationService teamInformationService = new TFSTeamInformationService(tfsConnection);
            return teamInformationService;
        }

        public IVersionControlService GetVersionControlService()
        {
            IVersionControlService versionControlService = new TFSVersionControlService(tfsConnection);
            return versionControlService;
        }


        public string GetLoggedInPerson()
        {
            return userName;
        }
    }
}
