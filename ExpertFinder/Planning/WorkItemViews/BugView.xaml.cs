﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Includes event handler to switch between the mini view and the editing view. 
    /// </summary>
    public partial class BugView : SurfaceUserControl
    {
        public BugView()
        {
            InitializeComponent();
           
        }

        /// <summary>
        /// According to the size of the user control, the mini view or the editing view of the work item is displayed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private new void SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (this.DataContext as WorkItemViewModel).OnSizeOfViewChanged(e, editView, miniView);
        }

        public void DisableAnalyzeButton()
        {
            editView.AnalyzeButton.Visibility = Visibility.Hidden;
        }
    }
}
