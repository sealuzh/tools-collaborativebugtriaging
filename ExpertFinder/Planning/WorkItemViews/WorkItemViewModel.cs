﻿using System;
using UZH.Infrastructure;
using System.Windows;
using System.Windows.Input;
using Microsoft.Surface.Presentation;
using System.Windows.Ink;
using System.IO;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Triaging;
using ScrumSupporter.Util;
using System.Windows.Media;


namespace ScrumSupporter.Planning
{
    /// <summary>
    /// The ViewModel of the UserStoryView. 
    /// </summary>
    public abstract class WorkItemViewModel : BaseViewModel
    {
        private WorkItemModel model;

        public WorkItemModel GetModel()
        {
            return model;
        }

        public WorkItemViewModel(WorkItemModel workItemModel)
        {
            this.model = workItemModel;
        }

        private ScatterView triagingContainer;

        public ScatterView TriagingContainer
        {
            get { return triagingContainer; }
            set { triagingContainer = value; }
        }
        

        public String Title
        {
            get
            {
                return model.Title;
            }
            set
            {
                model.Title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        public double StackRank
        {
            get
            {
                return model.StackRank;
            }
            set
            {
                model.StackRank = value;
                NotifyOfPropertyChange(() => StackRank);
            }
        }


        public int Id
        {
            get
            {
                return model.Id;
            }
            set
            {
                model.Id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public String AssignedTo
        {
            get
            {
                return model.AssignedTo;
            }
            set
            {
                model.AssignedTo = value;
                NotifyOfPropertyChange(() => AssignedTo);
            }
        }

        public String State
        {
            get
            {
                return model.State;
            }
            set
            {
                model.State = value;
                NotifyOfPropertyChange(() => State);
            }
        }

        public String Reason
        {
            get
            {
                return model.Reason;
            }
            set
            {
                model.Reason = value;
                NotifyOfPropertyChange(() => Reason);
            }
        }

        public String Area
        {
            get
            {
                return model.Area;
            }
            set
            {
                model.Area = value;
                NotifyOfPropertyChange(() => Area);
            }
        }

        public String Iteration
        {
            get
            {
                return model.Iteration;
            }
            set
            {
                model.Iteration = value;
                NotifyOfPropertyChange(() => Iteration);
            }
        }

        public object DraggedElement
        {
            get;
            set;
        }

        public Point GetPositionInBacklog(Random random)
        {
            if (random != null)
            {
                return CalculatePosition(random);
            }
            else
            {
                Random r = new Random();
                return CalculatePosition(r);
            }
        }


        protected abstract Point CalculatePosition(Random random);

        /// <summary>
        /// Looks up the DataTemplate from the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public abstract DataTemplate GetTemplate();

        /// <summary>
        /// Looks up the DataTemplate for the singleIteration view in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public abstract DataTemplate GetTemplateForSingleIterationView();

        /// <summary>
        /// Looks up the style of the cursor from the Dictionary.
        /// </summary>
        /// <returns></returns>
        public ContentControl GetCursorVisual()
        {
            ContentControl cursorVisual = new ContentControl()
            {
                Content = this,
            };
            cursorVisual.Style = GetStyleOfCursorVisual();
            return cursorVisual;
        }

        protected abstract Style GetStyleOfCursorVisual();

        /// <summary>
        /// Creates the EditingView in a Viewbox.
        /// </summary>
        /// <returns></returns>
        public Viewbox GetEditingView()
        {
            SurfaceUserControl editingView = GetSpecificEditingView();
            editingView.DataContext = this;
            Viewbox viewbox = new Viewbox();
            viewbox.Child = editingView;
            viewbox.Stretch = Stretch.UniformToFill;
            return viewbox;
        }

        protected abstract SurfaceUserControl GetSpecificEditingView();

        /// <summary>
        /// Creates a view of the model in a ScatterViewItem.
        /// </summary>
        /// <returns></returns>
        public abstract ScatterViewItem GetView();

        protected ScatterViewItem GetScatterViewItemConfigsOfTheView()
        {
            ScatterViewItem svi = new ScatterViewItem();

            svi.Height = 350 * 1.2;
            svi.Width = 400 * 1.2;
            svi.Orientation = 0;
            svi.Center = new Point(Constants.BACKLOGWIDTH / 2.0, Constants.BACKLOGHEIGHT / 2.0);
            svi.ShowsActivationEffects = false;

            return svi;
        }

        /// <summary>
        /// According to the size of the user control, the mini view or the editing view of the work item is displayed. 
        /// </summary>
        /// <param name="e"></param>
        /// <param name="editView"></param>
        /// <param name="miniView"></param>
        public void OnSizeOfViewChanged(SizeChangedEventArgs e, FrameworkElement editView, UIElement miniView)
        {
            if (e != null)
            {
                if (editView != null)
                {
                    if (miniView != null)
                    {
                        double width = editView.ActualWidth * (2.0 / 3.0);
                        double height = editView.ActualHeight * (2.0 / 3.0);
                        Size bigSize = new Size(width, height);
                        if (e.NewSize.Height > bigSize.Height && e.NewSize.Width > bigSize.Width)
                        {
                            editView.Visibility = Visibility.Visible;
                            miniView.Visibility = Visibility.Hidden;
                        }
                        else if (e.NewSize.Height < bigSize.Height && e.NewSize.Width < bigSize.Width)
                        {
                            editView.Visibility = Visibility.Hidden;
                            miniView.Visibility = Visibility.Visible;
                        }
                    }
                    else throw new ArgumentNullException("miniView", "is null");
                }
                else throw new ArgumentNullException("editView", "is null");
            }
            else throw new ArgumentNullException("e", "is null");
        }

        /// <summary>
        /// Opens the Triaging View in a ScatterViewItem.
        /// </summary>
        /// <param name="style"></param>
        public void OnOpenTriaging(Style style)
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;
            if (platform.IsCollaborationPlatformAvailable())
            {
                try
                {
                    TriagingContainerViewModel triagingModel = new TriagingContainerViewModel(this, platform);
                    ScatterViewItem sviTriagingView = triagingModel.GetViewWrappedInScatterView(style, TriagingContainer);
                    TriagingContainer.Items.Add(sviTriagingView);

                }
                catch (ArgumentNullException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    //Show dialog
                }
            }
        }
    }
}
