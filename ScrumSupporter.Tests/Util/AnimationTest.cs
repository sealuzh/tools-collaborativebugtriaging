﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using ScrumSupporter.Util;

namespace ScrumSupporter.Tests.Util
{
    [TestFixture]
    public class AnimationTest
    {
        private Animation anim;

        [TestFixtureSetUp]
        public void SetUp()
        {
            anim = new Animation();
        }

        [Test]
        public void GetTextAnimationTestTextNull()
        {
            Assert.Throws(typeof(ArgumentNullException), new TestDelegate(GetTextNullAnimation));
        }

        private void GetTextNullAnimation()
        {
            TimelineGroup tg = new Storyboard();
            anim.GetTextAnimation(null, tg);
        }

        [STAThread]
        [Test]
        public void GetTextAnmiationTimelineGroupNull()
        {
            Assert.Throws(typeof(ArgumentNullException), new TestDelegate(GetTimelineGroupNullAnimation));
        }

        private void GetTimelineGroupNullAnimation()
        {
            TextBlock text = new TextBlock();
            anim.GetTextAnimation(text, null);
        }


    }
}
