﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;


namespace ScrumSupporter
{
    public class VectorSpaceModel
    {
        private HashSet<string> VectorSpace = new HashSet<string>(); 
        private double N; // total number of documents

        //all words per work item
        private Dictionary<WorkItemModel, IEnumerable<string>> workItems  = new Dictionary<WorkItemModel, IEnumerable<string>>();

        //the document vectors of all work items
        //Collection<double> contains the weights of each term in the VectorSpace
        private Dictionary<WorkItemModel, ICollection<double>> DocumentVectors = new Dictionary<WorkItemModel, ICollection<double>>();


        public Dictionary<WorkItemModel, ICollection<Double>> GetFeatureVectors(ICollaborationPlatform platform)
        {
            PreprocessTextOfWorkItems(platform);
            GenerateDocumentVectors();

            return DocumentVectors;
        }


        /// <summary>
        /// Downloads all work items and preprocesses the text of the title and the description of every work item 
        /// and saves the words in a Dictionary and the VectorSpace is created.
        /// </summary>
        private void PreprocessTextOfWorkItems(ICollaborationPlatform platform)
        {
                ICollection<WorkItemModel> allWorkItems = platform.GetWorkItemService().GetAllWorkItems();
                //total number of documents
                N = allWorkItems.Count;

                TextProcessor textProcessor = new TextProcessor();
                foreach (WorkItemModel item in allWorkItems)
                {
                    string text = item.GetText();
                    IEnumerable<string> allWordsFromWorkItem = textProcessor.GetTokens(text);
                    foreach (String s in allWordsFromWorkItem)
                    {
                        VectorSpace.Add(s);
                    }
                    workItems.Add(item, allWordsFromWorkItem);
                }

                System.Console.WriteLine("VEctorSpace count: "+VectorSpace.Count);
        }

        /// <summary>
        /// Creates for each work item a weighted document vector.
        /// </summary>
        private void GenerateDocumentVectors()
        {
            //iterates through all work items
            foreach (KeyValuePair<WorkItemModel, IEnumerable<string>> entry in workItems)
            {
                //calculates the term frequency of all terms in a work item
                Dictionary<string, int> termFrequencies = CalculateTermFrequencies(entry.Value);
                Dictionary<string, double> weightedTerms = new Dictionary<string, double>();
                
                //calculate for each unique term of a work item the document frequency
                foreach (KeyValuePair<string, int> term in termFrequencies)
                {
                    weightedTerms.Add(term.Key, CalculateTfIdf(term.Value, term.Key, false));
                }
                DocumentVectors.Add(entry.Key, SetDocumentVector(weightedTerms));
            }
        }

        private Collection<double> SetDocumentVector(Dictionary<string, double> weightedTerms)
        {
            Collection<double> DocumentVector = new Collection<double>();
            foreach (string term in VectorSpace)
            {
                //if weightedTerms contains the entry take the weight, else take 0
                double value;
                if (weightedTerms.TryGetValue(term, out value))
                {
                    DocumentVector.Add(value);
                }
                else
                {
                    DocumentVector.Add(0);
                }
            }
            return DocumentVector;
        }

        private double CalculateTfIdf(int termFreq, String term, bool isNew)
        {
            int documentFrequency = CalculateDocumentFrequency(term);
            documentFrequency = isNew == true ? documentFrequency += 1 : documentFrequency;


            //idf = log(N/df)
            N = isNew == true ? N += 1 : N;
            double inverseDocumentFrequency = Math.Log(N / documentFrequency);

            //weight = tf * idf
            double weight = termFreq * inverseDocumentFrequency;
            return weight;
        }


        /// <summary>
        /// Calculates the number of of documents that contain the term.
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        private int CalculateDocumentFrequency(string term)
        {
            int counter = 0;

            foreach (KeyValuePair<WorkItemModel, IEnumerable<string>> entry in workItems)
            {
                if(entry.Value.Contains(term))
                {
                    counter ++;
                }
            }
            return counter;
        }

        /// <summary>
        /// Counts the occurences of the terms in a given collection of terms. 
        /// </summary>
        /// <param name="allTerms"></param>
        /// <returns></returns>
        private Dictionary<string, int> CalculateTermFrequencies(IEnumerable<string> allTerms)
        {
            Dictionary<string, int> termCount = new Dictionary<string, int>();

            foreach (string s in allTerms)
            {
                if (!termCount.ContainsKey(s))
                {
                    termCount.Add(s, 1);
                }
                else
                {
                    termCount[s]++;
                }
            }
            return termCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public Collection<double> GetFeatureVectorOfNewWorkItem(WorkItemModel model)
        {
            if (model != null)
            {
                Collection<double> FeatureVectorFromText = new Collection<double>();
                TextProcessor textProcessor = new TextProcessor();

                string text = model.GetText();
                IEnumerable<string> allWordsFromWorkItem = textProcessor.GetTokens(text);

                //get words according to the vector space
                ICollection<string> wordsInVectorSpace = GetWordsInVectorSpace(allWordsFromWorkItem);

                //calculates the term frequency of all terms in a work item
                Dictionary<string, int> termFrequencies = CalculateTermFrequencies(wordsInVectorSpace);
                Dictionary<string, double> weightedTerms = new Dictionary<string, double>();

                //calculate for each unique term of a work item the document frequency
                foreach (KeyValuePair<string, int> term in termFrequencies)
                {
                    weightedTerms.Add(term.Key, CalculateTfIdf(term.Value, term.Key, true));
                }
                return SetDocumentVector(weightedTerms);
            }
            else throw new ArgumentNullException("model", "is null");
        }

        private ICollection<string> GetWordsInVectorSpace(IEnumerable<string> wordsOfNewWorkItem)
        {
            //are the words in the vector space? If not try to find synonyms, if no
            //synonyms are found drop the word.

            List<string> words = wordsOfNewWorkItem.ToList();
            bool synonymFound = false;
            foreach (string word in words)
            {

                if (!VectorSpace.Contains(word))
                {
                    SynonymFinder synFinder = new SynonymFinder();
                    IList<String> synonyms = synFinder.FindSynonym(word);
                    string foundSynonym = "";
                    foreach (String synonym in synonyms)
                    {
                        if (VectorSpace.Contains(synonym))
                        {
                            synonymFound = true;
                            foundSynonym = synonym;
                            break;
                        }
                    }

                    if (synonymFound)
                    {
                        words.Remove(word);
                        words.Add(foundSynonym);
                    }
                }
                synonymFound = false;
            }

            return words;
        }


    }
}
