﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.Exceptions
{
    /// <summary>
    /// This Exception is thrown when two vectors do not have the same length.
    /// </summary>
    [Serializable]
    public class InnerVectorProductException : System.Exception
    {
        public InnerVectorProductException() { }
        public InnerVectorProductException(string message):base(message) { }
        public InnerVectorProductException(string message, System.Exception inner):base(message, inner) { }

        // Constructor needed for serialization 
        protected InnerVectorProductException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context){ }
    }
}
