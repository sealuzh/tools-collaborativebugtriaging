﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    /// <summary>
    /// The model of the Profile. 
    /// </summary>
    public class ProfileModel
    {
        private Dictionary<WorkItemModel, int> connectedWorkItems;

        public Dictionary<WorkItemModel, int> ConnectedWorkItems
        {
            get { return connectedWorkItems; }
        }

        public ProfileModel()
        {
            connectedWorkItems = new Dictionary<WorkItemModel, int>();
        }

        private String tfsAccountName;

        public String TfsAccountName
        {
            get { return tfsAccountName; }
            set { tfsAccountName = value; }
        }

        private String emailAddress = "katja.kevic@uzh.ch";

        public String EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        

        public override int GetHashCode()
        {
            return TfsAccountName.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as ProfileModel);
        }
        public bool Equals(ProfileModel obj)
        {
            return obj != null && obj.TfsAccountName == this.TfsAccountName;
        }
        

    }
}
