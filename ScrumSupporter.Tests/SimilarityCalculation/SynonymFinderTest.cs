﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class SynonymFinderTest
    {
        private SynonymFinder finder;

        [TestFixtureSetUp]
        public void SetUp()
        {
            finder = new SynonymFinder();
        }

        [Test]
        public void FindSynonymTest()
        {
            IList<String> syn = finder.FindSynonym("car");
            CollectionAssert.Contains(syn, "auto");
        }

    }
}
