﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.Filters;
using ScrumSupporter.Model;
using ScrumSupporter.Planning.WorkItemViews;


namespace ScrumSupporter.Tests.Planning.Filters
{
    [TestFixture]
    public class FilterViewModelTest
    {

        private ICollection<WorkItemViewModel> items;
        private FilterViewModel filterViewModel;

        private WorkItemViewModel bugViewModelState;
        private WorkItemViewModel taskViewModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            items = new List<WorkItemViewModel>();
            filterViewModel = new FilterViewModel();
            MockWorkItemViewModels();
        }

        private void MockWorkItemViewModels()
        {
            WorkItemModel bugModel = new BugModel() { AssignedTo = "", State = "Active" };
            WorkItemViewModel bugViewModel = new BugViewModel(bugModel);
            items.Add(bugViewModel);

            WorkItemModel taskModel = new TaskModel() { AssignedTo = "", State = "Active"};
            taskViewModel = new TaskViewModel(taskModel);
            items.Add(taskViewModel);

            WorkItemModel issueModel = new IssueModel() { AssignedTo = "", State = "Active"};
            WorkItemViewModel issueViewModel = new IssueViewModel(issueModel);
            items.Add(issueViewModel);

            WorkItemModel userStoryModel = new UserStoryModel() { AssignedTo = "", State = "Active"};
            WorkItemViewModel userStoryViewModel = new UserStoryViewModel(userStoryModel);
            items.Add(userStoryViewModel);

            WorkItemModel bugModelAssigned = new BugModel() { AssignedTo = "kevic", State = "Active"};
            WorkItemViewModel bugViewModelAssigned = new BugViewModel(bugModelAssigned);
            items.Add(bugViewModelAssigned);

            WorkItemModel taskModelAssigned = new TaskModel() { AssignedTo = "kevic", State = "Active" };
            WorkItemViewModel taskViewModelAssigned = new TaskViewModel(taskModelAssigned);
            items.Add(taskViewModelAssigned);

            WorkItemModel issueModelAssigned = new IssueModel() { AssignedTo = "kevic", State = "Active"};
            WorkItemViewModel issueViewModelAssigned = new IssueViewModel(issueModelAssigned);
            items.Add(issueViewModelAssigned);

            WorkItemModel userStoryModelAssigned = new UserStoryModel() { AssignedTo = "kevic", State = "Active"};
            WorkItemViewModel userStoryViewModelAssigned = new UserStoryViewModel(userStoryModelAssigned);
            items.Add(userStoryViewModelAssigned);

            WorkItemModel bugModelState = new BugModel() { AssignedTo = "", State = "Closed"};
            bugViewModelState = new BugViewModel(bugModelState);
            items.Add(bugViewModelState);

            WorkItemModel taskModelState = new TaskModel() { AssignedTo = "", State = "Closed" };
            WorkItemViewModel taskViewModelState = new TaskViewModel(taskModelState);
            items.Add(taskViewModelState);

            WorkItemModel issueModelState = new IssueModel() { AssignedTo = "", State = "Closed" };
            WorkItemViewModel issueViewModelState = new IssueViewModel(issueModelState);
            items.Add(issueViewModelState);

            WorkItemModel userStoryModelState = new UserStoryModel() { AssignedTo = "", State = "Closed" };
            WorkItemViewModel userStoryViewModelState = new UserStoryViewModel(userStoryModelState);
            items.Add(userStoryViewModelState);

            WorkItemModel bugModelStateResolved = new BugModel() { AssignedTo = "", State = "Resolved" };
            WorkItemViewModel bugViewModelStateResolved = new BugViewModel(bugModelStateResolved);
            items.Add(bugViewModelStateResolved);

            WorkItemModel taskModelStateResolved = new TaskModel() { AssignedTo = "", State = "Resolved" };
            WorkItemViewModel taskViewModelStateResolved = new TaskViewModel(taskModelStateResolved);
            items.Add(taskViewModelStateResolved);

            WorkItemModel issueModelStateResolved = new IssueModel() { AssignedTo = "", State = "Resolved" };
            WorkItemViewModel issueViewModelStateResolved = new IssueViewModel(issueModelStateResolved);
            items.Add(issueViewModelStateResolved);

            WorkItemModel userStoryModelStateResolved = new UserStoryModel() { AssignedTo = "", State = "Resolved" };
            WorkItemViewModel userStoryViewModelStateResolved = new UserStoryViewModel(userStoryModelStateResolved);
            items.Add(userStoryViewModelStateResolved);
        }

        #region work item type filter options

        private void OnlyBugSelected()
        {
            filterViewModel.IsBugChecked = true;
            filterViewModel.IsTaskChecked = false;
            filterViewModel.IsUserStoryChecked = false;
            filterViewModel.IsIssueChecked = false;
        }

        private void OnlyTaskSelected()
        {
            filterViewModel.IsTaskChecked = true;
            filterViewModel.IsIssueChecked = false;
            filterViewModel.IsUserStoryChecked = false;
            filterViewModel.IsBugChecked = false;
        }

        private void OnlyIssueSelected()
        {
            filterViewModel.IsTaskChecked = false;
            filterViewModel.IsIssueChecked = true;
            filterViewModel.IsUserStoryChecked = false;
            filterViewModel.IsBugChecked = false;
        }

        private void OnlyUserStorySelected()
        {
            filterViewModel.IsTaskChecked = false;
            filterViewModel.IsIssueChecked = false;
            filterViewModel.IsUserStoryChecked = true;
            filterViewModel.IsBugChecked = false;
        }

        private void NoTypeSelected()
        {
            filterViewModel.IsTaskChecked = false;
            filterViewModel.IsIssueChecked = false;
            filterViewModel.IsUserStoryChecked = false;
            filterViewModel.IsBugChecked = false;
        }

        private void AllTypesSelected()
        {
            filterViewModel.IsTaskChecked = true;
            filterViewModel.IsIssueChecked = true;
            filterViewModel.IsUserStoryChecked = true;
            filterViewModel.IsBugChecked = true;
        }
        #endregion

        #region work item state filter options

        private void OnlyActiveSelected()
        {
            filterViewModel.IsActiveChecked = true;
            filterViewModel.IsClosedChecked = false;
            filterViewModel.IsResolvedChecked = false;
        }


        private void OnlyClosedSelected()
        {
            filterViewModel.IsActiveChecked = false;
            filterViewModel.IsClosedChecked = true;
            filterViewModel.IsResolvedChecked = false;
        }

        private void OnlyResolvedSelected()
        {
            filterViewModel.IsActiveChecked = false;
            filterViewModel.IsClosedChecked = false;
            filterViewModel.IsResolvedChecked = true;
        }

        #endregion

        #region work item type assigned to filter options

        private void AssignedToNobodySelected()
        {
            filterViewModel.IsAssignedToNobodyChecked = true;
        }

        private void AssignedToNobodyNotSelected()
        {
            filterViewModel.IsAssignedToNobodyChecked = false;
        }

        #endregion

        #region work item type tests

        [Test]
        public void FilterIsBugSelectedTest()
        {
            OnlyBugSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(BugViewModel));
        }

        [Test]
        public void FilterIsTaskSelectedTest()
        {
            OnlyTaskSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(TaskViewModel));
        }

        [Test]
        public void FilterIsIssueSelected()
        {
            OnlyIssueSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(IssueViewModel));
        }

        [Test]
        public void FilterIsUserStorySelected()
        {
            OnlyUserStorySelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(UserStoryViewModel));
        }

        [Test]
        public void FilterAllTypesSelected()
        {
            AllTypesSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(WorkItemViewModel));
        }

        #endregion

        #region work item state tests

        [Test]
        public void FilterIsActiveStateSelectedTest()
        {
            OnlyActiveSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);

            foreach (WorkItemViewModel model in result)
            {
                Assert.AreEqual("Active", model.State);
            }
        }

        [Test]
        public void FilterIsClosedStateSelectedTest()
        {
            OnlyClosedSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);

            foreach (WorkItemViewModel model in result)
            {
                Assert.AreEqual("Closed", model.State);
            }
        }

        [Test]
        public void FilterIsResolvedStateSelectedTest()
        {
            OnlyResolvedSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);

            foreach (WorkItemViewModel model in result)
            {
                Assert.AreEqual("Resolved", model.State);
            }
        }


        #endregion

        #region work item assigned to tests

        [Test]
        public void FilterIsAssignedToSelectedTest()
        {
            AssignedToNobodySelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            CollectionAssert.AllItemsAreNotNull(result);
            int initialCount = items.Count;
            int finalCount = result.Count;
            Assert.GreaterOrEqual(initialCount, finalCount);

            foreach (WorkItemViewModel model in result)
            {
                Assert.AreEqual("", model.AssignedTo);
            }
        }

        [Test]
        public void FilterIsAssignedToNotSelectedTest()
        {
            AllTypesSelected();
            OnlyActiveSelected();
            AssignedToNobodyNotSelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            Assert.AreEqual(8, result.Count);
        }

        #endregion

        [Test]
        public void FilterCombinationBugClosedAssignedToNobodyTest()
        {
            OnlyBugSelected();
            OnlyClosedSelected();
            AssignedToNobodySelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            Assert.AreEqual(1, result.Count);
            CollectionAssert.Contains(result, bugViewModelState);
        }

        [Test]
        public void FilterCombinationTaskActiveAssignedToNobodyTest()
        {
            OnlyTaskSelected();
            OnlyActiveSelected();
            AssignedToNobodySelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);
            Assert.AreEqual(1, result.Count);
            CollectionAssert.Contains(result, taskViewModel);
        }

        [Test]
        public void FilterCombinationAllNotAssignedToNobodyTest()
        {
            AllTypesSelected();
            OnlyActiveSelected();
            AssignedToNobodySelected();
            ICollection<WorkItemViewModel> result = filterViewModel.Filter(items);

            Assert.AreEqual(4, result.Count);
        }
    }
}
