﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the work items of the collaboration platorm.
    /// </summary>
    public interface IWorkItemService
    {
        WorkItemModel GetWorkItem(int id);

        /// <summary>
        /// Queries the work items, which are assigned to an iteration. Sorted by stack rank. 
        /// </summary>
        /// <param name="iterationName"></param>
        /// <returns>A collection in which the entries are sorted by Stack Rank in descending order.</returns>
        ICollection<WorkItemModel> GetWorkItemsOfIteration(string iterationName);

        /// <summary>
        /// Queries all work items, which are in the backlog of the project.
        /// </summary>
        /// <returns></returns>
        ICollection<WorkItemModel> GetBacklogWorkItems();

        /// <summary>
        /// Queries all work items, which are associated to the project.
        /// </summary>
        /// <returns></returns>
        ICollection<WorkItemModel> GetAllWorkItems();

        /// <summary>
        /// Queries all work items, which are assigned to the developer.
        /// </summary>
        /// <param name="developersName"></param>
        /// <returns></returns>
        ICollection<WorkItemModel> GetWorkItemsAssignedTo(String developersName);

        /// <summary>
        /// Uploads the work items to the collaboration platform.
        /// </summary>
        /// <param name="workItems"></param>
        /// <returns></returns>
        bool UploadWorkItems(ICollection<WorkItemModel> workItems);

        /// <summary>
        /// Queries all work items which contain the given queryString in the title.
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        ICollection<WorkItemModel> GetWorkItemsContainingString(String queryString);

    }
}
