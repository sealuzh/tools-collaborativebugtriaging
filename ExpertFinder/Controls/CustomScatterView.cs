﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface.Presentation.Controls;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections;
using ExpertFinder.Planning;
using System.Collections.ObjectModel;

namespace ExpertFinder.Controls
{
    public class CustomScatterView : ScatterView, INotifyPropertyChanged
    {
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(CustomScatterView), new UIPropertyMetadata(null));

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<UserStoryViewModel>), typeof(CustomScatterView), new UIPropertyMetadata(null, ItemsSourceChangedCallback));

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public new ObservableCollection<UserStoryViewModel> ItemsSource
        {
            get { return (ObservableCollection<UserStoryViewModel>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        public virtual void NotifyOfPropertyChange(object sender, string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        protected static void ItemsSourceChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var itemSource = e.NewValue as IEnumerable;
            var parent = d as CustomScatterView;

            parent.AttachEvents();

            (parent as ScatterView).ItemsSource = parent.ItemsSource;

            if (itemSource != null)
            {
                foreach (var value in itemSource.OfType<UserStoryViewModel>())
                {
                    value.Container = parent;
                }
            }
        }

        private void AttachEvents()
        {
            if (ItemsSource == null) return;

            ItemsSource.CollectionChanged -= ItemsSourceCollectionChanged;
            ItemsSource.CollectionChanged += ItemsSourceCollectionChanged;
        }

        public void ItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var value in e.NewItems.OfType<UserStoryViewModel>())
                {
                    value.Container = this;
                }
            }
        }

        public void SetSelectedItem(FrameworkElement value)
        {
            
            if (SelectedItem == value.DataContext) return;

            SelectedItem = value.DataContext;
            NotifyOfPropertyChange(this, "SelectedItem");
        }
    }
}
