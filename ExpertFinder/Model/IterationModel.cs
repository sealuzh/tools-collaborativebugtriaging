﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.Model
{
    public class IterationModel
    {
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        private IterationModel childNode;

        public IterationModel ChildNode
        {
            get { return childNode; }
            set { childNode = value; }
        }
        
        
    }
}
