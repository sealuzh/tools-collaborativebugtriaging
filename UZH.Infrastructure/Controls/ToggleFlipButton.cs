// --------------------------------------------------------------------------------------------------------------------- 
// <summary>
//   Defines the ToggleFlipButton class.
// </summary>
// ---------------------------------------------------------------------------------------------------------------------

#region Change History
// YYYY-MM-DD Author: Change description
#endregion

using System.Windows;
using System.Windows.Controls;
using System;

namespace UZH.Infrastructure.Controls
{
   public enum ToggleFlipButtonPosition
   { 
      Left,
      Right
   }

   public class ToggleFlipButton : CheckBox
   {
      #region Fields

      public static readonly DependencyProperty LeftTextProperty =
          DependencyProperty.Register("LeftText", typeof(string), typeof(ToggleFlipButton), new UIPropertyMetadata(string.Empty));

      public static readonly DependencyProperty RightTextProperty =
          DependencyProperty.Register("RightText", typeof(string), typeof(ToggleFlipButton), new UIPropertyMetadata(string.Empty));

      public static readonly DependencyProperty PositionProperty =
          DependencyProperty.Register("Position", typeof(ToggleFlipButtonPosition), typeof(ToggleFlipButton), new UIPropertyMetadata(ToggleFlipButtonPosition.Left));

      public static readonly RoutedEvent OnLeftSelectedEvent =
         EventManager.RegisterRoutedEvent("OnLeftSelected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ToggleFlipButton));

      public static readonly RoutedEvent OnRightSelectedEvent =
         EventManager.RegisterRoutedEvent("OnRightSelected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ToggleFlipButton));

      #endregion

      #region Constructors
      public ToggleFlipButton()
      { 
      }
      #endregion

      #region Events
      public event RoutedEventHandler OnLeftSelected
      {
         add { AddHandler(OnLeftSelectedEvent, value); }
         remove { RemoveHandler(OnLeftSelectedEvent, value); }
      }

      public event RoutedEventHandler OnRightSelected
      {
         add { AddHandler(OnRightSelectedEvent, value); }
         remove { RemoveHandler(OnRightSelectedEvent, value); }
      }
      #endregion

      #region Enums

      #endregion

      #region Properties

      public ToggleFlipButtonPosition Position
      {
         get { return (ToggleFlipButtonPosition)GetValue(PositionProperty); }
         set { SetValue(PositionProperty, value); }
      }

      public string LeftText
      {
         get { return (string)GetValue(LeftTextProperty); }
         set { SetValue(LeftTextProperty, value); }
      }

      public string RightText
      {
         get { return (string)GetValue(RightTextProperty); }
         set { SetValue(RightTextProperty, value); }
      }

      #endregion

      #region Public Methods
      protected override void OnChecked(RoutedEventArgs e)
      {
         Position = ToggleFlipButtonPosition.Left;
         base.OnChecked(e);

         this.RaiseEvent(new RoutedEventArgs(OnLeftSelectedEvent));
      }

      protected override void OnUnchecked(RoutedEventArgs e)
      {
         Position = ToggleFlipButtonPosition.Right;
         base.OnUnchecked(e);

         this.RaiseEvent(new RoutedEventArgs(OnRightSelectedEvent));
      }
      #endregion

      #region Internal Methods

      #endregion

      #region Protected Methods

      #endregion

      #region Private Methods

      #endregion
   }
}
