﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Model;

namespace ScrumSupporter.LocalStorage
{
    public abstract class DBConnection : IDisposable
    {
        public abstract bool IsOpenSuccessful();

        public abstract void Dispose();

        public abstract void LoadWorkItemModelsIntoDB(IEnumerable<WorkItemViewModel> models);

        public abstract ICollection<WorkItemModel> QueryBacklogItems();

        public abstract ICollection<WorkItemsOfIterationContainerViewModel> QueryItemsOfIterations();
    }
}
