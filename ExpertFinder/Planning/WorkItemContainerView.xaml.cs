﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;
using System.Windows.Media.Animation;
using ScrumSupporter.Util;
using ScrumSupporter.Planning.WorkItemViews;
using ScrumSupporter.Planning.Filters;
using System.Timers;




namespace ScrumSupporter.Planning
{
    /// <summary>
    /// Interaktionslogik für WorkItemContainerView.xaml. Handlers several events of drag and drop operations between the backlog view,
    /// the single iteration view and the iteration views. 
    /// </summary>
    public partial class WorkItemContainerView: SurfaceUserControl
    {
        private DateTime oldTime;
        private DateTime oldTimeOpenEditingView;

        private DateTime sviSingleIterationManipulationStartTime;
        private ScatterViewItem sviIterationView;
        private SingleIterationView singleIterationView;

        private DateTime sviEditingViewManipulationStartTime;
        private ScatterViewItem sviEditingView;

        private bool isOrientationZero;
        private bool areIterationWorkItemsShown = false;
        private bool areBacklogWorkItemsShown = true;

        private ScatterViewItem sviFilterContainer;
        private DateTime sviFilterViewManipulationStartTime;


        /// <summary>
        /// Construcor, which sets the time for double clicking operations and initializes components and properties of the menu container. 
        /// </summary>
        public WorkItemContainerView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;
            oldTimeOpenEditingView = DateTime.Now;

            InitializeMenuContainer();
            InitializeIterationsContainer();


        }





        /// <summary>
        /// Groups the visual representations of the work items accoring to their type. 
        /// </summary>
        private void GroupWorkItems()
        {
            Random random = new Random();
            if (scatterview.HasItems)
            {
                foreach (var item in scatterview.Items)
                {
                    if (item is WorkItemViewModel)
                    {
                        WorkItemViewModel model = item as WorkItemViewModel;
                        DependencyObject depO = scatterview.ItemContainerGenerator.ContainerFromItem(model);
                        UIElement el = depO as UIElement;
                        ScatterViewItem svi = el as ScatterViewItem;
                        svi.Center = model.GetPositionInBacklog(random);
                    }
                }
            }
            if (WorkItemsOfSpecificIterationContainer.HasItems)
            {
                foreach (var item in WorkItemsOfSpecificIterationContainer.Items)
                {
                    if (item is WorkItemViewModel)
                    {
                        WorkItemViewModel model = item as WorkItemViewModel;
                        DependencyObject depO = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(model);
                        UIElement el = depO as UIElement;
                        ScatterViewItem svi = el as ScatterViewItem;
                        svi.Center = model.GetPositionInBacklog(random);
                    }
                }
            }
        }

        /// <summary>
        /// Groups the visual representations of the work items according to the property "Assigned To".
        /// </summary>
        private void GroupWorkItemsByFilterCriteria()
        {
            Random random = new Random();
            foreach (var item in WorkItemsOfSpecificIterationContainer.Items)
            {
                if (item is WorkItemViewModel)
                {
                    WorkItemViewModel model = item as WorkItemViewModel;
                    DependencyObject depO = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(model);
                    UIElement el = depO as UIElement;
                    ScatterViewItem svi = el as ScatterViewItem;

                    if (model.AssignedTo.Equals(""))
                    {
                        int x = random.Next(100, Constants.BACKLOGWIDTH / 2 - 100);
                        int y = random.Next(100, Constants.BACKLOGHEIGHT - 100);
                        svi.Center = new Point(x, y);
                    }
                    else
                    {
                        int x = random.Next(Constants.BACKLOGWIDTH / 2 + 100, Constants.BACKLOGWIDTH - 100);
                        int y = random.Next(100, Constants.BACKLOGHEIGHT - 100);
                        svi.Center = new Point(x, y);
                    }
                }
            }
        }

        /// <summary>
        /// Sets properties of the menu container, which includes adding new work items and leaving the view. 
        /// </summary>
        private void InitializeMenuContainer()
        {
            sviMenuContainer.Center = new Point(-60, MenuContainer.Height / 2);
            sviMenuContainer.DecelerationRate = double.NaN;
            sviMenuContainer.ShowsActivationEffects = false;
            sviMenuContainer.ScatterManipulationDelta += sviMenuContainerScatterManipulationDelta;
        }

        /// <summary>
        /// Sets properties of the iterations container, which shows the work items which are associated to an iteration. 
        /// </summary>
        private void InitializeIterationsContainer()
        {
            sviIterationsContainer.Center = new Point(120, IterationsContainer.Height / 2);
            sviIterationsContainer.DecelerationRate = double.NaN;
            sviIterationsContainer.ShowsActivationEffects = false;
            sviIterationsContainer.ScatterManipulationDelta += sviIterationsContainerScatterManipulationDelta;
            isOrientationZero = true;
        }

        /// <summary>
        /// To enable only horizontal movement of the menu container and the adjustment of center of the workitems. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void sviMenuContainerScatterManipulationDelta(object sender, ScatterManipulationDeltaEventArgs args)
        {
            sviMenuContainer.Center = new Point(sviMenuContainer.Center.X + args.HorizontalChange, 768 / 2);

            if (sviMenuContainer.Center.X < -60)
            {
                sviMenuContainer.Center = new Point(-60, MenuContainer.Height / 2);
            }
            if (sviMenuContainer.Center.X > 99)
            {
                sviMenuContainer.Center = new Point(99, MenuContainer.Height / 2);

                if (areBacklogWorkItemsShown)
                {
                    if (scatterview.HasItems)
                    {
                        foreach (var item in scatterview.Items)
                        {
                            if (item is WorkItemViewModel)
                            {
                                WorkItemViewModel model = item as WorkItemViewModel;
                                DependencyObject depO = scatterview.ItemContainerGenerator.ContainerFromItem(model);
                                UIElement el = depO as UIElement;
                                ScatterViewItem svi = el as ScatterViewItem;
                                if (svi != null)
                                {
                                    if (svi.Center.X < 150)
                                    {
                                        svi.Center = new Point(150, svi.Center.Y);
                                    }
                                }
                            }
                        }
                    }
                }
                if (areIterationWorkItemsShown)
                {
                    if (WorkItemsOfSpecificIterationContainer.HasItems)
                    {
                        foreach (var item in WorkItemsOfSpecificIterationContainer.Items)
                        {
                            if (item is WorkItemViewModel)
                            {
                                WorkItemViewModel model = item as WorkItemViewModel;
                                DependencyObject depO = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(model);
                                UIElement el = depO as UIElement;
                                ScatterViewItem svi = el as ScatterViewItem;
                                if (svi != null)
                                {
                                    if (svi.Center.X < 150)
                                    {
                                        svi.Center = new Point(150, svi.Center.Y);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            args.Handled = true;
        }


        /// <summary>
        /// To enable only horizontal movement of the iteration container and to reset the center of a work item, which is in the way. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void sviIterationsContainerScatterManipulationDelta(object sender, ScatterManipulationDeltaEventArgs args)
        {
            sviIterationsContainer.Center = new Point(sviIterationsContainer.Center.X + args.HorizontalChange, 768 / 2);
            if (sviIterationsContainer.Center.X > 120)
            {
                sviIterationsContainer.Center = new Point(120, sviIterationsContainer.Height / 2);
            }
            if (sviIterationsContainer.Center.X < -132)
            {
                sviIterationsContainer.Center = new Point(-132, sviIterationsContainer.Height / 2);

                if (areIterationWorkItemsShown)
                {
                    if (WorkItemsOfSpecificIterationContainer.HasItems)
                    {
                        foreach (var item in WorkItemsOfSpecificIterationContainer.Items)
                        {
                            if (item is WorkItemViewModel)
                            {
                                WorkItemViewModel model = item as WorkItemViewModel;
                                DependencyObject depO = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(model);
                                UIElement el = depO as UIElement;
                                ScatterViewItem svi = el as ScatterViewItem;
                                if (svi != null)
                                {
                                    if (svi.Center.X > 585)
                                    {
                                        svi.Center = new Point(585, svi.Center.Y);
                                    }
                                }
                            }
                        }
                    }
                }

                if (areBacklogWorkItemsShown)
                {
                    if (scatterview.HasItems)
                    {
                        foreach (var item in scatterview.Items)
                        {
                            if (item is WorkItemViewModel)
                            {
                                WorkItemViewModel model = item as WorkItemViewModel;
                                DependencyObject depO = scatterview.ItemContainerGenerator.ContainerFromItem(model);
                                UIElement el = depO as UIElement;
                                ScatterViewItem svi = el as ScatterViewItem;
                                if (svi != null)
                                {
                                    if (svi.Center.X > 585)
                                    {
                                        svi.Center = new Point(585, svi.Center.Y);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            args.Handled = true;
        }

        /// <summary>
        /// In case that the dragging of a work item in the backlog is canceled, it's visual representation
        /// is set back to visible.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragOfBacklogWorkItemCanceled(object sender, Microsoft.Surface.Presentation.SurfaceDragDropEventArgs e)
        {
            WorkItemViewModel data = e.Cursor.Data as WorkItemViewModel;
            ScatterViewItem item = data.DraggedElement as ScatterViewItem;
            if (item != null)
            {
                item.Visibility = Visibility.Visible;
                item.Orientation = e.Cursor.GetOrientation(this);
                item.Center = e.Cursor.GetPosition(this);
            }
        }

        /// <summary>
        /// Indicates that dropping is allowed/possible at the current position of the cursor. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropTargetDragEnter(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.DragSource is ScatterView)
            {
                e.Cursor.Visual.Tag = "DragEnter";
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Indicates that dropping is not allowed/possible at the current position of the cursor. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropTargetDragLeave(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.DragSource is ScatterView)
            {
                e.Cursor.Visual.Tag = null;
            }
        }

        /// <summary>
        /// Handles the dropping of a working item with no iteration assigned to an iteration. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropBacklogWorkItem(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.DragSource is ScatterView)
            {
                SurfaceListBox listbox = e.Cursor.CurrentTarget as SurfaceListBox;
                for (int index = 0; index < listbox.Items.Count; index++)
                {
                    var model = listbox.Items.GetItemAt(index);
                    DependencyObject depO = listbox.ItemContainerGenerator.ContainerFromItem(model);
                    UIElement el = depO as UIElement;
                    FrameworkElement frEl = el as FrameworkElement;

                    Rect bounds = VisualTreeHelper.GetDescendantBounds(frEl);
                    Point position = e.Cursor.GetPosition((UIElement)frEl);
                    if (frEl != null && bounds.Contains(position))
                    {
                        int indexOfUndelyingItem = listbox.Items.IndexOf(model);
                        WorkItemsOfIterationContainerViewModel container = model as WorkItemsOfIterationContainerViewModel;

                        container.WorkItemList.Add(e.Cursor.Data as WorkItemViewModel);
                        container.SetStackRank();
                        (this.DataContext as WorkItemContainerViewModel).BacklogList.Remove(e.Cursor.Data as WorkItemViewModel);
                    }
                }
            }
            else
            {
                DragOfWorkItemInIterationCanceled(sender, e);
            }
        }

        /// <summary>
        /// Initializes the dragging of a selected work item in the backlog. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScatterView_PreviewHoldGesture(object sender, ContactEventArgs e)
        {
            FrameworkElement findSource = e.OriginalSource as FrameworkElement;
            ScatterViewItem draggedElement = null;
            // Find the ScatterViewItem object that is being touched.
            while (draggedElement == null && findSource != null)
            {
                if ((draggedElement = findSource as ScatterViewItem) == null)
                {
                    findSource = VisualTreeHelper.GetParent(findSource) as FrameworkElement;
                }
            }

            if (draggedElement == null)
            {
                return;
            }
            WorkItemViewModel data = draggedElement.Content as WorkItemViewModel;
            if (data == null || !draggedElement.CanMove)
            {
                return;
            }
            data.DraggedElement = draggedElement;

            ContentControl cursorVisual = data.GetCursorVisual();
            List<InputDevice> devices = new List<InputDevice>();
            devices.Add(e.Device);
            foreach (InputDevice device in draggedElement.TouchesCapturedWithin)
            {
                if (device != e.Device)
                {
                    devices.Add(device);
                }
            }
            ItemsControl dragSource = ItemsControl.ItemsControlFromItemContainer(draggedElement);

            SurfaceDragDrop.BeginDragDrop(
              dragSource, draggedElement,
              cursorVisual,
              draggedElement.DataContext,
              devices,
              DragDropEffects.Move);

            draggedElement.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        /// <summary>
        /// Event handler to open the single iteration view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listbox_ContactDown(object sender, ContactEventArgs e)
        {
            DateTime time = DateTime.Now;
            TimeSpan interval = time.Subtract(oldTime);

            //if a double click occurs the SingleIterationView opens.
            if (interval.TotalMilliseconds < 500)
            {

                singleIterationView = new SingleIterationView();

                WorkItemsOfIterationContainerViewModel model = (WorkItemsOfIterationContainerViewModel)listbox.SelectedItem;
                singleIterationView.DataContext = model;

                sviIterationView = new ScatterViewItem();
                sviIterationView.MinHeight = 203;
                sviIterationView.MinWidth = 700;
                sviIterationView.Orientation = 0;
                sviIterationView.Center = new Point(Constants.TBLWIDTH * 0.3, Constants.TBLHEIGHT / 2);

                sviIterationView.Content = singleIterationView;

                sviIterationView.Style = (Style)FindResource("sviStyle");

                sviIterationView.ScatterManipulationStarted += sviSingleIterationView_ManipulationStarted;
                sviIterationView.ScatterManipulationCompleted += sviSingleIterationView_ManipulationCompleted;
                sviIterationView.SizeChanged += sviSingleIterationView_SizeChanged;

                DetailedIterationContainer.Items.Clear();
                DetailedIterationContainer.Items.Add(sviIterationView);
            }
            oldTime = time;




        }

        /// <summary>
        /// Captures the start time of the manipulation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviSingleIterationView_ManipulationStarted(object sender, ScatterManipulationStartedEventArgs e)
        {
            sviSingleIterationManipulationStartTime = DateTime.Now;
        }

        /// <summary>
        /// Evaluates if the manipulation was faster than 2000 ms and if the origin is out of the bounds of the table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviSingleIterationView_ManipulationCompleted(object sender, ScatterManipulationCompletedEventArgs e)
        {
            DateTime sviLoadProjectManipulationEndTime = DateTime.Now;
            TimeSpan durationOfManipulation = sviLoadProjectManipulationEndTime - sviSingleIterationManipulationStartTime;

            if (durationOfManipulation.TotalMilliseconds < 2000)
            {
                if (e.ManipulationOrigin.X < 0 || e.ManipulationOrigin.X > Constants.TBLWIDTH || e.ManipulationOrigin.Y < 0 || e.ManipulationOrigin.Y > Constants.TBLHEIGHT)
                {
                    try
                    {
                        DetailedIterationContainer.Items.Remove(sviIterationView);
                    }
                    catch (InvalidOperationException invalidOperationException)
                    {
                        System.Console.WriteLine("The scatterViewItem is already removed from the items collection: " + invalidOperationException.Message);
                    }
                }
            }
        }


        /// <summary>
        /// Handles the size changed event, such that the single iteration view is adapted to the new size. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviSingleIterationView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Size bigSize = new Size(700 * 1.3, 203 * 1.3);
            Size veryBigSize = new Size(700 * 1.7, 203 * 1.7);
            if (e.NewSize.Width > veryBigSize.Width && e.NewSize.Height > veryBigSize.Height)
            {
                singleIterationView.OnSizeChangedToVeryBig();
            }
            else if (e.NewSize.Width > bigSize.Width && e.NewSize.Height > bigSize.Height)
            {
                singleIterationView.OnSizeChangedToBig();
            }
            else if (e.NewSize.Width < bigSize.Width && e.NewSize.Height < bigSize.Height)
            {
                singleIterationView.OnSizeChangedToSmall();
            }
        }

        /// <summary>
        /// Shows the editing view of a work item after a double click occured on the single iteration view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SingleIterationViewPreviewContactDown(object sender, ContactEventArgs e)
        {
            if (e.Source is SingleIterationView)
            {
                SingleIterationView siv = e.Source as SingleIterationView;
                if (!(e.Contact.Target is Grid) && !(e.Contact.Target is Border))
                {
                    DateTime now = DateTime.Now;
                    TimeSpan interval = now.Subtract(oldTimeOpenEditingView);
                    if (interval.TotalMilliseconds < 500)
                    {
                        try
                        {
                            WorkItemViewModel model = siv.OnClickedOnWorkItem();
                            CreateEditingView(model.GetEditingView());
                        }
                        catch (InvalidOperationException invalidOperationException)
                        {
                            System.Console.WriteLine(invalidOperationException.Message);
                        }
                        finally
                        {
                            //Message Box
                        }
                    }

                }
            }
            oldTimeOpenEditingView = DateTime.Now;
        }

        /// <summary>
        /// Sets properties (position, size, look and feel) of the field sviEditingView.
        /// </summary>
        /// <param name="viewbox"></param>
        private void CreateEditingView(Viewbox viewbox)
        {
            sviEditingView = new ScatterViewItem();
            sviEditingView.Width = 800;
            sviEditingView.Height = 550;
            sviEditingView.Orientation = 0;

            sviEditingView.Center = new Point(Constants.TBLWIDTH / 2, Constants.TBLHEIGHT / 2);

            sviEditingView.Content = viewbox;

            sviEditingView.Style = (Style)FindResource("sviStyle");

            sviEditingView.ScatterManipulationStarted += sviEditingView_ManipulationStarted;
            sviEditingView.ScatterManipulationCompleted += sviEditingView_ManipulationCompleted;

            if (!EditingWorkItemContainer.Items.IsEmpty)
            {
                EditingWorkItemContainer.Items.Clear();
                EditingWorkItemContainer.Items.Add(sviEditingView);
            }
            else
            {
                EditingWorkItemContainer.Items.Add(sviEditingView);
            }
        }

        /// <summary>
        /// Captures the start time of the manipulation of the work item editing view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviEditingView_ManipulationStarted(object sender, ScatterManipulationStartedEventArgs e)
        {
            sviEditingViewManipulationStartTime = DateTime.Now;
        }
        /// <summary>
        /// Evaluates if the manipulation of the work item editing view was faster than 2000 ms and if the origin is out of the bounds of the table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviEditingView_ManipulationCompleted(object sender, ScatterManipulationCompletedEventArgs e)
        {
            DateTime now = DateTime.Now;
            TimeSpan durationOfManipulation = now - sviEditingViewManipulationStartTime;

            if (durationOfManipulation.TotalMilliseconds < 2000)
            {
                if (e.ManipulationOrigin.X < 0 || e.ManipulationOrigin.X > Constants.TBLWIDTH || e.ManipulationOrigin.Y < 0 || e.ManipulationOrigin.Y > Constants.TBLHEIGHT)
                {

                    Storyboard fadeoutSb = new Storyboard();
                    fadeoutSb = (Storyboard)FindResource("fadeout");
                    sviEditingView.BeginStoryboard(fadeoutSb);
                }
            }
        }
        /// <summary>
        /// The contact hold gesture which starts the drag and drop operation on a work item, which is in the iteration view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listbox_ContactHoldGesture(object sender, ContactEventArgs e)
        {
            SurfaceListBox parent = (SurfaceListBox)sender;

            DependencyObject depO = parent.ItemContainerGenerator.ContainerFromItem(parent.SelectedItem);
            WorkItemsOfIterationContainerView view = Finder.FindVisualChild<WorkItemsOfIterationContainerView>(depO);
            if (view.itemsControl.HasItems)
            {
                bool clickedOnWorkItem = false;
                foreach (var item in view.itemsControl.Items)
                {
                    DependencyObject containerFromItem = view.itemsControl.ItemContainerGenerator.ContainerFromItem(item);
                    UIElement containerFromItemUIElement = containerFromItem as UIElement;
                    FrameworkElement frEl = containerFromItemUIElement as FrameworkElement;

                    Rect bounds = VisualTreeHelper.GetDescendantBounds(frEl);
                    Point position = e.GetPosition((UIElement)frEl);
                    if (frEl != null && bounds.Contains(position))
                    {
                        WorkItemViewModel data = frEl.DataContext as WorkItemViewModel;
                        if (data == null)
                        {
                            return;
                        }
                        data.DraggedElement = frEl;
                        ContentControl cursorVisual = data.GetCursorVisual();

                        List<InputDevice> devices = new List<InputDevice>();
                        devices.Add(e.Device);

                        foreach (InputDevice device in frEl.TouchesCapturedWithin)
                        {
                            if (device != e.Device)
                            {
                                devices.Add(device);
                            }
                        }
                        ItemsControl dragSource = ItemsControl.ItemsControlFromItemContainer(frEl);
                        SurfaceDragDrop.BeginDragDrop(
                          dragSource,
                          frEl,
                          cursorVisual,
                          data,
                          devices,
                          DragDropEffects.Move);

                        frEl.Visibility = Visibility.Hidden;
                        clickedOnWorkItem = true;
                        e.Handled = true;
                    }
                }
                if (!clickedOnWorkItem)
                {
                    System.Console.WriteLine("Contact hold gesture on container");
                    DependencyObject dependecyObject = parent.ItemContainerGenerator.ContainerFromItem(parent.SelectedItem);
                    WorkItemsOfIterationContainerView containerView = Finder.FindVisualChild<WorkItemsOfIterationContainerView>(dependecyObject);

                    WorkItemsOfIterationContainerViewModel viewModel = containerView.DataContext as WorkItemsOfIterationContainerViewModel;
                    viewModel.DraggedElement = containerView;
                    ContentControl cursorVisualForIterationContainer = new ContentControl()
                        {
                            Content = viewModel,
                            Style = FindResource("CursorStyleIterationContainerView") as Style
                        };
                    List<InputDevice> devices = new List<InputDevice>();
                    devices.Add(e.Device);

                    foreach (InputDevice device in containerView.TouchesCapturedWithin)
                    {
                        if (device != e.Device)
                        {
                            devices.Add(device);
                        }
                    }
                    ItemsControl dragSource = listbox;
                    SurfaceDragDrop.BeginDragDrop(
                      dragSource,
                      containerView,
                      cursorVisualForIterationContainer,
                      viewModel,
                      devices,
                      DragDropEffects.Move);

                    containerView.Opacity = 0.3;
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Sets the visibility of the work item, which was dragged away from the iteration view to visible again. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragOfWorkItemInIterationCanceled(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.Data is WorkItemsOfIterationContainerViewModel)
            {
                WorkItemsOfIterationContainerViewModel model = e.Cursor.Data as WorkItemsOfIterationContainerViewModel;
                if (model.DraggedElement is WorkItemsOfIterationContainerView)
                {
                    WorkItemsOfIterationContainerView view = model.DraggedElement as WorkItemsOfIterationContainerView;
                    view.Opacity = 1;
                }
                return;
            }

            if (!(e.Cursor.DragSource is SurfaceListBox))
            {
                WorkItemViewModel data = e.Cursor.Data as WorkItemViewModel;

                if (e.Cursor.DragSource is SurfaceItemsControl)
                {
                    DependencyObject depO = (e.Cursor.DragSource as SurfaceItemsControl).ItemContainerGenerator.ContainerFromItem(data);
                    UIElement itemUI = depO as UIElement;
                    itemUI.Visibility = Visibility.Visible;
                }
                else
                {
                    SurfaceListBox parent = (SurfaceListBox)sender;

                    DependencyObject depO = parent.ItemContainerGenerator.ContainerFromItem(parent.SelectedItem);
                    WorkItemsOfIterationContainerView view = Finder.FindVisualChild<WorkItemsOfIterationContainerView>(depO);

                    DependencyObject item = view.itemsControl.ItemContainerGenerator.ContainerFromItem(data);
                    UIElement itemUI = item as UIElement;
                    itemUI.Visibility = Visibility.Visible;
                }
            }



        }

        /// <summary>
        /// Indicates that dropping is allowed/possible at the current position of the cursor. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragEnterIntoBacklog(object sender, SurfaceDragDropEventArgs e)
        {
            if (!(e.Cursor.DragSource is ScatterView))
            {
                if (e.Cursor.DragSource is SurfaceItemsControl)
                {
                    e.Cursor.Visual.Tag = "DragEnter";
                }
                else if (e.Cursor.DragSource is SurfaceListBox)
                {
                    e.Cursor.Visual.Tag = "DragEnter";
                }
            }
        }
        /// <summary>
        /// Indicates that dropping is not allowed/possible at the current position of the cursor. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragLeaveFromBacklog(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.DragSource is SurfaceItemsControl)
            {
                e.Cursor.Visual.Tag = null;
            }
            else if (e.Cursor.DragSource is SurfaceListBox)
            {
                e.Cursor.Visual.Tag = null;
            }
        }

        /// <summary>
        /// Handles the dropping of a work item, which was assigned to an iteration back to the unassigned work items. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropIterationWorkItemToBacklog(object sender, SurfaceDragDropEventArgs e)
        {
            if (e.Cursor.Data is WorkItemsOfIterationContainerViewModel && !(e.Cursor.CurrentTarget is SurfaceListBox))
            {
                WorkItemsOfIterationContainerViewModel model = e.Cursor.Data as WorkItemsOfIterationContainerViewModel;
                HideAllBacklogItems();
                WorkItemsOfSpecificIterationContainer.DataContext = model;
                areIterationWorkItemsShown = true;
                UndoFilter();
                if (model.DraggedElement is WorkItemsOfIterationContainerView)
                {
                    WorkItemsOfIterationContainerView view = model.DraggedElement as WorkItemsOfIterationContainerView;
                    view.Opacity = 1;
                }
                GroupWorkItemsByFilterCriteria();
                return;
            }

            if (!(e.Cursor.DragSource is ScatterView) && !(e.Cursor.CurrentTarget is SurfaceListBox))
            {
                Point position = e.Cursor.GetPosition(scatterview);
                bool inTheBounds = false;
                if ((Constants.TBLWIDTH - listbox.ActualWidth) > position.X)
                {
                    inTheBounds = true;
                }

                if (e.Cursor.DragSource is SurfaceItemsControl && inTheBounds)
                {
                    WorkItemViewModel model = e.Cursor.Data as WorkItemViewModel;
                    //remove from iteration
                    ((e.Cursor.DragSource as SurfaceItemsControl).DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Remove(model);
                    ((e.Cursor.DragSource as SurfaceItemsControl).DataContext as WorkItemsOfIterationContainerViewModel).SetStackRank();

                    //add to backlog
                    (this.DataContext as WorkItemContainerViewModel).BacklogList.Add(model);
                }
                else if (e.Cursor.DragSource is SurfaceListBox && inTheBounds)
                {
                    WorkItemViewModel model = e.Cursor.Data as WorkItemViewModel;
                    //remove from iteration
                    (e.Cursor.DragSource.DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Remove(model);
                    (e.Cursor.DragSource.DataContext as WorkItemsOfIterationContainerViewModel).SetStackRank();

                    //add to backlog
                    (this.DataContext as WorkItemContainerViewModel).BacklogList.Add(model);

                }
                else
                {
                    DragOfWorkItemInIterationCanceled(sender, e);
                }
            }
            else if (e.Cursor.DragSource is ScatterView)
            {
                DragOfBacklogWorkItemCanceled(sender, e);
            }


        }

        /// <summary>
        /// Handles the switch between two orientation options of the iteration container. (To Down)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDown(object sender, RoutedEventArgs e)
        {
            if (!isOrientationZero)
            {
                sviIterationsContainer.Orientation = 0;
                listbox.Margin = new Thickness(44, 0, 0, 0);
                orientation180DownButton.Visibility = Visibility.Hidden;
                orientation180UpButton.Visibility = Visibility.Hidden;
                orientation0DownButton.Visibility = Visibility.Visible;
                orientation0UpButton.Visibility = Visibility.Visible;
                isOrientationZero = true;
            }
        }

        /// <summary>
        /// Handles the switch between two orientation options of the iteration container. (To Up)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUp(object sender, RoutedEventArgs e)
        {
            if (isOrientationZero)
            {
                sviIterationsContainer.Orientation = 180;
                listbox.Margin = new Thickness(0, 0, 44, 0);
                orientation180DownButton.Visibility = Visibility.Visible;
                orientation180UpButton.Visibility = Visibility.Visible;
                orientation0UpButton.Visibility = Visibility.Hidden;
                orientation0DownButton.Visibility = Visibility.Hidden;
                isOrientationZero = false;
            }
        }

        /// <summary>
        /// Event handler of group button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickedGroupWorkItems(object sender, RoutedEventArgs e)
        {
            GroupWorkItems();
        }

        /// <summary>
        /// As soon as the scatterview is loaded the items are grouped.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scatterview_Loaded(object sender, RoutedEventArgs e)
        {
            GroupWorkItems();
        }

        /// <summary>
        /// Sets several properties of the ScatterViewItem, which contains the Filter View. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFilterView(object sender, RoutedEventArgs e)
        {
            FilterViewModel filterViewModel = new FilterViewModel();
            FilterView filterView = filterViewModel.CreateView();

            sviFilterContainer = new ScatterViewItem();
            sviFilterContainer.Content = filterView;
            sviFilterContainer.ScatterManipulationStarted += sviFilterView_ManipulationStarted;
            sviFilterContainer.ScatterManipulationCompleted += sviFilterView_ManipulationCompleted;
            sviFilterContainer.PreviewContactDown += sviFilterView_PreviewContactDown;

            sviFilterContainer.Center = new Point(Constants.TBLWIDTH / 2, Constants.TBLHEIGHT / 2);
            sviFilterContainer.Orientation = 0;
            sviFilterContainer.ShowsActivationEffects = false;
            sviFilterContainer.Width = 900;
            sviFilterContainer.Height = 430;
            SolidColorBrush brush = new SolidColorBrush(Colors.Transparent);
            sviFilterContainer.Background = brush;
            sviFilterContainer.ZIndex = 1;

            if (FilterContainer.HasItems)
            {
                FilterContainer.Items.Clear();
                FilterContainer.Items.Add(sviFilterContainer);
            }
            else
            {
                FilterContainer.Items.Add(sviFilterContainer);
            }
        }

        /// <summary>
        /// Captures the start time of the manipulation of the filter view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviFilterView_ManipulationStarted(object sender, ScatterManipulationStartedEventArgs e)
        {
            sviFilterViewManipulationStartTime = DateTime.Now;
        }

        /// <summary>
        /// Removes the filter view, if it was thrown out. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviFilterView_ManipulationCompleted(object sender, ScatterManipulationCompletedEventArgs e)
        {
            DateTime sviFilterManipulationEndTime = DateTime.Now;
            TimeSpan durationOfManipulation = sviFilterManipulationEndTime - sviFilterViewManipulationStartTime;

            if (durationOfManipulation.TotalMilliseconds < 2000)
            {
                if (e.ManipulationOrigin.X < 0 || e.ManipulationOrigin.X > Constants.TBLWIDTH || e.ManipulationOrigin.Y < 0 || e.ManipulationOrigin.Y > Constants.TBLHEIGHT)
                {
                    if (FilterContainer.HasItems)
                    {
                        FilterContainer.Items.Clear();
                    }
                }
            }
        }

        /// <summary>
        /// Starts the filtering, if the "Filter" button is clicked. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviFilterView_PreviewContactDown(object sender, ContactEventArgs e)
        {
            FilterView filterView = sviFilterContainer.Content as FilterView;
            SurfaceButton filterButton = filterView.FilterButton;

            Rect buttonBounds = VisualTreeHelper.GetDescendantBounds(filterButton);
            bool overfilterButton = buttonBounds.Contains(e.GetPosition(filterButton));

            if (overfilterButton)
            {
                FilterViewModel filterViewModel = filterView.DataContext as FilterViewModel;

                if (areBacklogWorkItemsShown)
                {
                    ICollection<WorkItemViewModel> filteredItems = filterViewModel.Filter((this.DataContext as WorkItemContainerViewModel).BacklogList);
                    SetFilteredItemsInvisible(filteredItems, scatterview);
                }

                if (areIterationWorkItemsShown)
                {
                    WorkItemsOfIterationContainerViewModel context = WorkItemsOfSpecificIterationContainer.DataContext as WorkItemsOfIterationContainerViewModel;
                    ICollection<WorkItemViewModel> filteredItems = filterViewModel.Filter(context.WorkItemList);
                    SetFilteredItemsInvisible(filteredItems, WorkItemsOfSpecificIterationContainer);
                }
                if (FilterContainer.HasItems)
                {
                    FilterContainer.Items.Clear();
                }
            }
        }

        /// <summary>
        /// Sets the items, which are not relevant invisible. 
        /// </summary>
        /// <param name="itemsToFilter"></param>
        /// <param name="container"></param>
        private void SetFilteredItemsInvisible(ICollection<WorkItemViewModel> itemsToFilter, ScatterView container)
        {
            foreach (WorkItemViewModel item in container.Items)
            {
                if (!(itemsToFilter.Contains(item)))
                {
                    DependencyObject visualRepresentationFromItem = container.ItemContainerGenerator.ContainerFromItem(item);
                    FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                    fEl.Visibility = Visibility.Hidden;
                }
            }
        }

        /// <summary>
        /// Click handler of the "Undo Filter" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUndoFilter(object sender, RoutedEventArgs e)
        {
            UndoFilter();
        }

        /// <summary>
        /// Sets all work items in the currently active view to visible. 
        /// </summary>
        private void UndoFilter()
        {
            if (areBacklogWorkItemsShown)
            {
                foreach (WorkItemViewModel item in scatterview.Items)
                {
                    DependencyObject visualRepresentationFromItem = scatterview.ItemContainerGenerator.ContainerFromItem(item);
                    FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                    fEl.Visibility = Visibility.Visible;
                }
            }

            if (areIterationWorkItemsShown)
            {
                foreach (WorkItemViewModel item in WorkItemsOfSpecificIterationContainer.Items)
                {
                    DependencyObject visualRepresentationFromItem = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(item);
                    FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                    fEl.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// ClickHandler for the buttton "Clear Table". 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClearTable(object sender, RoutedEventArgs e)
        {
            ClearTable();
        }

        /// <summary>
        /// Sets all work items to invisible. 
        /// </summary>
        private void ClearTable()
        {
            HideAllBacklogItems();
            HideAllIterationItems();
        }

        /// <summary>
        /// Sets all backlog work items to invisible.
        /// </summary>
        private void HideAllBacklogItems()
        {
            foreach (WorkItemViewModel item in scatterview.Items)
            {
                DependencyObject visualRepresentationFromItem = scatterview.ItemContainerGenerator.ContainerFromItem(item);
                FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                fEl.Visibility = Visibility.Hidden;
            }
            SetAddButtonsDisabled();
            areBacklogWorkItemsShown = false; 
        }

        private void SetAddButtonsDisabled()
        {
            addBugButton.Opacity = 0.6;
            addBugButton.IsEnabled = false;
            addTaskButton.Opacity = 0.6;
            addTaskButton.IsEnabled = false;
            addIssueButton.Opacity = 0.6;
            addIssueButton.IsEnabled = false;
            addUserStoryButton.Opacity = 0.6;
            addUserStoryButton.IsEnabled = false;
        }
        private void setAddButtonsEnabled()
        {
            addBugButton.Opacity = 1;
            addBugButton.IsEnabled = true;
            addTaskButton.Opacity = 1;
            addTaskButton.IsEnabled = true;
            addIssueButton.Opacity = 1;
            addIssueButton.IsEnabled = true;
            addUserStoryButton.Opacity = 1;
            addUserStoryButton.IsEnabled = true;
        }

        /// <summary>
        /// Sets all work items from an iteration to invisible. 
        /// </summary>
        private void HideAllIterationItems()
        {
            if (WorkItemsOfSpecificIterationContainer.HasItems)
            {
                foreach (var item in WorkItemsOfSpecificIterationContainer.Items)
                {
                    DependencyObject visualRepresentationFromItem = WorkItemsOfSpecificIterationContainer.ItemContainerGenerator.ContainerFromItem(item);
                    FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                    fEl.Visibility = Visibility.Hidden;
                }
            }
            areIterationWorkItemsShown = false;
        }

        /// <summary>
        /// Click handler to create the initial setup of the work items. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInitialSetup(object sender, RoutedEventArgs e)
        {
            foreach (WorkItemViewModel item in scatterview.Items)
            {
                DependencyObject visualRepresentationFromItem = scatterview.ItemContainerGenerator.ContainerFromItem(item);
                FrameworkElement fEl = visualRepresentationFromItem as FrameworkElement;
                fEl.Visibility = Visibility.Visible;
            }
            areIterationWorkItemsShown = false;
            areBacklogWorkItemsShown = true;
            setAddButtonsEnabled();
            GroupWorkItems();

            HideAllIterationItems();

        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            //(this.DataContext as WorkItemContainerViewModel).UploadWorkItems();
           // Screenshot.TakeScreenshot(this);
        }

        private void SurfaceUserControl_ContactDown(object sender, ContactEventArgs e)
        {

            if (e.Contact.IsTagRecognized)
            {
                //Screenshot.TakeScreenshot(this);
            }
        }





    }
}
