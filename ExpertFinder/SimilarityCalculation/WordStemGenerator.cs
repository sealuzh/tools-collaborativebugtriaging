﻿using System;
using LemmaSharp;

namespace ScrumSupporter
{
    /// <summary>
    /// This class looks up the stem of a given word.
    /// </summary>
    public class WordStemGenerator
    {
        private ILemmatizer lmtz;

        /// <summary>
        /// Initializes the Lemmatizer.
        /// </summary>
        public WordStemGenerator()
        {
           lmtz = new LemmatizerPrebuiltCompact(LemmaSharp.LanguagePrebuilt.English);
        }

        /// <summary>
        /// Finds the stem of a given word.
        /// </summary>
        /// <param name="word">the word to look up</param>
        /// <returns>the stem</returns>
        public String FindWordStem(String word)
        {
            if (word != null)
            {
                String wordLower = word.ToLower();
                String lemma = lmtz.Lemmatize(wordLower);

                return lemma;
            }
            else return String.Empty;
        }


    }
}
