﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Collections.ObjectModel;
using Microsoft.Surface.Presentation.Controls;
using System.Windows;
using System.Windows.Input;
using Db4objects.Db4o;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Windows.Media;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Planning;

namespace ScrumSupporter.Profile
{
    /// <summary>
    /// ViewModel of the ProfileContainerView.
    /// </summary>
    public class ProfileContainerViewModel : BaseViewModel
    {

        private ProfileContainerView view;
        private ICollection<ProfileViewModel> profileList = new List<ProfileViewModel>();
        private ICollection<UpcomingEvent> eventsList = new ObservableCollection<UpcomingEvent>();
        private ICollaborationPlatform platform;
        private ScatterView workItemsContainer;
        private ScatterView detailWorkItemContainer;
        private ScatterView changesetsContainer;
        private ScatterView triagingContainer;

        public ProfileContainerViewModel(ICollaborationPlatform collaborationPlatform)
        {
            this.platform = collaborationPlatform;
        }

        public ICollection<ProfileViewModel> GetProfileList()
        {
            return profileList;
        }
        public ICollection<UpcomingEvent> GetEventsList()
        {
            return eventsList;
        }


        /// <summary>
        /// Loads the ProfileModels and the Events.
        /// </summary>
        public void LoadProfileData()
        {
            ICollection<ProfileModel> profiles = platform.GetProfileService().GetAllProfiles();
            foreach (ProfileModel profile in profiles)
            {
                ProfileViewModel profileViewModel = new ProfileViewModel(profile, platform);
                profileViewModel.AssociatedWorkItemsContainer = workItemsContainer;
                profileViewModel.DetailWorkItemContainer = detailWorkItemContainer;
                profileViewModel.AssociatedChangesetsContainer = changesetsContainer;
                profileViewModel.SetTriagingContainer(triagingContainer);
                profileList.Add(profileViewModel);
            }

            ICollection<UpcomingEvent> events = platform.GetTeamInformationService().GetTeamEvents();
            foreach (UpcomingEvent e in events)
            {
                eventsList.Add(e);
            }

        }

        private void SaveAllWorkItemsFromDeveloperToDB(Collection<WorkItem> workItemsFromTFS)
        {
            foreach (WorkItem workItemTFS in workItemsFromTFS)
            {
                WorkItemModel workitemLocal = new UserStoryModel();
                workitemLocal.Title = workItemTFS.Title;
                // workitemLocal.Description = workItemTFS.Description;
                workitemLocal.ChangedDate = workItemTFS.ChangedDate;
                workitemLocal.Id = workItemTFS.Id;

                //query the object in the DB. If is exists already update it, else add it.
                IList<WorkItemModel> result = null; // Db4oConnection.GetInstance().DB.Query<WorkItemModel>(w => w.Id == workitemLocal.Id);
                if (result.Count > 0)
                {
                    //update the already existing object in the DB
                    WorkItemModel found = result.First();
                    found.Title = workitemLocal.Title;
                    // found.Description = workitemLocal.Description;
                    found.ChangedDate = workitemLocal.ChangedDate;
                    //Db4oConnection.GetInstance().DB.Store(found);
                }
                else
                {
                    //add it to the DB
                   // Db4oConnection.GetInstance().DB.Store(workitemLocal);
                }
            }
            //Db4oConnection.GetInstance().DB.Commit();
        }

        private void SaveAllChangesetsFromDeveloperToDB(Collection<Changeset> changesetsFromTFS)
        {
            foreach (Changeset changesetTFS in changesetsFromTFS)
            {
                ChangesetModel changesetLocal = new ChangesetModel();
                changesetLocal.Id = changesetTFS.ChangesetId;
                changesetLocal.CreationDate = changesetTFS.CreationDate;
                changesetLocal.Committer = changesetTFS.Committer;
                changesetLocal.Comment = changesetTFS.Comment;

                //query the object in the DB. If is exists already update it, else add it.
                IList<ChangesetModel> result = null; // Db4oConnection.GetInstance().DB.Query<ChangesetModel>(c => c.Id == changesetLocal.Id);
                if (result.Count > 0)
                {
                    //update the already existing object in the DB
                    ChangesetModel found = result.First();
                    found.Comment = changesetLocal.Comment;
                    found.CreationDate = changesetLocal.CreationDate;
                    found.Committer = changesetLocal.Committer;
                    //Db4oConnection.GetInstance().DB.Store(found);
                }
                else
                {
                    //add it to the DB
                    //Db4oConnection.GetInstance().DB.Store(changesetLocal);
                }
            }
        }

        /// <summary>
        /// Creates the ProfileContainerView and sets itself as DataContext. The containers for several views in the 
        /// ProfileContainer are set.
        /// </summary>
        /// <returns></returns>
        public ProfileContainerView CreateView()
        {
            var v = view ?? (view = new ProfileContainerView());
            v.DataContext = this;

            workItemsContainer = view.WorkItemContainer;
            detailWorkItemContainer = view.DetailWorkItemContainer;
            changesetsContainer = view.ChangesetsContainer;
            triagingContainer = view.TriagingContainer;
            return v;
        }


        public ICommand Saving
        {
            get { return new RelayCommand(SavingExecute, CanSavingExecute); }
        }

        void SavingExecute()
        {
            //System.Console.WriteLine("SAVING PROFILES IN THE DB");

            //foreach (ProfileViewModel p in this.ProfileList)
            //{
            //    //query the profile, if it exists update it. if it is a new one, add it to the DB.
            //    IList<ProfileModel> result=  Database.getInstance().DB.Query<ProfileModel>(pm => pm.TfsAccountName.Equals(p.TfsAccountName));
            //    if (result.Count > 0)
            //    {
            //        ProfileModel found = result.First();
            //        found.Name = p.Name;
            //        found.Interests = p.Interests;
            //        found.PicturePath = p.PicturePath;
            //        found.LatestWorkItemId = p.LatestWorkItemId;
            //        Database.getInstance().DB.Store(found);
            //    }
            //    else
            //    {
            //        Database.getInstance().DB.Store(p.getModel());
            //    }

            //}
            //Database.getInstance().DB.Commit();
        }

        bool CanSavingExecute()
        {
            return true;
        }

    }
}
