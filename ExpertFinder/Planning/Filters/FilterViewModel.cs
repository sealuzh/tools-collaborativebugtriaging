﻿using System.Collections.Generic;
using UZH.Infrastructure;
using ScrumSupporter.Planning.WorkItemViews;

namespace ScrumSupporter.Planning.Filters
{
    /// <summary>
    /// The ViewModel of FilterView. Contains the functionality to filter a given Collection of work items
    /// according to the selected criteria.
    /// </summary>
    public class FilterViewModel : BaseViewModel
    {

        public FilterViewModel()
        {
            IsBugChecked = true;
            IsTaskChecked = true;
            IsIssueChecked = true;
            IsUserStoryChecked = true;

            IsActiveChecked = true;
            IsClosedChecked = false;
            IsResolvedChecked = false;

            IsAssignedToNobodyChecked = true;

        }

        private bool isBugChecked;

        public bool IsBugChecked
        {
            get { return isBugChecked; }
            set
            {
                isBugChecked = value;
                NotifyOfPropertyChange(() => IsBugChecked);
            }
        }

        private bool isTaskChecked;

        public bool IsTaskChecked
        {
            get { return isTaskChecked; }
            set
            {
                isTaskChecked = value;
                NotifyOfPropertyChange(() => IsTaskChecked);
            }
        }

        private bool isIssueChecked;

        public bool IsIssueChecked
        {
            get { return isIssueChecked; }
            set
            {
                isIssueChecked = value;
                NotifyOfPropertyChange(() => IsIssueChecked);
            }
        }

        private bool isUserStoryChecked;

        public bool IsUserStoryChecked
        {
            get { return isUserStoryChecked; }
            set
            {
                isUserStoryChecked = value;
                NotifyOfPropertyChange(() => IsUserStoryChecked);
            }
        }

        private bool isActiveChecked;

        public bool IsActiveChecked
        {
            get { return isActiveChecked; }
            set
            {
                isActiveChecked = value;
                NotifyOfPropertyChange(() => IsActiveChecked);
            }
        }

        private bool isResolvedChecked;

        public bool IsResolvedChecked
        {
            get { return isResolvedChecked; }
            set
            {
                isResolvedChecked = value;
                NotifyOfPropertyChange(() => IsResolvedChecked);
            }
        }

        private bool isClosedChecked;

        public bool IsClosedChecked
        {
            get { return isClosedChecked; }
            set
            {
                isClosedChecked = value;
                NotifyOfPropertyChange(() => IsClosedChecked);
            }
        }

        private bool isAssignedToNobodyChecked;

        public bool IsAssignedToNobodyChecked
        {
            get { return isAssignedToNobodyChecked; }
            set
            {
                isAssignedToNobodyChecked = value;
                NotifyOfPropertyChange(() => IsAssignedToNobodyChecked);
            }
        }

        /// <summary>
        /// Filters the work items according to the selected criteria.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public ICollection<WorkItemViewModel> Filter(ICollection<WorkItemViewModel> items)
        {
            ICollection<WorkItemViewModel> typeFilteredWorkItems = new List<WorkItemViewModel>();
            typeFilteredWorkItems = FilterWorkItemType(items);
            ICollection<WorkItemViewModel> stateFilteredWorkItems = new List<WorkItemViewModel>();
            stateFilteredWorkItems = FilterState(typeFilteredWorkItems);
            ICollection<WorkItemViewModel> assignedToFilteredWorkItems = new List<WorkItemViewModel>();
            assignedToFilteredWorkItems = FilterAssignedTo(stateFilteredWorkItems);


            return assignedToFilteredWorkItems;
        }

        /// <summary>
        /// Filters the work items given to the criteria regarding AssignedTo.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private ICollection<WorkItemViewModel> FilterAssignedTo(ICollection<WorkItemViewModel> items)
        {
            ICollection<WorkItemViewModel> filteredWorkItems = new List<WorkItemViewModel>();
            if (isAssignedToNobodyChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if(model.AssignedTo.Equals(""))
                    {
                        filteredWorkItems.Add(model);
                    }
                }
                return filteredWorkItems;
            }
            else
            {
                return items;
            }
        }

        /// <summary>
        /// Filters the work items given the criteria regarding the State.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private ICollection<WorkItemViewModel> FilterState(ICollection<WorkItemViewModel> items)
        {
            ICollection<WorkItemViewModel> filteredWorkItems = new List<WorkItemViewModel>();
            if (isActiveChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model.State.Equals("Active"))
                    {
                        filteredWorkItems.Add(model);
                    }
                }
            }
            else if (isClosedChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model.State.Equals("Closed"))
                    {
                        filteredWorkItems.Add(model);
                    }
                }
            }
            else if (isResolvedChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model.State.Equals("Resolved"))
                    {
                        filteredWorkItems.Add(model);
                    }
                }
            }
            else
            {
                return items;
            }
            return filteredWorkItems;
        }

        /// <summary>
        /// Filters the work items given the selected work item types. 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private ICollection<WorkItemViewModel> FilterWorkItemType(ICollection<WorkItemViewModel> items)
        {
            ICollection<WorkItemViewModel> filteredWorkItems = new List<WorkItemViewModel>();
            //filter the checked work item types. 
            if (isTaskChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model is TaskViewModel)
                    {
                        filteredWorkItems.Add(model);
                    }
                }
            }

            if (isBugChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model is BugViewModel)
                    {
                        filteredWorkItems.Add(model);
                    }
                }

            }

            if (isIssueChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model is IssueViewModel)
                    {
                        filteredWorkItems.Add(model);
                    }
                }

            }

            if (isUserStoryChecked)
            {
                foreach (WorkItemViewModel model in items)
                {
                    if (model is UserStoryViewModel)
                    {
                        filteredWorkItems.Add(model);
                    }
                }

            }
            return filteredWorkItems;
        }

        /// <summary>
        /// Creates a Filter View and sets itself as the Data Context. 
        /// </summary>
        /// <returns></returns>
        public FilterView CreateView()
        {
            FilterView view = new FilterView();
            view.DataContext = this;
            return view;
        }



    }
}
