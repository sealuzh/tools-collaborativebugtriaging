﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ExpertFinder.Planning;
using System.Runtime.Serialization;
using System.Reflection;
using System.Threading;

namespace ExpertFinder.Util
{
    /// <summary>
    /// Used to save objects persistently on the harddisk.
    /// </summary>
    /// <remarks>
    /// author: Katja Kevic
    /// date (yyyy mm dd): 2012 08 29
    /// </remarks>
    public static class Serializer 

    {
        public static void Serialize(string filename,
        ISerializable objectToSerialize)
        {
            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, objectToSerialize);
            stream.Close();
        }

        public static ISerializable DeSerialize(string filename)
        {
            ISerializable objectToSerialize;
            Stream stream = File.Open(filename, FileMode.Open);


            try
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToSerialize =
                    (ISerializable)bFormatter.Deserialize(stream);
                stream.Close();
                return objectToSerialize;
            }
            catch (TargetInvocationException e)
            {
                //Dialog_UnableToOpenLocalSource dialog = new Dialog_UnableToOpenLocalSource();
                //dialog.IsOpen = true;
                //Thread.Sleep(2000);

                stream.Close();
                return null;
            }

            
            
        }

 
    }
}
