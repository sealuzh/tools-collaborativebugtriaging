﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Collections.ObjectModel;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Triaging;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter.Triaging
{
    public class SelectWorkItemToAnalyzeViewModel : BaseViewModel
    {
        public ObservableCollection<WorkItemViewModel> WorkItemsToSelect { get; set; } //getter and setter needed because of the Binding Mechanism.
        private SelectWorkItemToAnalyzeView view;
        private ICollaborationPlatform platform;


        public SelectWorkItemToAnalyzeViewModel(ICollaborationPlatform collaborationPlatform)
        {
            WorkItemsToSelect = new ObservableCollection<WorkItemViewModel>();
            this.platform = collaborationPlatform;
        }


        /// <summary>
        /// Returns the work item to the corresponding ID. If the collaboration platform is not available an exception is thrown.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WorkItemModel FindWorkItemById(String id)
        {
            if (platform.IsCollaborationPlatformAvailable())
            {
                WorkItemModel model = platform.GetWorkItemService().GetWorkItem(Convert.ToInt32(id));
                return model;
            }
            else
            {
                throw new CollaborationPlatformUnavailableException("The collaboration platform is currently not available.");
            }
        }

        /// <summary>
        /// Queries the work items which contain the queryText in their title. If the collaboration platform is not available
        /// an exception is thrown.
        /// </summary>
        /// <param name="queryText"></param>
        /// <returns></returns>
        public void SetPossibleWorkItems(String queryText, ScatterView triaginContainer)
        {
            ICollection<WorkItemModel> possibleWorkItems = new List<WorkItemModel>();

            if (platform.IsCollaborationPlatformAvailable())
            {
                possibleWorkItems = platform.GetWorkItemService().GetWorkItemsContainingString(queryText);
            }
            else
            {
                throw new CollaborationPlatformUnavailableException("The collaboration platform is currently not available.");
            }

            WorkItemsToSelect.Clear();

            foreach (WorkItemModel model in possibleWorkItems)
            {
                WorkItemsToSelect.Add(model.GetViewModelType());
            }

            foreach (WorkItemViewModel workItemViewModel in WorkItemsToSelect)
            {
                workItemViewModel.TriagingContainer = triaginContainer;
            }
        }

        /// <summary>
        /// Assesses if possible work items are found.
        /// </summary>
        /// <returns></returns>
        public Boolean ArePossibleWorkItemsFound()
        {
            return WorkItemsToSelect.Count > 0 ? true : false;
        }

        /// <summary>
        /// Creates the SelectWorkItemToAnlayzeView and sets itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public SelectWorkItemToAnalyzeView CreateView()
        {
            view = new SelectWorkItemToAnalyzeView();
            view.DataContext = this;
            return view;
        }
    }
}
