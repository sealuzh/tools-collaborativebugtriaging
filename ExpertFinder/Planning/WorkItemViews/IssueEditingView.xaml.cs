﻿using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using System;
using ScrumSupporter.Planning;
using System.IO;

namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Interaktionslogik für IssueEditingView.xaml
    /// </summary>
    public partial class IssueEditingView : SurfaceUserControl
    {
        public IssueEditingView()
        {
            InitializeComponent();
        }

        private void OnOpenTriagingVIew(object sender, RoutedEventArgs e)
        {
            WorkItemViewModel dataContext = this.DataContext as WorkItemViewModel;
            dataContext.OnOpenTriaging(FindResource("sviStyle") as Style);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            String strokesPath = "Resources/Strokes/" + (this.DataContext as IssueViewModel).Id + ".isf";
            StrokesHandler strokesHandler = new StrokesHandler(strokesPath, inkCanvas, SaveInkButton, EraseInkButton, BackButton);
            try
            {
                strokesHandler.LoadStrokes();
            }
            catch (FileLoadException e1)
            {
                System.Console.WriteLine(e1.Message);
            }

            OpenInkCanvasButton.Click += strokesHandler.OnOpenInkCanvas;
        }
    }
}
