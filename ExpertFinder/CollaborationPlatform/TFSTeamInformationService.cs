﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using TfsLink.ProjectDashboard;
using TfsLink;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Implements the TeamInformationService of the Team Foundation Server.
    /// </summary>
    public class TFSTeamInformationService: ITeamInformationService
    {
        private ITfs tfsConnection;

        public TFSTeamInformationService(ITfs TfsConnection)
        {
            this.tfsConnection = TfsConnection;
        }

        public ICollection<UpcomingEvent> GetTeamEvents()
        {
            ICollection<UpcomingEvent> events = new List<UpcomingEvent>();
            Collection<CalendarItem> items= tfsConnection.GetCalendarItems();

            foreach (CalendarItem item in items)
            {
                UpcomingEvent model = new UpcomingEvent()
                {
                    Title = item.Title,
                    Description = item.Description,
                    Location = item.Location,
                    StartTime = (DateTime)item.StartTime,
                    EndTime = (DateTime)item.EndTime
                };
                events.Add(model);
            }
            return events;
        }
    }
}
