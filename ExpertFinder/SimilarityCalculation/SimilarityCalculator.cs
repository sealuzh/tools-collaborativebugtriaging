﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter
{
    /// <summary>
    /// Implements the ISimilarityCalculator Interface. Computes the List of similar work items.
    /// </summary>
    public class SimilarityCalculator: ISimilarityCalculator
    {
        /// <summary>
        /// Computes an ordered list of similar work items.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<SimilarWorkItemModel> GetSimilarWorkItems(WorkItemModel model, ICollaborationPlatform platform)
        {
            IList<SimilarWorkItemModel> similarWorkItems = new List< SimilarWorkItemModel>();

            VectorSpaceModel vsm = new VectorSpaceModel();
            //key: ID, value: vector
            Dictionary<WorkItemModel, ICollection<double>> DocumentVectors = vsm.GetFeatureVectors(platform);

            //get Query Vector of provided model
            ICollection<double> queryVector;
            if (DocumentVectors.ContainsKey(model))
            {
                DocumentVectors.TryGetValue(model, out queryVector);
            }
            else
            {
                //build the document vector
                queryVector = vsm.GetFeatureVectorOfNewWorkItem(model);
            }


            foreach (KeyValuePair<WorkItemModel, ICollection<double>> entry in DocumentVectors)
            {
                CosineSimilarityCalculator sim = new CosineSimilarityCalculator();
                double similarity = sim.CalculateCosineSimilarity(queryVector, entry.Value);

                similarWorkItems.Add(new SimilarWorkItemModel(entry.Key, similarity));
            }

            IEnumerable<SimilarWorkItemModel> sortedEnum = similarWorkItems.OrderByDescending(f => f.Similarity);
            IList<SimilarWorkItemModel> sortedList = sortedEnum.ToList();

            return sortedList;
        }
    }
}
