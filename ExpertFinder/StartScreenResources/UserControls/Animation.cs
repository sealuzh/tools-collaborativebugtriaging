﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;

namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// This is a Decrease and Increase animation of a slice.
    /// </summary>
    class Animation
    {

        public Storyboard getSliceAnimation(Path slice, TextBlock text, Storyboard sb)
        {
            DoubleAnimation widthAnimation = new DoubleAnimation();
            widthAnimation.From = slice.ActualWidth;
            widthAnimation.To = slice.ActualWidth - 30;
            widthAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            widthAnimation.AutoReverse = true;

            DoubleAnimation heightAnimation = new DoubleAnimation();
            heightAnimation.From = slice.ActualHeight;
            heightAnimation.To = slice.ActualHeight - 30;
            heightAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            heightAnimation.AutoReverse = true;

            DoubleAnimation fontSizeAnimation = new DoubleAnimation();
            fontSizeAnimation.From = text.FontSize;
            fontSizeAnimation.To = text.FontSize - 4;
            fontSizeAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            fontSizeAnimation.AutoReverse = true; 


            Storyboard.SetTarget(widthAnimation, slice);
            Storyboard.SetTargetProperty(widthAnimation, new PropertyPath(Path.WidthProperty));

            Storyboard.SetTarget(heightAnimation, slice);
            Storyboard.SetTargetProperty(heightAnimation, new PropertyPath(Path.HeightProperty));

            Storyboard.SetTarget(fontSizeAnimation, text);
            Storyboard.SetTargetProperty(fontSizeAnimation, new PropertyPath(TextBlock.FontSizeProperty));


            sb.Children.Add(heightAnimation);
            sb.Children.Add(widthAnimation);
            sb.Children.Add(fontSizeAnimation);

            return sb;

        }

    }
}
