﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.Controls
{
    /// <summary>
    /// Interaktionslogik für CustomFinishedButton.xaml
    /// </summary>
    public partial class CustomFinishedButton : SurfaceButton
    {
        public CustomFinishedButton()
        {
            InitializeComponent();
        }

        // Create a custom routed event by first registering a RoutedEventID 
        // This event uses the bubbling routing strategy 
        public static readonly RoutedEvent FinishedEvent = EventManager.RegisterRoutedEvent(
            "Finished", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CustomFinishedButton));

        // Provide CLR accessors for the event 
        public event RoutedEventHandler Finished
        {
            add { AddHandler(FinishedEvent, value); }
            remove { RemoveHandler(FinishedEvent, value); }
        }

        void RaiseFinishedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(CustomFinishedButton.FinishedEvent);
            RaiseEvent(newEventArgs);
        }

       //  For demonstration purposes we raise the event when the MyButtonSimple is clicked 
        public void OnFinished()
        {
            RaiseFinishedEvent();
        }




    }
}
