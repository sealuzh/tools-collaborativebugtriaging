﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Moq;
using System.Windows.Input;
using System.Reflection;
using ScrumSupporter.Planning;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform.Exceptions;
using ScrumSupporter.Planning.WorkItemViews;


namespace ScrumSupporter.Tests.Planning
{
    [TestFixture]
    public class WorkItemContainerViewModelTest
    {
        private WorkItemContainerViewModel workItemContainerViewModel;
        private ICollaborationPlatform platform;

        [TestFixtureSetUp]
        public void SetUp()
        {
            workItemContainerViewModel = new WorkItemContainerViewModel();
            platform = CreateMockCollaborationPlatform();
        }


        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            Credentials credentials = new Credentials("username", "password", "domain", "uri", "projectName");
            mockCollaborationPlatform.Setup(m => m.Login(credentials)).Returns(true);
            mockCollaborationPlatform.Setup(m => m.GetLoggedInPerson()).Returns("username");

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetBacklogWorkItems()).Returns(GetMockedUpBacklogItems());
            mockWorkItemService.Setup(m => m.GetWorkItemsOfIteration(It.IsAny<String>())).Returns(GetMockedUpBacklogItems());

            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);

            Mock<IIterationService> mockIterationService = new Mock<IIterationService>();
            mockIterationService.Setup(m => m.GetAllIterationNames()).Returns(GetMockedIterations());

            mockCollaborationPlatform.Setup(m => m.GetIterationService()).Returns(mockIterationService.Object);

            return mockCollaborationPlatform.Object;
        }


        private ICollection<WorkItemModel> GetMockedUpBacklogItems()
        {
            ICollection<WorkItemModel> backlogItems = new List<WorkItemModel>();

            WorkItemModel bugModel = new BugModel();
            backlogItems.Add(bugModel);

            WorkItemModel taskModel = new TaskModel();
            backlogItems.Add(taskModel);

            WorkItemModel issueModel = new IssueModel();
            backlogItems.Add(issueModel);

            WorkItemModel userStoryModel = new UserStoryModel();
            backlogItems.Add(userStoryModel);

            return backlogItems;
        }

        private ICollection<String> GetMockedIterations()
        {
            String iter1 = "Iteration 1";
            String iter2 = "Iteration 2";
            String iter3 = "Iteration 3";
            String iter4 = "Iteration 4";

            ICollection<String> iterations = new List<String>();
            iterations.Add(iter1);
            iterations.Add(iter2);
            iterations.Add(iter3);
            iterations.Add(iter4);
            return iterations;
        }

        private MethodInfo GetMethod(string methodName)
        {
            var method = this.workItemContainerViewModel.GetType()
                .GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);

            return method;
        }

        private void ClearData()
        {
            workItemContainerViewModel.BacklogList.Clear();
            workItemContainerViewModel.WorkItemsOfIterationContainerList.Clear();
            workItemContainerViewModel.AvailableIterations.Clear();
            workItemContainerViewModel.AvailableIterationsModels.Clear();
        }


        [Test(Description = "Load items and assert the counts of the backlog list, available iterations list and workitemsofiterationscontainer" +
            "List.")]
        public void LoadDataTest()
        {
            int countOfBacklogList = workItemContainerViewModel.BacklogList.Count;
            Assert.AreEqual(0, countOfBacklogList);
            int countOfWorkItemsOfIterationContainerList = workItemContainerViewModel.WorkItemsOfIterationContainerList.Count;
            Assert.AreEqual(0, countOfWorkItemsOfIterationContainerList);
            int countOfAvailableIterations = workItemContainerViewModel.AvailableIterations.Count;
            Assert.AreEqual(0, countOfAvailableIterations);
            int countOfAvailableIterationsModels = workItemContainerViewModel.AvailableIterationsModels.Count;
            Assert.AreEqual(0, countOfAvailableIterationsModels);

            workItemContainerViewModel.LoadData(platform);

            CollectionAssert.AllItemsAreInstancesOfType(workItemContainerViewModel.BacklogList, typeof(WorkItemViewModel));
            CollectionAssert.AllItemsAreInstancesOfType(workItemContainerViewModel.AvailableIterations, typeof(String));
            CollectionAssert.AllItemsAreInstancesOfType(workItemContainerViewModel.WorkItemsOfIterationContainerList, typeof(WorkItemsOfIterationContainerViewModel));

            int countOfBacklogListAfterLoading = workItemContainerViewModel.BacklogList.Count;
            Assert.AreEqual(4, countOfBacklogListAfterLoading);
            int countOfWorkItemsOfIterationContainerListAfterLoading = workItemContainerViewModel.WorkItemsOfIterationContainerList.Count;
            Assert.AreEqual(4, countOfWorkItemsOfIterationContainerListAfterLoading);
            int countOfAvailableIterationsAfterLoading = workItemContainerViewModel.AvailableIterations.Count;
            Assert.AreEqual(4, countOfAvailableIterationsAfterLoading);
            int countOfAvailableIterationsModelsAfterLoading = workItemContainerViewModel.AvailableIterationsModels.Count;
            Assert.AreEqual(4, countOfAvailableIterationsModelsAfterLoading);

            ClearData();
        }

        [Test]
        public void LoadDataNullTest()
        {
            Assert.Throws(typeof(CollaborationPlatformUnavailableException), new TestDelegate(LoadDataWithNullPlatform));
        }

        private void LoadDataWithNullPlatform()
        {
            workItemContainerViewModel.LoadData(null);
        }

        [Test]
        public void AddBugToBacklogTest()
        {
            int countOfBacklog = workItemContainerViewModel.BacklogList.Count;

            MethodInfo method = GetMethod("AddBugToBacklogExecute");
            method.Invoke(workItemContainerViewModel, null);
            Assert.AreEqual(countOfBacklog + 1, workItemContainerViewModel.BacklogList.Count);

            WorkItemViewModel bug = workItemContainerViewModel.BacklogList.Last();
            Assert.IsInstanceOf(typeof(BugViewModel), bug);

            Assert.AreEqual("new Bug", bug.Title);
            Assert.AreEqual("MP1201\\ScrumSupporter", bug.Area);
            Assert.AreEqual("MP1201", bug.Iteration);
            Assert.AreEqual("Active", bug.State);
            Assert.AreEqual("New", bug.Reason);
            Assert.AreEqual(2, (bug as BugViewModel).Priority);
            Assert.AreEqual("3 - Medium", (bug as BugViewModel).Severity);
            Assert.AreEqual("", bug.AssignedTo);

            ClearData();
        }


        [Test]
        public void AddTaskToBacklogTest()
        {
            int countOfBacklog = workItemContainerViewModel.BacklogList.Count;

            MethodInfo method = GetMethod("AddTaskToBacklogExecute");
            method.Invoke(workItemContainerViewModel, null);
            Assert.AreEqual(countOfBacklog + 1, workItemContainerViewModel.BacklogList.Count);

            WorkItemViewModel task = workItemContainerViewModel.BacklogList.Last();
            Assert.IsInstanceOf(typeof(TaskViewModel), task);

            Assert.AreEqual("new Task", task.Title);
            Assert.AreEqual("MP1201\\ScrumSupporter", task.Area);
            Assert.AreEqual("MP1201", task.Iteration);
            Assert.AreEqual("Active", task.State);
            Assert.AreEqual("New", task.Reason);
            Assert.AreEqual("", task.AssignedTo);
            Assert.AreEqual(2, (task as TaskViewModel).Priority);

            ClearData();
        }

        [Test]
        public void AddUserStoryToBacklogTest()
        {
            workItemContainerViewModel.LoadData(platform);

            int countOfBacklog = workItemContainerViewModel.BacklogList.Count;

            MethodInfo method = GetMethod("AddUserStoryToBacklogExecute");
            method.Invoke(workItemContainerViewModel, null);
            Assert.AreEqual(countOfBacklog + 1, workItemContainerViewModel.BacklogList.Count);

            WorkItemViewModel userStory = workItemContainerViewModel.BacklogList.Last();
            Assert.IsInstanceOf(typeof(UserStoryViewModel), userStory);

            Assert.AreEqual("new User Story", userStory.Title);
            Assert.AreEqual("MP1201\\ScrumSupporter", userStory.Area);
            Assert.AreEqual("MP1201", userStory.Iteration);
            Assert.AreEqual("Active", userStory.State);
            Assert.AreEqual("New", userStory.Reason);
            Assert.AreEqual("username", userStory.AssignedTo);
            Assert.AreEqual("As a <type of user> I want <some goal> so that <some reason>", (userStory as UserStoryViewModel).Description);

            ClearData();
        }

        [Test]
        public void AddIssueToBacklogTest()
        {
            workItemContainerViewModel.LoadData(platform);

            int countOfBacklog = workItemContainerViewModel.BacklogList.Count;

            MethodInfo method = GetMethod("AddIssueToBacklogExecute");
            method.Invoke(workItemContainerViewModel, null);
            Assert.AreEqual(countOfBacklog + 1, workItemContainerViewModel.BacklogList.Count);

            WorkItemViewModel issue = workItemContainerViewModel.BacklogList.Last();
            Assert.IsInstanceOf(typeof(IssueViewModel), issue);

            Assert.AreEqual("new Issue", issue.Title);
            Assert.AreEqual("MP1201\\ScrumSupporter", issue.Area);
            Assert.AreEqual("MP1201", issue.Iteration);
            Assert.AreEqual("Active", issue.State);
            Assert.AreEqual("New", issue.Reason);
            Assert.AreEqual("username", issue.AssignedTo);
            Assert.AreEqual(2, (issue as IssueViewModel).Priority);

            ClearData();
        }



    }
}
