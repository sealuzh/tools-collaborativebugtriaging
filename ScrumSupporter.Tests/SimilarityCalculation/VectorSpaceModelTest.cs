﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Moq;
using System.Collections.ObjectModel;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class VectorSpaceModelTest
    {
        private VectorSpaceModel vectorSpaceModel;
        private ICollaborationPlatform platform;

        private WorkItemModel bugModel;
        private WorkItemModel taskModel;
        private WorkItemModel issueModel;
        private WorkItemModel userStoryModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            vectorSpaceModel = new VectorSpaceModel();
            platform = CreateMockCollaborationPlatform();
        }

        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetAllWorkItems()).Returns(GetMockedUpWorkItems());

            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);

            return mockCollaborationPlatform.Object;
        }

        private ICollection<WorkItemModel> GetMockedUpWorkItems()
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();

            bugModel = new BugModel() { Title = "new Bug", StepsToReproduce = "These are the steps to reproduce", Id= 1 };
            items.Add(bugModel);

            taskModel = new TaskModel() { Title = "new task", Description = "This is the task description", Id= 2 };
            items.Add(taskModel);

            issueModel = new IssueModel() { Title = "new issue", Description = "This is the description of the issue", Id = 3 };
            items.Add(issueModel);

            userStoryModel = new UserStoryModel() { Title = "new user story", Description = "user story description", Id = 4 };
            items.Add(userStoryModel);

            return items;
        }


        [Test]
        public void GetFeatureVectorsTest()
        {
            Dictionary<WorkItemModel, ICollection<double>> result = vectorSpaceModel.GetFeatureVectors(platform);

            ICollection<double> bugVector =  result[bugModel];
            Assert.AreEqual(8, bugVector.Count);

            double[] bugVectorArray = bugVector.ToArray();
            Assert.That(bugVectorArray[0] != 0);
            Assert.That(bugVectorArray[1] != 0);
            Assert.That(bugVectorArray[2] != 0);
            Assert.That(bugVectorArray[3] == 0);
            Assert.That(bugVectorArray[4] == 0);
            Assert.That(bugVectorArray[5] == 0);
            Assert.That(bugVectorArray[6] == 0);
            Assert.That(bugVectorArray[7] == 0);

            ICollection<double> taskVector = result[taskModel];
            Assert.AreEqual(8, taskVector.Count);

            double[] taskVectorArray = taskVector.ToArray();
            Assert.That(taskVectorArray[0] == 0);
            Assert.That(taskVectorArray[1] == 0);
            Assert.That(taskVectorArray[2] == 0);
            Assert.That(taskVectorArray[3] != 0);
            Assert.That(taskVectorArray[4] != 0);
            Assert.That(taskVectorArray[5] == 0);
            Assert.That(taskVectorArray[6] == 0);
            Assert.That(taskVectorArray[7] == 0);

            ICollection<double> issueVector = result[issueModel];
            Assert.AreEqual(8, issueVector.Count);

            double[] issueVectorArray = issueVector.ToArray();
            Assert.That(issueVectorArray[0] == 0);
            Assert.That(issueVectorArray[1] == 0);
            Assert.That(issueVectorArray[2] == 0);
            Assert.That(issueVectorArray[3] == 0);
            Assert.That(issueVectorArray[4] != 0);
            Assert.That(issueVectorArray[5] != 0);
            Assert.That(issueVectorArray[6] == 0);
            Assert.That(issueVectorArray[7] == 0);


            ICollection<double> userStoryVector = result[userStoryModel];
            Assert.AreEqual(8, userStoryVector.Count);

            double[] userStoryVectorArray = userStoryVector.ToArray();
            Assert.That(userStoryVectorArray[0] == 0);
            Assert.That(userStoryVectorArray[1] == 0);
            Assert.That(userStoryVectorArray[2] == 0);
            Assert.That(userStoryVectorArray[3] == 0);
            Assert.That(userStoryVectorArray[4] != 0);
            Assert.That(userStoryVectorArray[5] == 0);
            Assert.That(userStoryVectorArray[6] != 0);
            Assert.That(userStoryVectorArray[7] != 0);

        }
    }
}
