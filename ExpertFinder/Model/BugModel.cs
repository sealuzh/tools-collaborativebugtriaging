﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.WorkItemViews;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    public class BugModel: WorkItemModel
    {

        private String resolvedReason;

        public String ResolvedReason
        {
            get { return resolvedReason; }
            set { resolvedReason = value; }
        }

        private int priority;

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        private String severity;

        public String Severity
        {
            get { return severity; }
            set { severity = value; }
        }

        private String stepsToReproduce;

        public String StepsToReproduce
        {
            get { return stepsToReproduce; }
            set { stepsToReproduce = value; }
        }


        public override WorkItemViewModel GetViewModelType()
        {
            return new BugViewModel(this);
        }

        /// <summary>
        /// Return the free text of the work item.
        /// </summary>
        /// <returns></returns>
        public override string GetText()
        {
            return this.Title + " " + this.StepsToReproduce;
        }

        public override WorkItem AdaptNewWorkItemToTfs(Project teamProject)
        {
            if (teamProject != null)
            {
                WorkItemType workItemType = teamProject.WorkItemTypes["Bug"];
                WorkItem workItem = new WorkItem(workItemType);
                
                return SetBugDetails(workItem);
            }
            else throw new ArgumentNullException("teamProject", "is null");
        }

        public override WorkItem AdaptExistingWorkItemToTfs(WorkItem workItem)
        {
            if (workItem != null)
            {
                return SetBugDetails(workItem);
            }
            else throw new ArgumentNullException("workItem", "is null");
        }

        private WorkItem SetBugDetails(WorkItem workItem)
        {
            workItem = SetCommonFields(workItem);

            AllowedValuesCollection allowedResolvedReasons = workItem.Fields["Resolved Reason"].AllowedValues;
            if (allowedResolvedReasons.Contains(this.ResolvedReason.ToString()))
            {
                workItem.Fields["Resolved Reason"].Value = this.ResolvedReason;
            }
            AllowedValuesCollection allowedPriorities = workItem.Fields["Priority"].AllowedValues;
            if (allowedPriorities.Contains(this.Priority.ToString()))
            {
                workItem.Fields["Priority"].Value = this.Priority;
            }

            AllowedValuesCollection allowedSeverities = workItem.Fields["Severity"].AllowedValues;
            if (allowedSeverities.Contains(this.Severity.ToString()))
            {
                workItem.Fields["Severity"].Value = this.Severity;
            }
            AllowedValuesCollection allowedReproSteps = workItem.Fields["Repro Steps"].AllowedValues;
            if (allowedReproSteps.Contains(this.StepsToReproduce.ToString()))
            {
                workItem.Fields["Repro Steps"].Value = this.StepsToReproduce;
            }


            return workItem;
        }
    }
}
