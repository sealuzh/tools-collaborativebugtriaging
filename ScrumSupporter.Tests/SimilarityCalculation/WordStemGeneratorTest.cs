﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class WordStemGeneratorTest
    {
        private WordStemGenerator wordStemGen;

        [TestFixtureSetUp]
        public void SetUp()
        {
            wordStemGen = new WordStemGenerator();
        }

        [Test]
        public void FindWordStemTest()
        {
            String stem = wordStemGen.FindWordStem("supports");
            Assert.AreEqual("support", stem);

            stem = wordStemGen.FindWordStem("developers");
            Assert.AreEqual("developer", stem);

            stem = wordStemGen.FindWordStem("asdfghjkl");
            Assert.AreEqual("asdfghjkl", stem);
        }

        [Test]
        public void FindNullWordStemTest()
        {
            String stem = wordStemGen.FindWordStem(null);
            Assert.AreEqual("", stem);
        }
    }
}
