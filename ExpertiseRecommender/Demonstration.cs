﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ExpertFinder.Model;
using TextProcessing;

namespace ExpertiseRecommender
{
    public class Demonstration
    {

        static void Main()
        {
            Lemma l = new Lemma();
            String stem = l.findLemma("initializes");


            //Tokeniser tok = new Tokeniser();
            // bool containsWord = tok.IsStopWord("its");



            //ISimilarityCalculator simi = new SimilarityCalculator();
            //SortedList<double, SimilarWorkItemModel> simis =  simi.GetSimilarWorkItems(32);

            //Dictionary<int, Collection<double>> FeatureVectors = VectorSpaceModel.getInstance().GetFeatureVectors();

            //// **** SOMEHOW RELATED ****
            //Collection<double> featureVector62;
            //FeatureVectors.TryGetValue(62, out featureVector62);

            //Collection<double> featureVector44;
            //FeatureVectors.TryGetValue(44, out featureVector44);


            //CosineSimilarity sim = new CosineSimilarity(featureVector62, featureVector44);
            //double similarity = sim.GetCosineSimilarity();

            //System.Console.WriteLine(similarity);
            

            ////*** COMPLETELY DIFFERENT ***
            //Collection<double> featureVector47;
            //FeatureVectors.TryGetValue(47, out featureVector47);

            //CosineSimilarity sim1 = new CosineSimilarity(featureVector47, featureVector44);
            //double similarity1 = sim1.GetCosineSimilarity();

            //System.Console.WriteLine(similarity1);


            ////*** THE SAME ****
            //CosineSimilarity sim2 = new CosineSimilarity(featureVector47, featureVector47);
            //double similarity2 = sim2.GetCosineSimilarity();

            //System.Console.WriteLine(similarity2);


            ////** with new text ***
            //String text = "This is the new text which is related to Profile Management";
            //Collection<double> textFeatureVector =  VectorSpaceModel.getInstance().getFeatureVectorOfText(text);

            ////user story 25 is related to Profile Mgmt
            //Collection<double> featureVector25;
            //FeatureVectors.TryGetValue(25, out featureVector25);

            //CosineSimilarity sim3 = new CosineSimilarity(featureVector25, textFeatureVector);
            //double similarity3 = sim3.GetCosineSimilarity();

            //System.Console.WriteLine(similarity3);


            System.Console.ReadLine();
        }

    }
}
