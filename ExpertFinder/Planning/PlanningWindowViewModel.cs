﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.IO;
using ExpertFinder.Model;
using ExpertFinder.Util;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System.Threading;
using TFS;
using ExpertFinder.Dialogs;
using Microsoft.Surface.Presentation.Controls;
using System.Windows;

namespace ExpertFinder.Planning
{
    /// <summary>
    /// ViewModel of the PlanningWindowView.
    /// </summary>
    public class PlanningWindowViewModel
    {
        private PlanningWindowView view;

        public PlanningWindowView CreateView()
        {
            var v = view ?? (view = new PlanningWindowView());
            v.DataContext = this;

            return v;
        }


        public ICommand OpenUserStories
        {
            get { return new RelayCommand(OpenUserStoriesExecute, CanOpenUserStoriesExecute); }
        }
        void OpenUserStoriesExecute()
        {

            LoaderWorkflow loader = new LoaderWorkflow();
            loader.start(view.DialogContainer);

        }

        bool CanOpenUserStoriesExecute()
        {
            return true;
        }





    }
}
