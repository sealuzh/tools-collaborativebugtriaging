﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Db4objects.Db4o;
using System.IO;
using Db4objects.Db4o.Ext;
using Db4objects.Db4o.Config;
using ScrumSupporter.Model;
using System.Collections.ObjectModel;
using ScrumSupporter.Planning;

namespace ScrumSupporter.LocalStorage
{
    public class Db4oConnection : DBConnection
    {

        private IObjectContainer db;
        private bool openSuccessfully = false;

        public IObjectContainer DB
        {
            get
            {
                if (openSuccessfully == true) return db;
                else throw new InvalidOperationException("DB not able to open");
            }
            set { db = value; }
        }

        private const String DATABASEPATH = "Resources/DBv1.db4o";

        public Db4oConnection()
        {
            try
            {
                db = Db4oFactory.OpenFile(DATABASEPATH);
                openSuccessfully = true;
            }
            catch (DatabaseFileLockedException e)
            {
                System.Console.WriteLine(e.StackTrace);
            }
            catch (Db4oIOException e1)
            {
                System.Console.WriteLine(e1.StackTrace);
            }
            catch (IncompatibleFileFormatException e2)
            {
                System.Console.WriteLine(e2.StackTrace);
            }
            catch (OldFormatException e3)
            {
                System.Console.WriteLine(e3.StackTrace);
            }
            catch (DatabaseReadOnlyException e4)
            {
                System.Console.WriteLine(e4.StackTrace);
            }
        }

        public override bool IsOpenSuccessful()
        {
            return openSuccessfully;
        }

        public override void LoadWorkItemModelsIntoDB(IEnumerable<WorkItemViewModel> models)
        {
            if (models != null)
            {
                foreach (WorkItemViewModel item in models)
                {
                    IList<WorkItemModel> result = DB.Query<WorkItemModel>(m => m.Id.Equals(item.Id));
                    foreach (object res in result)
                    {
                        DB.Delete(res);
                    }
                }

                foreach (WorkItemViewModel item in models)
                {
                    DB.Store(item.GetModel());
                }

                DB.Commit();
            }
            else throw new ArgumentNullException("models", "is null");
        }


        public override void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                DB.Close();
                DB.Dispose();
            }

        }

        public override ICollection<WorkItemModel> QueryBacklogItems()
        {
            IList<WorkItemModel> result = DB.Query<WorkItemModel>(m => m.Iteration.Equals("MP1201"));
            return result;
        }

        public override ICollection<WorkItemsOfIterationContainerViewModel> QueryItemsOfIterations()
        {
            ICollection<WorkItemsOfIterationContainerViewModel> itemsOfIterations = new List<WorkItemsOfIterationContainerViewModel>();
            HashSet<String> iterations = GetAllIterations();
            foreach (String iter in iterations)
            {
                IList<WorkItemModel> result = DB.Query<WorkItemModel>(m => m.Iteration.Equals(iter));
                IList<WorkItemViewModel> resultViewModels = new List<WorkItemViewModel>();
                foreach (WorkItemModel workItemModel in result)
                {
                    resultViewModels.Add(workItemModel.GetViewModelType());
                }

                WorkItemsOfIterationContainerViewModel viewModel = new WorkItemsOfIterationContainerViewModel(resultViewModels);
                itemsOfIterations.Add(viewModel);
            }
            return itemsOfIterations;
        }


        private HashSet<String> GetAllIterations()
        {
            HashSet<String> iterations = new HashSet<string>();
            IObjectSet result = DB.QueryByExample(typeof(WorkItemModel));
            foreach (WorkItemModel workItemModel in result)
            {
                if (!workItemModel.Iteration.Equals("MP1201"))
                {
                    iterations.Add(workItemModel.Iteration);
                }
            }
            return iterations;
        }
    }
}
