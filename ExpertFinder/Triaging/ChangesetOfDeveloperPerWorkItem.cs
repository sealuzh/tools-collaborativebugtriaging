﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Model;
using ScrumSupporter.Profile;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter.Triaging
{
    class ChangesetOfDeveloperPerWorkItem
    {
        private ICollection<ChangesetModel> changesets = new List<ChangesetModel>();

        public ChangesetOfDeveloperPerWorkItem(WorkItemViewModel workItem, ProfileViewModel profileModel)
        {

            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;
            if (platform.IsCollaborationPlatformAvailable())
            {
                ICollection<ChangesetModel> allChangesets = platform.GetVersionControlService().GetChangesetsOfWorkItem(workItem.Id);
                foreach (ChangesetModel changeset in allChangesets)
                {
                    String name = profileModel.TfsAccountName;
                    if (profileModel.TfsAccountName.Equals("Tim (0.88)"))
                    {
                        name = "ARKTOS\\mp1201-timo";
                    }
                    if (profileModel.TfsAccountName.Equals("Wendy (0.73)"))
                    {
                        name = "ARKTOS\\mp1201-wendy";
                    }
                    if (profileModel.TfsAccountName.Equals("Tom (0.44)"))
                    {
                        name = "ARKTOS\\mp1201-tom";
                    }


                    if (changeset.Committer.Equals(name))
                    {
                        changesets.Add(changeset);
                    }
                }
            }
        }

        public ICollection<ChangesetModel> GetChangesetsToSelect()
        {
            return changesets;
        }
        
    }
}
