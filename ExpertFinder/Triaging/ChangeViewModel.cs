﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;

namespace ScrumSupporter.Triaging
{
    public class ChangeViewModel: BaseViewModel
    {
        private String className;

        public String ClassName
        {
            get { return className; }
            set { className = value;
            NotifyOfPropertyChange(() => ClassName);
            }
        }

        private bool ticked = false;

        public bool Ticked
        {
            get { return ticked; }
            set { ticked = value;
            NotifyOfPropertyChange(() => Ticked);
            }
        }
        
        
    }
}
