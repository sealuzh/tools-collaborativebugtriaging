﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Moq;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Profile;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter.Tests.Profile
{
    [TestFixture]
    public class ProfileViewModelTest
    {
        private ICollaborationPlatform platform;
        private ProfileViewModel viewModel;
        private WorkItemModel bug;

        [TestFixtureSetUp]
        public void SetUp()
        {
            platform = CreateMockCollaborationPlatform();
            ProfileModel model = new ProfileModel() { TfsAccountName = "kevic"};
            viewModel = new ProfileViewModel(model, platform);
        }


        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            mockCollaborationPlatform.Setup(m => m.IsCollaborationPlatformAvailable()).Returns(true);

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetWorkItemsAssignedTo(It.IsAny<String>())).Returns(GetMockedWorkItems());


            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);

            Mock<IVersionControlService> mockVersionControlService = new Mock<IVersionControlService>();
            mockVersionControlService.Setup(m => m.GetChangesetsOfDeveloper(It.IsAny<String>())).Returns(CreateMockChangesets());

            mockCollaborationPlatform.Setup(m => m.GetVersionControlService()).Returns(mockVersionControlService.Object);

            return mockCollaborationPlatform.Object;
        }

        private ICollection<WorkItemModel> GetMockedWorkItems()
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();
            bug = new BugModel() { Id = 2, Title = "This is the bug title" };
            items.Add(bug);
            TaskModel task = new TaskModel() { Id = 3, Title = "this is the task title" };
            items.Add(task);
            return items;
        }

        private ICollection<ChangesetModel> CreateMockChangesets()
        {
            ICollection<ChangesetModel> items = new List<ChangesetModel>();
            ChangesetModel ch1 = new ChangesetModel() { Committer = "katja" };
            ChangesetModel ch2 = new ChangesetModel() { Committer = "kevic" };
            items.Add(ch1);
            items.Add(ch2);

            return items;
        }

        [Test]
        public void TestState()
        {
            Assert.AreEqual(2, viewModel.AssignedWorkItemsCount);
            Assert.AreEqual(2, viewModel.AssociatedChangesetsCount);
        }

        [Test]
        public void PlatformNullTest()
        {
            Assert.Throws(typeof(CollaborationPlatformUnavailableException), new TestDelegate(CreateProfileViewModelWithNullPlatform));
        }

        private void CreateProfileViewModelWithNullPlatform()
        {
            ProfileModel model = new ProfileModel() { TfsAccountName = "kevic" };
            ProfileViewModel nullModel = new ProfileViewModel(model, null);
        }

        [Test]
        public void ModelNullTest()
        {
            Assert.Throws(typeof(ArgumentNullException), new TestDelegate(CreateProfileViewModelWithNullModel));
        }

        private void CreateProfileViewModelWithNullModel()
        {
            ProfileModel model = null;
            ProfileViewModel nullModel = new ProfileViewModel(model, platform);
        }

    }
}
