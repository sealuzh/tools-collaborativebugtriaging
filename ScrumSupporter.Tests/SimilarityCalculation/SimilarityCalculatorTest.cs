﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Moq;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;

namespace ScrumSupporter.Tests.SimilarityCalculation
{
    [TestFixture]
    public class SimilarityCalculatorTest
    {
        private SimilarityCalculator calc;
        private ICollaborationPlatform platform;
        private WorkItemModel bugModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            calc = new SimilarityCalculator();
            platform = CreateMockCollaborationPlatform();

        }

        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetAllWorkItems()).Returns(GetMockedUpWorkItems());

            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);

            return mockCollaborationPlatform.Object;
        }

        private ICollection<WorkItemModel> GetMockedUpWorkItems()
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();

            bugModel = new BugModel() { Title = "new Bug", StepsToReproduce = "These are the steps to reproduce", Id = 1 };
            items.Add(bugModel);

            WorkItemModel taskModel = new TaskModel() { Title = "new task", Description = "This is the task description", Id = 2 };
            items.Add(taskModel);

            WorkItemModel issueModel = new IssueModel() { Title = "new issue", Description = "This is the description of the issue", Id = 3 };
            items.Add(issueModel);

            WorkItemModel userStoryModel = new UserStoryModel() { Title = "new user story", Description = "user story description", Id = 4 };
            items.Add(userStoryModel);

            return items;
        }

        [Test]
        public void GetSimilarWorkItemsTest()
        {
            IList<SimilarWorkItemModel> simis = calc.GetSimilarWorkItems(bugModel, platform);

            SimilarWorkItemModel itself = simis.First();
            Assert.AreEqual(bugModel, itself.WorkItem);

            Assert.AreEqual(4, simis.Count);
        }
    }
}
