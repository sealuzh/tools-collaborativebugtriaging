﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ScrumSupporter.Planning
{
    public class WorkItemsOfIterationContainerViewModel
    {

        public String Header { get; set; }
        public ObservableCollection<WorkItemViewModel> WorkItemList { get; set; } //getter and setter needed because of the Binding Mechanism.


        public WorkItemsOfIterationContainerViewModel(IList<WorkItemViewModel> list)
        {
            if (list != null)
            {
                WorkItemList = new ObservableCollection<WorkItemViewModel>();
                if (list.Count > 0)
                {
                    Header = list.First().Iteration;
                }

                foreach (var model in list)
                {
                    WorkItemList.Add(model);
                }
            }
            else throw new ArgumentNullException("list", "is null");
        }

        /// <summary>
        /// Sets the StackRank in ascending order. 
        /// </summary>
        public void SetStackRank()
        {
            int stackrank = 1;
            foreach (WorkItemViewModel workItem in WorkItemList)
            {
                workItem.StackRank = stackrank;
                stackrank++;
            }

        }

        public object DraggedElement
        {
            get;
            set;
        }
    }
}
