﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.Util;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScrumSupporter.Planning.WorkItemViews
{
    public class TaskViewModel : WorkItemViewModel
    {
        TaskModel taskModel;

        public TaskViewModel(WorkItemModel model)
            : base(model)
        {
            taskModel = model as TaskModel;
        }

        public String Description
        {
            get
            {
                return taskModel.Description;
            }
            set
            {
                taskModel.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public String Activity
        {
            get
            {
                return taskModel.Activity;
            }
            set
            {
                taskModel.Activity = value;
                NotifyOfPropertyChange(() => Activity);
            }
        }

        public int Priority
        {
            get
            {
                return taskModel.Priority;
            }
            set
            {
                taskModel.Priority = value;
                NotifyOfPropertyChange(() => Priority);
            }
        }

        public double OriginalEstimate
        {
            get
            {
                return taskModel.OriginalEstimate;
            }
            set
            {
                taskModel.OriginalEstimate = value;
                NotifyOfPropertyChange(() => OriginalEstimate);
            }
        }

        public double Remaining
        {
            get
            {
                return taskModel.Remaining;
            }
            set
            {
                taskModel.Remaining = value;
                NotifyOfPropertyChange(() => Remaining);
            }
        }

        public double Completed
        {
            get
            {
                return taskModel.Completed;
            }
            set
            {
                taskModel.Completed = value;
                NotifyOfPropertyChange(() => Completed);
            }
        }

        protected override Point CalculatePosition(Random random)
        {
            Point position = new Point();
            if (random != null)
            {
                position.X = random.Next((Constants.BACKLOGWIDTH / 2) + 100, (Constants.BACKLOGWIDTH - 100));
                position.Y = random.Next(100, (Constants.BACKLOGHEIGHT / 2 - 100));
                return position;
            }
            position.X = (Constants.BACKLOGWIDTH / 2) + 100;
            position.Y = 100;
            return position;
        }

        /// <summary>
        /// Looks up the DataTemplate in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplate()
        {
            return Application.Current.Resources["TaskViewModelTemplate"] as DataTemplate;
        }

        /// <summary>
        /// Looks up the DataTemplate for the singleIteration view in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplateForSingleIterationView()
        {
            return Application.Current.Resources["TaskViewModelTemplateForSingleIterationView"] as DataTemplate;
        }

        /// <summary>
        /// Creates an TaskView in a ScatterViewItem and sets itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public override ScatterViewItem GetView()
        {
            ScatterViewItem svi = GetScatterViewItemConfigsOfTheView();
            TaskView view = new TaskView();
            view.DataContext = this;
            //view.DisableAnalyzeButton();
            svi.Content = view;
            return svi;
        }

        protected override Style GetStyleOfCursorVisual()
        {
            return Application.Current.Resources["CursorStyleTaskBacklogView"] as Style;
        }

        protected override SurfaceUserControl GetSpecificEditingView()
        {
            return new TaskEditingView();
        }
    }
}
