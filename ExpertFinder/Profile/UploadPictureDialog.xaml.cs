﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.Profile
{
    /// <summary>
    /// Interaktionslogik für UploadPictureDialog.xaml
    /// </summary>
    public partial class UploadPictureDialog : SurfaceUserControl
    {
        public UploadPictureDialog()
        {
            InitializeComponent();
        }

        private void uploadButton_Click(object sender, RoutedEventArgs e)
        {
            string sourcePath = picturePath.Text;
            string pictureName = sourcePath.Substring(sourcePath.LastIndexOf("\\")+1);
                
           // string targetPath = @"C:\Users\Katja\Documents\Visual Studio 2008\Projects\ExpertFinder\ExpertFinder\Images\"+pictureName;

            string currentDir = Environment.CurrentDirectory;
            currentDir = currentDir.Replace("\\\\","\\");
            string targetPath = @currentDir.Substring(0, currentDir.IndexOf("bin")) +"Images\\" + pictureName;

            System.IO.File.Copy(sourcePath, targetPath, true);

            picturePath.Text = @"/ExpertFinder;component/Images/"+pictureName;

            this.Visibility = Visibility.Hidden;
        }
    }
}
