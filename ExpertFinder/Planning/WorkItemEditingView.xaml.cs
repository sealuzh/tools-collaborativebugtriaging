﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Manipulations;
using System.Windows.Ink;
using ScrumSupporter.Util;

namespace ScrumSupporter.Planning
{
    /// <summary>
    /// Interaktionslogik für WorkItemEditingView.xaml
    /// </summary>
    public partial class WorkItemEditingView : SurfaceWindow
    {

        private Affine2DManipulationProcessor manipulationProcessor = null;
        //private Affine2DInertiaProcessor inertiaProcessor = null;
        private double rotation;
        private RotateTransform rt = new RotateTransform();

        DateTime oldTime = DateTime.Now;


        DrawingAttributes inkDA;

        public WorkItemEditingView()
        {
            InitializeComponent();
            InitializeManipulationIntertiaProcessor();
            InitializeInkCanvas();
           
        }


        private void InitializeInkCanvas()
        {
            inkDA = new DrawingAttributes();
            inkDA.Color = Colors.Black;
            inkDA.Height = 2;
            inkDA.Width = 2;
            inkCanvas.DefaultDrawingAttributes = inkDA;
        }


        private void InitializeManipulationIntertiaProcessor()
        {
            manipulationProcessor =
                new Affine2DManipulationProcessor(Affine2DManipulations.Rotate, note);
            manipulationProcessor.Affine2DManipulationDelta +=
                new EventHandler<Affine2DOperationDeltaEventArgs>(manipulations_Delta);
            manipulationProcessor.Affine2DManipulationCompleted +=
                new EventHandler<Affine2DOperationCompletedEventArgs>(manipulations_Completed);
            note.RenderTransformOrigin = new Point(0.5, 0.5);

            manipulationProcessor.PivotPoint = new Point(0.5, 0.5);
            manipulationProcessor.PivotRadius = 320;

            //inertiaProcessor = new Affine2DInertiaProcessor();
            //inertiaProcessor.Affine2DInertiaDelta += manipulations_Delta;

        }

        private void note_ContactDown(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            manipulationProcessor.BeginTrack(e.Contact);
        }

        private void manipulations_Delta(object sender, Affine2DOperationDeltaEventArgs e)
        {
            TransformGroup tg = new TransformGroup();
            //RotateTransform rt = new RotateTransform(rotation + e.CumulativeRotation);

            rt.Angle += e.RotationDelta;
            tg.Children.Add(rt);
            note.RenderTransform = tg;
        }

        private void manipulations_Completed(object sender, Affine2DOperationCompletedEventArgs e)
        {
            rotation += e.TotalRotation;

            ManipulationsUtil maniUtil = new ManipulationsUtil();
            Affine2DInertiaProcessor inertiaProcessor =
                maniUtil.GetInertiaProcessor(new Thickness(0, 0, this.ActualWidth, this.ActualHeight), e);

            inertiaProcessor.Affine2DInertiaDelta += manipulations_Delta;
            try
            {
                inertiaProcessor.Begin();
            }
            catch (InvalidOperationException e1)
            {
                System.Console.WriteLine(e1.Message);
            }

            

        }

        private void SurfaceWindow_ContactDown(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            if (e.OriginalSource is Border)
            {
                DateTime time = DateTime.Now;

                TimeSpan interval = time.Subtract(oldTime);

                if (interval.TotalMilliseconds < 500)
                {
                    //this.Close();
                    this.Visibility = Visibility.Hidden;
                }
                oldTime = time;
            }
        }


        private void OpenInkCanvas(object sender, RoutedEventArgs e)
        {
            inkCanvas.Visibility = Visibility.Visible;
            SaveInkButton.Visibility = Visibility.Visible;
            EraseInkButton.Visibility = Visibility.Visible;
            BackButton.Visibility = Visibility.Visible;
        }


        private void eraseButtonClick(object sender, RoutedEventArgs e)
        {
            inkCanvas.Strokes.Clear();
            
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            inkCanvas.Visibility = Visibility.Hidden;
            SaveInkButton.Visibility = Visibility.Hidden;
            EraseInkButton.Visibility = Visibility.Hidden;
            BackButton.Visibility = Visibility.Hidden;
        }

    }
}
