﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;

namespace ScrumSupporter.Model
{
    public class UpcomingEvent: BaseViewModel
    {

        private String title;

        public String Title
        {
            get { return title; }
            set { title = value;
            NotifyOfPropertyChange(() => Title);
            }
        }

        private String description;

        public String Description
        {
            get { return description; }
            set { description = value;
            NotifyOfPropertyChange(() => Description);
            }
        }

        private DateTime startTime;

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value;
            NotifyOfPropertyChange(() => StartTime);
            }
        }

        private DateTime endTime;

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value;
            NotifyOfPropertyChange(() => EndTime);
            }
        }

        private String location;

        public String Location
        {
            get { return location; }
            set { location = value;
            NotifyOfPropertyChange(() => Location);
            }
        }
        
        

    }
}
