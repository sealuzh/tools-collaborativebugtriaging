﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Client;

namespace TfsLink
{
    /// <summary>
    /// Interface to connect to the TFS.
    /// </summary>
    public interface IConnection
    {

        /// <summary>
        /// Tries to connect to the TFS server with the given credentials. 
        /// </summary>
        /// <returns>true, if the authetification passed.</returns>
        Boolean Connect();

        /// <summary>
        /// Returns the Team project.
        /// </summary>
        TfsTeamProjectCollection GetTeamProjectCollection();
    }
}
