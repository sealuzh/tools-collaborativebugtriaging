﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;
using System.Collections.ObjectModel;
using ScrumSupporter.CollaborationPlatform;
using System.Windows.Controls;

namespace ScrumSupporter.Triaging
{
    class ChangesetsToSelectViewModel
    {
        private ObservableCollection<ChangesetModel> changesetsToSelect = new ObservableCollection<ChangesetModel>();
        private Grid hostGrid;

        public ObservableCollection<ChangesetModel> GetChangesetsToSelect()
        {
            return changesetsToSelect;
        }

        public void SetChangesetsToSelect(ICollection<ChangesetModel> changesets)
        {
            foreach (ChangesetModel ch in changesets)
            {
                ch.SetHostingGrid(hostGrid);
                changesetsToSelect.Add(ch);
            }

        }

        public void SetHostingContainer(Grid grid)
        {
            this.hostGrid = grid;
        }

        public Grid GetHostingContainer()
        {
            return hostGrid;
        }



    }
}
