﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;

namespace ScrumSupporter
{
    /// <summary>
    /// This class comprises text preprocessing capabilities: decides if a word is a stop word, finds the stem of a word, removes punctuation, tokenises a sentence. 
    /// </summary>
    public class TextProcessor
    {
        private const String STOPWORDSURI = "http://jmlr.csail.mit.edu/papers/volume5/lewis04a/a11-smart-stop-list/english.stop";
        private ICollection<String> stopWords = new HashSet<String>();
        private WordStemGenerator wordStemGenerator = new WordStemGenerator();

        /// <summary>
        /// Constructor. Loads the list of the stop words.
        /// </summary>
        public TextProcessor()
        {
            SetStopWords();
        }

        /// <summary>
        /// Loads the list of stopwords.
        /// </summary>
        private void SetStopWords()
        {
            try
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                string webData = wc.DownloadString(STOPWORDSURI);
                
                foreach (String s in webData.Split('\n'))
                {
                    stopWords.Add(s);
                }

            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
            catch (NotSupportedException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Evaluates according to a list of words if the given words is classified as a stopword. 
        /// </summary>
        /// <param name="word"></param>
        /// <returns>true, if it is classified stopword and false if the word is not classified as stopword.</returns>
        private bool IsStopWord(String word)
        {
            return stopWords.Contains(word);
        }

        /// <summary>
        /// Tries to find the stem of the given word.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private String GetWordStem(String word)
        {
            return wordStemGenerator.FindWordStem(word);
        }

        /// <summary>
        /// Splits a given sentence into words and removes punctuation and stopwords.
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns>A Collection of word stems.</returns>
        public ICollection<String> GetTokens(string sentence)
        {
            ICollection<String> tokens = new List<string>();
            if (sentence != null)
            {
                string lowerCasedSentece = sentence.ToLower();
                foreach (string s in lowerCasedSentece.Split(new char[] { ' ', ',', '.', '(', ')', '?', '!', '"' }))
                {
                    if (s.Length != 0 && !IsStopWord(s))
                    {
                        String stem = GetWordStem(s);
                        tokens.Add(stem);
                    }
                }
            }
            return tokens;
        }

    }
}
