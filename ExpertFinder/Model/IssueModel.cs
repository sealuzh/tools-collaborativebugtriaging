﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.WorkItemViews;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    public class IssueModel:WorkItemModel
    {

        private String desc;

        public String Description
        {
            get { return desc; }
            set { desc = value; }
        }

        private int priority;

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        private DateTime dueDate;

        public DateTime DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }


        public override WorkItemViewModel GetViewModelType()
        {
            return new IssueViewModel(this);
        }

        /// <summary>
        /// Return the free text of the work item.
        /// </summary>
        /// <returns></returns>
        public override string GetText()
        {
            return this.Title + " " + this.Description;
        }

        public override WorkItem AdaptNewWorkItemToTfs(Project teamProject)
        {
            if (teamProject != null)
            {
                WorkItemType workItemType = teamProject.WorkItemTypes["Issue"];
                WorkItem workItem = new WorkItem(workItemType);
                return SetIssueDetails(workItem);
            }
            else throw new ArgumentNullException("teamProject", "is null");
        }

        public override WorkItem AdaptExistingWorkItemToTfs(WorkItem workItem)
        {
            if (workItem != null)
            {
                return SetIssueDetails(workItem);
            }
            else throw new ArgumentNullException("workItem", "is null");
        }

        private WorkItem SetIssueDetails(WorkItem workItem)
        {
            workItem = SetCommonFields(workItem);
            workItem.Description = this.Description;

            AllowedValuesCollection allowedPriorities = workItem.Fields["Priority"].AllowedValues;
            if (allowedPriorities.Contains(this.Priority.ToString()))
            {
                workItem.Fields["Priority"].Value = this.Priority;
            }
            AllowedValuesCollection allowedDueDates = workItem.Fields["Due Date"].AllowedValues;
            if (allowedDueDates.Contains(this.DueDate.ToString()))
            {
                workItem.Fields["Due Date"].Value = this.DueDate;
            }
            return workItem;
        }
    }
}
