﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using Moq;
using ScrumSupporter.Triaging;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;
using ScrumSupporter.Profile;
using ScrumSupporter.CollaborationPlatform.Exceptions;
using ScrumSupporter.Planning.WorkItemViews;


namespace ScrumSupporter.Tests.Triaging
{
    [TestFixture]
    public class TriagingContainerViewModelTest
    {
        private TriagingContainerViewModel viewModel;
        private TriagingContainerViewModel unavailableViewModel;
        private ICollaborationPlatform platform;
        private ICollaborationPlatform unavailablePlatform;
        private WorkItemModel bug;
        private WorkItemModel wiModel;
        private WorkItemViewModel wiViewModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            wiModel = new BugModel() { Id = 10, Title = "This is the bug title" };
            wiViewModel = wiModel.CreateWorkItemViewModel();
            platform = CreateMockCollaborationPlatform();
            unavailablePlatform = CreateUnavailableMockCollaborationPlatform();
            WorkItemViewModel analyzedWI = new BugViewModel(bug);
            viewModel = new TriagingContainerViewModel(analyzedWI, platform);
        }



        private ICollaborationPlatform CreateMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            mockCollaborationPlatform.Setup(m => m.IsCollaborationPlatformAvailable()).Returns(true);

            Mock<IWorkItemService> mockWorkItemService = new Mock<IWorkItemService>();
            mockWorkItemService.Setup(m => m.GetAllWorkItems()).Returns(GetMockedWorkItems());
            mockWorkItemService.Setup(m => m.GetWorkItemsAssignedTo(It.IsAny<String>())).Returns(GetMockedWorkItems());


            mockCollaborationPlatform.Setup(m => m.GetWorkItemService()).Returns(mockWorkItemService.Object);

            Mock<IVersionControlService> mockVersionControlService = new Mock<IVersionControlService>();
            mockVersionControlService.Setup(m => m.GetChangesetsOfWorkItem(It.IsAny<Int32>())).Returns(CreateMockChangesets());
            mockVersionControlService.Setup(m => m.GetChangesetsOfDeveloper(It.IsAny<String>())).Returns(CreateMockChangesets());

            mockCollaborationPlatform.Setup(m => m.GetVersionControlService()).Returns(mockVersionControlService.Object);

            return mockCollaborationPlatform.Object;
        }

        private ICollection<ChangesetModel> CreateMockChangesets()
        {
            ICollection<ChangesetModel> items = new List<ChangesetModel>();
            ChangesetModel ch1 = new ChangesetModel() { Committer = "katja" };
            ChangesetModel ch2 = new ChangesetModel() { Committer = "kevic" };
            items.Add(ch1);
            items.Add(ch2);

            return items;
        }

        private ICollaborationPlatform CreateUnavailableMockCollaborationPlatform()
        {
            Mock<ICollaborationPlatform> mockCollaborationPlatform = new Mock<ICollaborationPlatform>();
            mockCollaborationPlatform.Setup(m => m.IsCollaborationPlatformAvailable()).Returns(false);

            return mockCollaborationPlatform.Object;
        }

        private ICollection<WorkItemModel> GetMockedWorkItems()
        {
            ICollection<WorkItemModel> items = new List<WorkItemModel>();
            bug = new BugModel() { Id = 2, Title = "This is the bug title"};
            items.Add(bug);
            TaskModel task = new TaskModel() { Id = 3, Title="this is the task title"};
            items.Add(task);
            return items;
        }

        [Test]
        public void TestUnavailablePlatformException()
        {
            Assert.Throws(typeof(CollaborationPlatformUnavailableException), new TestDelegate(GenerateUnavailablePlatformException));
        }

        private void GenerateUnavailablePlatformException()
        {
            WorkItemViewModel analyzedWI = new BugViewModel(bug);
            unavailableViewModel = new TriagingContainerViewModel(analyzedWI, unavailablePlatform);
        }

        private ProfileViewModel CreateMockProfileViewModel()
        {
            ProfileModel p = new ProfileModel();
            p.ConnectedWorkItems.Add(wiModel, 5);
            ProfileViewModel profile = new ProfileViewModel(p, platform);

            return profile;
        }


        [Test]
        public void GetNumberOfChangesetsTest()
        {
            int result = viewModel.GetNumberOfChangesets(CreateMockProfileViewModel(), wiViewModel);
            Assert.AreEqual(5, result);
        }

        [Test]
        public void GetNumberOfChangesetsProfileViewModelNullTest()
        {
            int result = viewModel.GetNumberOfChangesets(null, wiViewModel);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetNumberOfChangesetsWorkItemViewModelNullTest()
        {
            int result = viewModel.GetNumberOfChangesets(CreateMockProfileViewModel(), null);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetSimilarityTest()
        {
            double simi = viewModel.GetSimilarity(wiViewModel);
            Assert.AreEqual(0, simi);
        }

        [Test]
        public void GetSimilarityNullTest()
        {
            double simi = viewModel.GetSimilarity(null);
            Assert.AreEqual(0, simi);
        }

    }
}
