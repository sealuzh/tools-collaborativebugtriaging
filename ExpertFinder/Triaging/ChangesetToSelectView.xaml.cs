﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using System.Windows.Forms.Integration;
using ScrumSupporter.Util;

namespace ScrumSupporter.Triaging
{
    /// <summary>
    /// Interaktionslogik für ChangesetToSelectView.xaml
    /// </summary>
    public partial class ChangesetToSelectView : SurfaceUserControl
    {
        private DateTime oldTime;
        public ChangesetToSelectView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;
        }

        private WindowsFormsHost host;
        private void changesListBox_PreviewContactDown(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            DateTime now = DateTime.Now;
            TimeSpan interval = now.Subtract(oldTime);

            if (interval.TotalMilliseconds < Constants.DOUBLECLICKLIMIT)
            {
                System.Console.WriteLine("CONTACT DOWN");
                System.Console.WriteLine((changesListBox.SelectedItem as ChangeViewModel).ClassName);
                String className = (changesListBox.SelectedItem as ChangeViewModel).ClassName;

                //OPEN WINMERGE

                ChangesetModel dataContext = this.DataContext as ChangesetModel;
                Grid grid = dataContext.GetHostingGrid();
                host = dataContext.GetChangesViewInWinMerge(className);
                grid.Children.Add(host);
            }
            oldTime = DateTime.Now;




        }
    }
}
