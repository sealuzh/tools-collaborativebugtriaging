﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Manipulations;
using ScrumSupporter.Util;
using ScrumSupporter.Planning;
using ScrumSupporter.Profile;

namespace ScrumSupporter.Profile
{
    /// <summary>
    /// Interaktionslogik für ProfileView.xaml
    /// </summary>
    public partial class ProfileView : SurfaceUserControl
    {
        private DateTime oldTime;
        private SurfaceListBox listBox;
        private Affine2DManipulationProcessor manipulationProcessorDetailWorkItemView;
        private Affine2DManipulationProcessor manipulationProcessorWorkItems;
        private Affine2DManipulationProcessor manipulationProcessorChangesets;
        private ScatterView containerForDetailWorkItemView;
        private ScatterView workItemsContainer;
        private ScatterView changesetsContainer;

        public ProfileView()
        {
            InitializeComponent();
            oldTime = DateTime.Now;
        }

        /// <summary>
        /// Opens the list of all work items associated to the developer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openWorkItemsButton_Click(object sender, RoutedEventArgs e)
        {
            workItemsContainer = (this.DataContext as ProfileViewModel).AssociatedWorkItemsContainer;
            ScatterViewItem sviWorkItems = CreateWorkItemsScatterViewItem();

            sviWorkItems.PreviewContactDown += sviWorkItemsContactDown;
            manipulationProcessorWorkItems = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviWorkItems);
            manipulationProcessorWorkItems.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(sviWorkItemsManipulationCompleted);

            workItemsContainer.Items.Clear();
            workItemsContainer.Items.Add(sviWorkItems);
        }

        /// <summary>
        /// Evaluates if the sviWorkItems view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviWorkItemsManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                Affine2DInertiaProcessor inertiaProcessor = new Affine2DInertiaProcessor();
                inertiaProcessor.InitialOrigin = e.ManipulationOrigin;

                inertiaProcessor.DesiredAngularDeceleration = .0001;
                inertiaProcessor.DesiredDeceleration = .0001;
                inertiaProcessor.DesiredExpansionDeceleration = .0010;
                inertiaProcessor.Bounds = new Thickness(0, 0, workItemsContainer.ActualWidth, workItemsContainer.ActualHeight);
                inertiaProcessor.ElasticMargin = new Thickness(20);

                inertiaProcessor.InitialVelocity = e.Velocity;
                inertiaProcessor.InitialExpansionVelocity = e.ExpansionVelocity;
                inertiaProcessor.InitialAngularVelocity = e.AngularVelocity;

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(sviWorkItemsInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    workItemsContainer.Items.Clear();
                }

            }
        }

        /// <summary>
        /// Removes the sviWorkItems ScatterViewItem from the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviWorkItemsInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            workItemsContainer.Items.Clear();
        }


        /// <summary>
        /// Creates a SurfaceListBox in a ScatterViewItem which holds all work items assigned to the developer.
        /// </summary>
        /// <returns></returns>
        private ScatterViewItem CreateWorkItemsScatterViewItem()
        {
            Color neutralColor = (Color)FindResource("NeutralColor");
            DialogContainerConfigurer config = new DialogContainerConfigurer();
            ScatterViewItem item = config.GetScatterViewItemContainer();

            listBox = config.GetListBoxConfigs(neutralColor);
            listBox.ItemsSource = (this.DataContext as ProfileViewModel).GetAssignedWorkItems();


            WorkItemTemplateSelector templateSelector = new WorkItemTemplateSelector();
            listBox.ItemTemplateSelector = templateSelector;

            listBox.PreviewContactDown += ListBoxPreviewContactDown;
            item.ScatterManipulationDelta += SviManipulationDelta;

            item.Content = config.GetContectOfScatterViewItem(neutralColor, listBox); ;

            return item;
        }


        /// <summary>
        /// Prevents the ScatterViewItem from sliding out the reachable container. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviManipulationDelta(object sender, ScatterManipulationDeltaEventArgs e)
        {
            if ((sender as ScatterViewItem).ActualCenter.X < 128)
            {
                (sender as ScatterViewItem).Center = new Point(128, (sender as ScatterViewItem).ActualCenter.Y);
            }
        }

        /// <summary>
        /// Opens the detailed view of a work item if a double click occured.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBoxPreviewContactDown(object sender, ContactEventArgs e)
        {
            DateTime now = DateTime.Now;
            TimeSpan interval = now.Subtract(oldTime);

            containerForDetailWorkItemView = (this.DataContext as ProfileViewModel).DetailWorkItemContainer;

            //if a double click occurs the Detail work item view opens.
            if (interval.TotalMilliseconds < Constants.DOUBLECLICKLIMIT)
            {
                if (listBox.Items.CurrentItem is WorkItemViewModel)
                {
                    containerForDetailWorkItemView.Items.Clear();
                    WorkItemViewModel model = listBox.Items.CurrentItem as WorkItemViewModel;

                    ScatterViewItem sviDetailWorkItem = model.GetView();

                    sviDetailWorkItem.PreviewContactDown += SviDetailWorkItemContactDown;
                    manipulationProcessorDetailWorkItemView = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviDetailWorkItem);
                    manipulationProcessorDetailWorkItemView.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviDetailWorkItemViewManipulationCompleted);

                    containerForDetailWorkItemView.Items.Add(sviDetailWorkItem);
                }
            }
            oldTime = DateTime.Now;
        }

        /// <summary>
        /// Starts the manipulation processor of the DetailWorkItem view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorDetailWorkItemView.BeginTrack(e.Contact);
        }

        /// <summary>
        /// Evaluates if the DetailWorkItem view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemViewManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                Affine2DInertiaProcessor inertiaProcessor = new Affine2DInertiaProcessor();
                inertiaProcessor.InitialOrigin = e.ManipulationOrigin;

                inertiaProcessor.DesiredAngularDeceleration = .0001;
                inertiaProcessor.DesiredDeceleration = .0001;
                inertiaProcessor.DesiredExpansionDeceleration = .0010;
                inertiaProcessor.Bounds = new Thickness(0, 0, containerForDetailWorkItemView.ActualWidth, containerForDetailWorkItemView.ActualHeight);
                inertiaProcessor.ElasticMargin = new Thickness(20);

                inertiaProcessor.InitialVelocity = e.Velocity;
                inertiaProcessor.InitialExpansionVelocity = e.ExpansionVelocity;
                inertiaProcessor.InitialAngularVelocity = e.AngularVelocity;

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviDetailWorkItemViewInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    containerForDetailWorkItemView.Items.Clear();
                }

            }
        }

        /// <summary>
        /// Removes the DetailWorkItemView from the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviDetailWorkItemViewInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            containerForDetailWorkItemView.Items.Clear();
        }

        /// <summary>
        /// Starts the manipulation processor of the sviWorkItems view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sviWorkItemsContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorWorkItems.BeginTrack(e.Contact);
        }

        /// <summary>
        /// Opens a list of all changesets associated to the developer. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openChangesetsButton_Click(object sender, RoutedEventArgs e)
        {
            changesetsContainer = (this.DataContext as ProfileViewModel).AssociatedChangesetsContainer;
            ScatterViewItem sviChangesets = CreateChangesetsScatterViewItem();


            sviChangesets.PreviewContactDown += SviChangesetsContactDown;
            manipulationProcessorChangesets = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviChangesets);
            manipulationProcessorChangesets.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetsManipulationCompleted);

            changesetsContainer.Items.Clear();
            changesetsContainer.Items.Add(sviChangesets);
        }


        /// <summary>
        /// Evaluates if the Changesets view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviChangesetsManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor = 
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, changesetsContainer.ActualWidth, changesetsContainer.ActualHeight), e);

                try
                {
                    inertiaProcessor.Begin();
                    inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviChangesetsInertiaCompleted);
                }
                catch (InvalidOperationException e1)
                {
                    System.Console.WriteLine(e1.Message);
                    changesetsContainer.Items.Clear();
                }

            }
        }

        /// <summary>
        /// Removes the Changesets from the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviChangesetsInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            changesetsContainer.Items.Clear();
        }

        /// <summary>
        /// Starts the manipulation processor of the Changesets view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviChangesetsContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorChangesets.BeginTrack(e.Contact);
        }

        /// <summary>
        /// Creates a SurfaceListBox in a ScatterViewItem which holds all the associated changesets of a developer.
        /// </summary>
        /// <returns></returns>
        private ScatterViewItem CreateChangesetsScatterViewItem()
        {
            Color neutralColor = (Color)FindResource("NeutralColor");
            DialogContainerConfigurer config = new DialogContainerConfigurer();

            ScatterViewItem item = config.GetScatterViewItemContainer();

            SurfaceListBox changesetListBox = config.GetListBoxConfigs(neutralColor);
            changesetListBox.ItemsSource = (this.DataContext as ProfileViewModel).GetAssociatedChangesets();

            FrameworkElementFactory dataTemplateFactoryPanel = new FrameworkElementFactory(typeof(ChangesetView));
            DataTemplate dataTemplate = new DataTemplate();
            dataTemplate.VisualTree = dataTemplateFactoryPanel;
            changesetListBox.ItemTemplate = dataTemplate;

            item.ScatterManipulationDelta += SviManipulationDelta;


            item.Content = config.GetContectOfScatterViewItem(neutralColor, changesetListBox);

            return item;
        }

    }
}
