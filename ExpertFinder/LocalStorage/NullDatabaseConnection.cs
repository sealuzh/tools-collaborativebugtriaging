﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;
using ScrumSupporter.Planning;

namespace ScrumSupporter.LocalStorage
{
    public class NullDatabaseConnection: DBConnection
    {
        public override bool IsOpenSuccessful()
        {
            return false;
        }

        public override void LoadWorkItemModelsIntoDB(IEnumerable<Planning.WorkItemViewModel> models)
        {
            
        }

        public override void Dispose()
        {
        }

        public override ICollection<WorkItemModel> QueryBacklogItems()
        {
            ICollection<WorkItemModel> emptyList = new List<WorkItemModel>();
            return emptyList;
        }

        public override ICollection<WorkItemsOfIterationContainerViewModel> QueryItemsOfIterations()
        {
            ICollection<WorkItemsOfIterationContainerViewModel> items = new List<WorkItemsOfIterationContainerViewModel>();
            return items;
        }
    }
}
