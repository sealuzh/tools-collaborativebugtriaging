﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Planning;
using ScrumSupporter.Model;
using ScrumSupporter.Util;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScrumSupporter.Planning.WorkItemViews
{
    public class IssueViewModel: WorkItemViewModel
    {
        IssueModel issueModel;

        public IssueViewModel(WorkItemModel model)
            :base(model)
        {
            issueModel = model as IssueModel;
        }


        public int Priority
        {
            get
            {
                return issueModel.Priority;
            }
            set
            {
                issueModel.Priority = value;
                NotifyOfPropertyChange(() => Priority);
            }
        }

        public String Description
        {
            get
            {
                return issueModel.Description;
            }
            set
            {
                issueModel.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public DateTime DueDate
        {
            get
            {
                return issueModel.DueDate;
            }
            set
            {
                issueModel.DueDate = value;
                NotifyOfPropertyChange(() => DueDate);
            }
        }


        protected override Point CalculatePosition(Random random)
        {
            Point position = new Point();
            if (random != null)
            {
                position.X = random.Next(100, (Constants.BACKLOGWIDTH / 2) - 100);
                position.Y = random.Next((Constants.BACKLOGHEIGHT / 2 + 100), (Constants.BACKLOGHEIGHT - 100));
                return position;
            }
            position.X = 100;
            position.Y = (Constants.BACKLOGHEIGHT / 2 + 100);
            return position;
        }

        /// <summary>
        /// Looks up the DataTemplate in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplate()
        {
            return Application.Current.Resources["IssueViewModelTemplate"] as DataTemplate;
        }

        /// <summary>
        /// Looks up the DataTemplate for the singleIteration view in the Resource Dictionary. 
        /// </summary>
        /// <returns></returns>
        public override DataTemplate GetTemplateForSingleIterationView()
        {
            return Application.Current.Resources["IssueViewModelTemplateForSingleIterationView"] as DataTemplate;
        }

        /// <summary>
        /// Creates an IssueView in a ScatterViewItem and sets itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public override ScatterViewItem GetView()
        {
            ScatterViewItem svi = GetScatterViewItemConfigsOfTheView();
            IssueView view = new IssueView();
            //view.DisableAnalyzeButton();
            view.DataContext = this;
            svi.Content = view;

            return svi;
        }

        protected override Style GetStyleOfCursorVisual()
        {
            return Application.Current.Resources["CursorStyleIssueBacklogView"] as Style;
        }

        protected override SurfaceUserControl GetSpecificEditingView()
        {
            return new IssueEditingView();
        }
    }
}
