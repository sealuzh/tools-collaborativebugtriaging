﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using ScrumSupporter.Exceptions;
using System.Collections.Generic;

namespace ScrumSupporter
{
    /// <summary>
    /// Calculates the cosine similarity of two vectors by dividing the inner vector product by 
    /// the product of the euclidean norm of the vectors.
    /// </summary>
    public class CosineSimilarityCalculator
    {
        /// <summary>
        /// Calculates the cosine similarity of two vectors.
        /// </summary>
        /// <param name="vector1"></param>
        /// <param name="vector2"></param>
        /// <returns></returns>
        public double CalculateCosineSimilarity(ICollection<double> vector1, ICollection<double> vector2)
        {
            if (vector1 != null)
            {
                if (vector2 != null)
                {
                    if (vector1.Count != vector2.Count)
                    {
                        throw new InnerVectorProductException("vectors do not have the same length.");
                    }

                    double cosineSimilarity = CalculateInnerVectorProduct(vector1, vector2) / CalculateEuclideanProduct(vector1, vector2);
                    return cosineSimilarity;
                }
                else throw new ArgumentNullException("vector2", "is null");
            }
            else throw new ArgumentNullException("vector1","is null");
        }


        /// <summary>
        /// Calculates and sets the euclidean product.
        /// </summary>
        /// <returns></returns>
        private double CalculateEuclideanProduct(ICollection<double> vector1, ICollection<double> vector2)
        {
            double sumUnderRootOfVector1 = 0;
            foreach (double entry in vector1)
            {
                sumUnderRootOfVector1 += (entry * entry);
            }

            double euclideanProductVector1 = Math.Sqrt(sumUnderRootOfVector1);

            double sumUnderRootOfVector2 = 0;
            foreach (double entry in vector2)
            {
                sumUnderRootOfVector2 += (entry * entry);
            }
            double euclideanProductVector2 = Math.Sqrt(sumUnderRootOfVector2);

            return euclideanProductVector1 * euclideanProductVector2;
        }



        /// <summary>
        /// Calculates the inner vector product of the two vectors.
        /// </summary>
        /// <returns></returns>
        private double CalculateInnerVectorProduct(ICollection<double> vector1, ICollection<double> vector2)
        {
            double innerVectorProduct = 0.0;

            for (int i = 0; i < vector1.Count; i++)
            {
                double entry1 = vector1.ElementAt(i);
                double entry2 = vector2.ElementAt(i);

                double product = entry1 * entry2;
                innerVectorProduct += product; 
            }
            return innerVectorProduct;
        }
    }
}
