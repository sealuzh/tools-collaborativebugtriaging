using System;
using System.Windows;
using System.Windows.Media;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Manipulations;
using System.Windows.Media.Animation;
using ScrumSupporter.LoadProject;
using System.Windows.Input;
using ScrumSupporter.Util;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Triaging;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms.Integration;
using System.Net.Mail;
using System.Text;

namespace ScrumSupporter
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// This is the logic of the start window of the ScrumSupporter. 
    /// </summary>
    public partial class StartWindow : SurfaceWindow
    {

        private Affine2DManipulationProcessor manipulationProcessor = null;
        private RotateTransform rt = new RotateTransform();
        private ScatterViewItem sviLoadProjectView;
        private ScatterViewItem sviAnalyzeWorkItemDialogView;

        private Affine2DManipulationProcessor manipulationProcessorAnalyzeDialogView;
        private Affine2DManipulationProcessor manipulationProcessorLoadProjectView;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public StartWindow()
        {
            StartWindowViewModel model = new StartWindowViewModel();
            this.DataContext = model;

            InitializeComponent();

            // Add handlers for Application activation events
            AddActivationHandlers();

            //To initialize the roation processor and the inertia processor.
            InitializeManipulationIntertiaProcessor();

            profilesSlice.SetProfileContainer(ProfilesContainer);
            workItemManagementSlice.SetWorkItemMgmtContainer(WorkItemMgmtContainer);


           
        }


        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Remove handlers for Application activation events
            RemoveActivationHandlers();
        }

        /// <summary>
        /// Adds handlers for Application activation events.
        /// </summary>
        private void AddActivationHandlers()
        {
            // Subscribe to surface application activation events
            ApplicationLauncher.ApplicationActivated += OnApplicationActivated;
            ApplicationLauncher.ApplicationPreviewed += OnApplicationPreviewed;
            ApplicationLauncher.ApplicationDeactivated += OnApplicationDeactivated;
        }

        /// <summary>
        /// Removes handlers for Application activation events.
        /// </summary>
        private void RemoveActivationHandlers()
        {
            // Unsubscribe from surface application activation events
            ApplicationLauncher.ApplicationActivated -= OnApplicationActivated;
            ApplicationLauncher.ApplicationPreviewed -= OnApplicationPreviewed;
            ApplicationLauncher.ApplicationDeactivated -= OnApplicationDeactivated;
        }

        /// <summary>
        /// This is called when application has been activated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationActivated(object sender, EventArgs e)
        {
            //TODO: enable audio, animations here
        }

        /// <summary>
        /// This is called when application is in preview mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationPreviewed(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        ///  This is called when application has been deactivated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnApplicationDeactivated(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }

        /// <summary>
        /// EventHandler to enable the rotation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void canvas_ContactChanged(object sender, ContactEventArgs e)
        {
            manipulationProcessor.BeginTrack(e.Contact);
            // e.Handled = true;
        }

        /// <summary>
        /// rotation processors are initialized.
        /// </summary>
        private void InitializeManipulationIntertiaProcessor()
        {
            manipulationProcessor = new Affine2DManipulationProcessor(Affine2DManipulations.Rotate, canvas);
            manipulationProcessor.Affine2DManipulationDelta += new EventHandler<Affine2DOperationDeltaEventArgs>(manipulations_Delta);
            manipulationProcessor.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(manipulations_Completed);
            canvas.RenderTransformOrigin = new Point(0.5, 0.5);

            manipulationProcessor.PivotPoint = new Point(0.5, 0.5);
            manipulationProcessor.PivotRadius = 320;

            //inertiaProcessor = new Affine2DInertiaProcessor();
            //inertiaProcessor.Affine2DInertiaDelta += manipulations_Delta;

        }

        /// <summary>
        /// Updates the Angle of the RotationTransformation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void manipulations_Delta(object sender, Affine2DOperationDeltaEventArgs e)
        {
            TransformGroup tg = new TransformGroup();
            //RotateTransform rt = new RotateTransform(rotation + e.CumulativeRotation);

            rt.Angle += e.RotationDelta;
            tg.Children.Add(rt);
            canvas.RenderTransform = tg;

        }



        /// <summary>
        /// Calculates the rotation and the inertia. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void manipulations_Completed(object sender, Affine2DOperationCompletedEventArgs e)
        {
            ManipulationsUtil maniUtil = new ManipulationsUtil();
            Affine2DInertiaProcessor inertiaProcessor = maniUtil.GetInertiaProcessor(new Thickness(0, 0, this.ActualWidth, this.ActualHeight), e);

            inertiaProcessor.Affine2DInertiaDelta += manipulations_Delta;


            try
            {
                inertiaProcessor.Begin();
            }
            catch (InvalidOperationException exception)
            {
                System.Console.WriteLine("Inertia Processor crashed: " + exception.Message);
            }


        }


        private void OnClose(object sender, RoutedEventArgs e)
        {
            DialogContainer.Items.RemoveAt(0);
        }

        /// <summary>
        /// Event Handler, which initiates the Storyboard, which has a Completed Event Handler attached. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadProjectContactHoldGesture(object sender, ContactEventArgs e)
        {
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sbOfLoadProjectViewCompleted);
            Animation animation = new Animation();
            sb = animation.GetTextAnimation(loadProjectSlice.loadProjectText, sb) as Storyboard;

            loadProjectSlice.BeginStoryboard(sb);
        }

        /// <summary>
        /// Sets several properties of the ScatterViewItem, which contains the LoadProjectView. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void sbOfLoadProjectViewCompleted(object sender, EventArgs e)
        {
            LoadProjectViewModel loadProjectViewModel = new LoadProjectViewModel(this.DataContext);
            LoadProjectView loadProjectView = loadProjectViewModel.CreateView();

            sviLoadProjectView = new ScatterViewItem();
            sviLoadProjectView.Name = "sviLoadProjectView";
            sviLoadProjectView.Content = loadProjectView;
            sviLoadProjectView.Height = 400;
            sviLoadProjectView.Width = 800;
            sviLoadProjectView.Orientation = 0;
            sviLoadProjectView.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

            manipulationProcessorLoadProjectView = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviLoadProjectView);
            manipulationProcessorLoadProjectView.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviLoadProjectViewManipulationCompleted);
            sviLoadProjectView.PreviewContactDown += SviLoadProjectViewPreviewContactDown;

            sviLoadProjectView.ShowsActivationEffects = false;

            try
            {
                sviLoadProjectView.Style = (Style)FindResource("sviStyle");
            }
            catch (ResourceReferenceKeyNotFoundException resourceReferenceNotFoundExceptionn)
            {
                System.Console.WriteLine(resourceReferenceNotFoundExceptionn.Message);
                //Leave the default style
            }
            catch (ArgumentNullException argumentNullException)
            {
                System.Console.WriteLine(argumentNullException.Message);
                //Leave the default style
            }

            if (DialogContainer.HasItems)
            {
                DialogContainer.Items.Clear();
            }

            DialogContainer.Items.Add(sviLoadProjectView);
        }


        /// <summary>
        /// Evaluates if the LoadProject view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviLoadProjectViewManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor = 
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, this.ActualWidth, this.ActualHeight), e);

                inertiaProcessor.Begin();
                inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviLoadProjectViewInertiaCompleted);
            }
        }

        /// <summary>
        /// Removes the LoadProject view from its container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviLoadProjectViewInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            Storyboard fadeoutSb = new Storyboard();
            fadeoutSb = (Storyboard)FindResource("fadeout");
            sviLoadProjectView.BeginStoryboard(fadeoutSb);
        }

        /// <summary>
        /// Starts the manipulation processor of LoadProject view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviLoadProjectViewPreviewContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorLoadProjectView.BeginTrack(e.Contact);
        }


        /// <summary>
        /// Event Handler of the "Analyze work item slice", which begins the storyboards.
        /// The storyboard has an event handler added to its Completed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AnalyzeWorkItemSlice_ContactHoldGesture(object sender, ContactEventArgs e)
        {
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sbOfWorkItemToAnalyzeCompleted);
            Animation animation = new Animation();
            sb = animation.GetTextAnimation(analyzeWorkItemSlice.analyzeWorkItemText, sb) as Storyboard;

            analyzeWorkItemSlice.BeginStoryboard(sb);
        }

        /// <summary>
        /// Sets some visual properties of the ScatterViewItem, which contains the AnalyzeWorkItemDialog. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void sbOfWorkItemToAnalyzeCompleted(object sender, EventArgs e)
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;

            SelectWorkItemToAnalyzeViewModel analyzeWorkItemDialogViewModel = new SelectWorkItemToAnalyzeViewModel(platform);
            SelectWorkItemToAnalyzeView analyzeWorkItemDialog = analyzeWorkItemDialogViewModel.CreateView();
            analyzeWorkItemDialog.SetTriagingContainer(TriagingContainer);

            analyzeWorkItemDialog.SetContainerForDetailWorkItemView(DetailWorkItemDialogContainer);
            analyzeWorkItemDialog.SetSelectWorkItemToAnalyzeContainer(WorkItemToAnalyzeDialogContainer);

            sviAnalyzeWorkItemDialogView = new ScatterViewItem();
            sviAnalyzeWorkItemDialogView.Content = analyzeWorkItemDialog;
            sviAnalyzeWorkItemDialogView.Height = 400;
            sviAnalyzeWorkItemDialogView.Width = 800;
            sviAnalyzeWorkItemDialogView.Orientation = 0;
            sviAnalyzeWorkItemDialogView.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

            sviAnalyzeWorkItemDialogView.ShowsActivationEffects = false;
            sviAnalyzeWorkItemDialogView.Background = new SolidColorBrush(Colors.Transparent);

            manipulationProcessorAnalyzeDialogView = new Affine2DManipulationProcessor(Affine2DManipulations.TranslateX | Affine2DManipulations.TranslateY, sviAnalyzeWorkItemDialogView);
            manipulationProcessorAnalyzeDialogView.Affine2DManipulationCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviAnalyzeWorkItemDialogManipulationCompleted);
            sviAnalyzeWorkItemDialogView.PreviewContactDown += SviAnalyzeWorkItemDialogViewPreviewContactDown;


            try
            {
                sviLoadProjectView.Style = (Style)FindResource("sviStyle");
            }
            catch (ResourceReferenceKeyNotFoundException resourceReferenceNotFoundExceptionn)
            {
                System.Console.WriteLine(resourceReferenceNotFoundExceptionn.Message);
                //Leave the default style
            }
            catch (ArgumentNullException argumentNullException)
            {
                System.Console.WriteLine(argumentNullException.Message);
                //Leave the default style
            }

            if (WorkItemToAnalyzeDialogContainer.HasItems)
            {
                WorkItemToAnalyzeDialogContainer.Items.Clear();
            }

            WorkItemToAnalyzeDialogContainer.Items.Add(sviAnalyzeWorkItemDialogView);

        }

        /// <summary>
        /// Evaluates if the AnalyzeWorkItem view is thrown out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviAnalyzeWorkItemDialogManipulationCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            if (e.Velocity.X > 2.8 || e.Velocity.X < -2.8 || e.Velocity.Y > 2.8 || e.Velocity.Y < -2.8)
            {
                ManipulationsUtil maniUtil = new ManipulationsUtil();
                Affine2DInertiaProcessor inertiaProcessor = 
                    maniUtil.GetInertiaProcessor(new Thickness(0, 0, this.ActualWidth, this.ActualHeight), e);
                    

                inertiaProcessor.Begin();
                inertiaProcessor.Affine2DInertiaCompleted += new EventHandler<Affine2DOperationCompletedEventArgs>(SviAnalyzeWorkItemViewInertiaCompleted);
            }
        }

        /// <summary>
        /// Removes the AnalyzeWorkItem view from its container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviAnalyzeWorkItemViewInertiaCompleted(object sender, Affine2DOperationCompletedEventArgs e)
        {
            WorkItemToAnalyzeDialogContainer.Items.Clear();
        }

        /// <summary>
        /// Starts the manipulation processor of AnalyzeWorkItem view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SviAnalyzeWorkItemDialogViewPreviewContactDown(object sender, ContactEventArgs e)
        {
            manipulationProcessorAnalyzeDialogView.BeginTrack(e.Contact);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            System.Console.WriteLine("Try to send email");


            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.gmx.com");
            mail.From = new MailAddress("seal.seal@gmx.ch");
            mail.To.Add("katja.kevic@uzh.ch");
            mail.Subject = "Test Mail - 1";
            mail.Body = "mail with attachment";

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("seal.seal@gmx.ch", "sealseal");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);


            System.Console.WriteLine("Email sent");
        }


    }


}