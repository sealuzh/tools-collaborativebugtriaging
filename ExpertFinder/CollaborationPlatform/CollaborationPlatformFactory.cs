﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Creates an instance of a CollaborationPlatform.
    /// </summary>
    public class CollaborationPlatformFactory
    {
        private static ICollaborationPlatform collaborationPlatform;

        public ICollaborationPlatform CollaborationPlatform
        {
            get {

                if (collaborationPlatform == null)
                {
                    throw new InvalidOperationException("CollaborationPlatform Type not recognized");
                }
                else
                {
                    return collaborationPlatform; 
                }
            }
            set { collaborationPlatform = value; }
        }

        /// <summary>
        /// Constructor used to set the type of the CollaborationPlatform.
        /// </summary>
        /// <param name="type"></param>
        public CollaborationPlatformFactory(Type type)
        {
            if(type == Type.TFS)
            {
                collaborationPlatform = new TFSCollaborationPlatform();
            }

        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public CollaborationPlatformFactory()
        {
        }
    }
}
