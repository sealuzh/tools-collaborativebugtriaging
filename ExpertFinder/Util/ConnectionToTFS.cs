﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TFS;
using ExpertFinder.CollaborationPlatform;

namespace ExpertFinder.Util
{
    /// <summary>
    /// Singleton to handle the connection to the TFS.
    /// </summary>
    public class ConnectionToTFS
    {
        private static ConnectionToTFS instance;

        private Credentials cred; 

        private ITfs myTfs;

        public ITfs MyTfs
        {
            get { return myTfs; }
            set { myTfs = value; }
        }
        

        public static ConnectionToTFS getInstance()
        {
            if (instance == null)
            {
                instance = new ConnectionToTFS();
            }
            return instance;
        }

        public void register(String username, String password, String domain, String uri, String projectName)
        {
            cred = new Credentials(username, password, domain, uri, projectName);
            myTfs = new Tfs(cred.Username, cred.Password, cred.Domain, new Uri(cred.Uri), cred.ProjectName);
        }

        /// <summary>
        /// Checks if the credentials are already set.
        /// </summary>
        /// <returns>true, if the credentials are set.</returns>
        public Boolean IsRegistered()
        {
            if (cred == null)
            {
                return false;
            }
            return true;
        }
    }
}
