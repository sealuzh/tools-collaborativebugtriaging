﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Manipulations;
using ExpertFinder.Model;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation;
using TFS;
using System.IO;
using ExpertFinder.Util;
using System.Threading;
using System.Globalization;

namespace ExpertFinder.Planning
{
    /// <summary>
    /// Interaktionslogik für PlanningWindow.xaml
    /// </summary>
    /// <remarks>
    /// author: Katja Kevic
    /// date (yyyy mm dd): 2012 08 13
    /// </remarks>
    public partial class PlanningWindowView : SurfaceWindow
    {
        public PlanningWindowView()
        {
            InitializeComponent();
            //ContactDown += new ContactEventHandler(PlanningWindowView_ContactDown);

            //ContactUp += new ContactEventHandler(PlanningWindowView_ContactUp);
        }


        void PlanningWindowView_ContactUp(object sender, ContactEventArgs e)
        {
            //ItemCanvas.Children.Clear();

        }



        /// <summary>
        /// Determine the type of contact and display it in a label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PlanningWindowView_ContactDown(object sender, ContactEventArgs e)
        {
            Point p = e.GetPosition(this);

            Contact c = e.Contact;
            //by default it is a blob
            string type = "Blob";
            //try to find the type of item on the Surface 
            if (c.Tag.Type == TagType.Byte)
            {
                type = "Byte Tag";
                type += (" Value: " + c.Tag.Byte.Value.ToString("X", CultureInfo.InvariantCulture));
            
            }
            else if (c.Tag.Type == TagType.Identity)
            {
                type = "Identity Tag";
                type += (" Value: " + c.Tag.Identity.Value.ToString("X", CultureInfo.InvariantCulture));
            }
            else if (c.IsFingerRecognized)
            {
                type = "Finger";
            }
            //display the type of item in a label
            InfoLabel.Content = type;

          //  PopulateItemCircle();
        }


        //private void PopulateItemCircle()
        //{
        //    // only add the items to the circle once.
        //    // if there are items in the canvas 
        //    //  then we have already added them and do not need to add them again
        //        string itemsFolder = Directory.GetCurrentDirectory();
        //        itemsFolder += @"\Items\";
        //        string[] photosFiles =
        //            Directory.GetFiles(itemsFolder, "*.png");

        //        double segment = 360.0 / photosFiles.Length;
        //        double rotation = 0.0;

        //        foreach (string pic in photosFiles)
        //        {
        //            AddImageToCircle(pic, rotation);
        //            rotation += segment;
        //        }
            
        //}

        //private void AddImageToCircle(string pic, double rotation)
        //{
        //    SurfaceButton sb = new SurfaceButton();
        //    // apply the xaml style to the surface content control
        //    //sb.Style = (Style)FindResource("SB_Style");

        //    // load the file into an Image object to use as the content 
        //    Image img = LoadImageFromPath(pic);
        //    img.Width = 200;
        //    img.Height = 150;

        //    //add the control to the circle
        //    sb.Margin = new Thickness(100, 0, 0, 0);

        //    //Rotate the Surface Button so that the highlight on the Glass Button is coming from that same place on all of them.
        //    double circleRadius = ItemCanvas.Height / 2;
        //    double radians = rotation * Math.PI / 180.0;
        //    double x = circleRadius * Math.Cos(radians);
        //    double y = circleRadius * Math.Sin(radians) + ItemCanvas.Height / 2;
        //    TranslateTransform tt = new TranslateTransform(x, y);
        //    sb.RenderTransformOrigin = new Point(0.5, 0.5);
        //    sb.RenderTransform = tt;

        //    sb.Content = img;

        //    sb.Command = (this.DataContext as PlanningWindowViewModel).OpenUserStories;

        //    // add an event handler for a click on this image
        //    //sb.Click += new RoutedEventHandler(sb_Click);
        //    //place the scc at the correct location on the circle
        //    ItemCanvas.Children.Add(sb);
        //}

        void sb_Click(object sender, RoutedEventArgs e)
        {

        }

        private static Image LoadImageFromPath(string path)
        {
            ImageSourceConverter converter = new ImageSourceConverter();
            Image image = new Image();
            image.Source = (ImageSource)converter.ConvertFromString(path);
            return image;
        }





    }


}

