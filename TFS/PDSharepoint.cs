﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Services.Client;
using System.Collections.ObjectModel;
using System.Net;
using TfsLink.ProjectDashboard;

namespace TfsLink
{
    /// <summary>
    /// Looks up all calendar items on teh Project Dashboard.
    /// </summary>
    public class PDSharepoint
    {
        private TfsLink.ProjectDashboard.MP1201DataContext dc;



        public PDSharepoint(String username, String password)
        {
            dc = new TfsLink.ProjectDashboard.MP1201DataContext(new Uri("http://arktos/sites/DefaultCollection/MP1201/_vti_bin/ListData.svc/"));
            dc.Credentials = new System.Net.NetworkCredential(username, password);
        }

        /// <summary>
        /// Looks up all calendar entries on the Project Dashboard.
        /// </summary>
        /// <returns></returns>
        public Collection<CalendarItem> GetCalenderItems()
        {
            Collection<CalendarItem> calendarItems = new Collection<CalendarItem>();

            try
            {
                DataServiceQuery<CalendarItem> items = dc.Calendar;
                IQueryable<CalendarItem> filteredItems = from item in items where item.Title != String.Empty select item;

                foreach (CalendarItem i in filteredItems)
                {
                    calendarItems.Add(i);
                }
            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
            return calendarItems;
        }
    }
}
