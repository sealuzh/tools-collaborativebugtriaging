﻿#pragma checksum "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EC624C725A4C7F908F0EADA1E9DD3B85"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.296
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Controls.ContactVisualizations;
using Microsoft.Surface.Presentation.Controls.Primitives;
using ScrumSupporter.Dialogs;
using ScrumSupporter.LoadProject;
using ScrumSupporter.Planning;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ScrumSupporter.Triaging {
    
    
    /// <summary>
    /// SelectWorkItemToAnalyzeView
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class SelectWorkItemToAnalyzeView : Microsoft.Surface.Presentation.Controls.SurfaceUserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.SurfaceButton toAnalyzeButton;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.SurfaceTextBox idTextBox;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label IdLabel;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.SurfaceTextBox titleContainsTextBox;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.ScatterView PossibleWorkItemsContainer;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.ScatterViewItem sviPossibleWorkItems;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.SurfaceButton ClosePossibleWIButton;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.SurfaceListBox workItemsToSelectListbox;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ScrumSupporter.Dialogs.Dialog_InvalidWorkItemID DialogInvalidWorkItemID;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ScrumSupporter.Dialogs.Dialog_CollaborationPlatformUnavailable DialogCollaborationPlatformUnavailable;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ScrumSupporter.Dialogs.Dialog_NoMatchingWorkItemFound DialogNoMatchingWorkItemFound;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ScrumSupporter;component/triaging/selectworkitemtoanalyzeview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.toAnalyzeButton = ((Microsoft.Surface.Presentation.Controls.SurfaceButton)(target));
            
            #line 23 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            this.toAnalyzeButton.Click += new System.Windows.RoutedEventHandler(this.toAnalyzeButton_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.idTextBox = ((Microsoft.Surface.Presentation.Controls.SurfaceTextBox)(target));
            
            #line 24 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            this.idTextBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.idTextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.IdLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.titleContainsTextBox = ((Microsoft.Surface.Presentation.Controls.SurfaceTextBox)(target));
            
            #line 27 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            this.titleContainsTextBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.titleContainsTextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.PossibleWorkItemsContainer = ((Microsoft.Surface.Presentation.Controls.ScatterView)(target));
            return;
            case 7:
            this.sviPossibleWorkItems = ((Microsoft.Surface.Presentation.Controls.ScatterViewItem)(target));
            return;
            case 8:
            this.ClosePossibleWIButton = ((Microsoft.Surface.Presentation.Controls.SurfaceButton)(target));
            
            #line 39 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            this.ClosePossibleWIButton.Click += new System.Windows.RoutedEventHandler(this.ClosePossibleWIButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.workItemsToSelectListbox = ((Microsoft.Surface.Presentation.Controls.SurfaceListBox)(target));
            
            #line 43 "..\..\..\Triaging\SelectWorkItemToAnalyzeView.xaml"
            this.workItemsToSelectListbox.PreviewContactDown += new Microsoft.Surface.Presentation.ContactEventHandler(this.WorkItemsToSelectListbox_PreviewContactDown);
            
            #line default
            #line hidden
            return;
            case 10:
            this.DialogInvalidWorkItemID = ((ScrumSupporter.Dialogs.Dialog_InvalidWorkItemID)(target));
            return;
            case 11:
            this.DialogCollaborationPlatformUnavailable = ((ScrumSupporter.Dialogs.Dialog_CollaborationPlatformUnavailable)(target));
            return;
            case 12:
            this.DialogNoMatchingWorkItemFound = ((ScrumSupporter.Dialogs.Dialog_NoMatchingWorkItemFound)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

