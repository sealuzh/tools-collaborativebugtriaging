﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using ScrumSupporter.Util;

namespace ScrumSupporter.Profile
{
    /// <summary>
    /// Provides configurations for the container, which can hold a list of items.
    /// </summary>
    public class DialogContainerConfigurer
    {

        /// <summary>
        /// Sets the common properties of the ScatterViewItem which holds the dialog.
        /// </summary>
        /// <returns></returns>
        public ScatterViewItem GetScatterViewItemContainer()
        {
            ScatterViewItem item = new ScatterViewItem();
            item.Width = 250; item.Height = 400;
            item.Orientation = 0;
            item.Background = new SolidColorBrush(Colors.Transparent);
            item.ShowsActivationEffects = false;
            item.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

            return item;
        }

        /// <summary>
        /// Sets the common properties of the Grid containing the SurfaceListBox and adds it to a ViewBox.
        /// </summary>
        /// <param name="neutralColor"></param>
        /// <param name="listbox"></param>
        /// <returns></returns>
        public Viewbox GetContectOfScatterViewItem(Color neutralColor, UIElement listbox)
        {
            Grid grid = new Grid();
            grid.Width = 250;
            grid.Height = 400;
            grid.Background = new SolidColorBrush(neutralColor);

            grid.Children.Add(listbox);

            Viewbox viewbox = new Viewbox();
            viewbox.Stretch = Stretch.UniformToFill;
            viewbox.Child = grid;

            return viewbox;
        }

        /// <summary>
        /// Sets the common properties of the SurfaceListBox.
        /// </summary>
        /// <param name="neutralColor"></param>
        /// <returns></returns>
        public SurfaceListBox GetListBoxConfigs(Color neutralColor)
        {
            SurfaceListBox listBox = new SurfaceListBox();
           
            listBox.IsSynchronizedWithCurrentItem = true;
            //listBox.Margin = new Thickness(46, 0, 0, 0);
            listBox.Height = 400;
            listBox.Background = new SolidColorBrush(neutralColor);

            FrameworkElementFactory factoryPanel = new FrameworkElementFactory(typeof(StackPanel));
            factoryPanel.SetValue(StackPanel.WidthProperty, 150.0);
            ItemsPanelTemplate template = new ItemsPanelTemplate();
            template.VisualTree = factoryPanel;

            listBox.ItemsPanel = template;

            return listBox;
        }

        public SurfaceListBox GetListBoxConfigsForChangesets(Color neutralColor)
        {
            SurfaceListBox listBox = new SurfaceListBox();

            listBox.IsSynchronizedWithCurrentItem = true;
            listBox.Margin = new Thickness(10, 0, 0, 0);
            listBox.Height = 400;
            listBox.Background = new SolidColorBrush(neutralColor);
            listBox.SetValue(ScrollViewer.VerticalScrollBarVisibilityProperty, ScrollBarVisibility.Disabled);

            FrameworkElementFactory factoryPanel = new FrameworkElementFactory(typeof(StackPanel));
            factoryPanel.SetValue(StackPanel.WidthProperty, 220.0);
            ItemsPanelTemplate template = new ItemsPanelTemplate();
            template.VisualTree = factoryPanel;

            listBox.ItemsPanel = template;

            return listBox;
        }

    }
}
