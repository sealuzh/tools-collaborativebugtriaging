﻿using System.Windows;
using Microsoft.Surface.Presentation.Controls;

namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Includes event handler to switch between the mini view and the editing view. 
    /// </summary>
    public partial class TaskView : SurfaceUserControl
    {
        public TaskView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// According to the size of the user control, the mini view or the editing view of the work item is displayed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private new void SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (this.DataContext as WorkItemViewModel).OnSizeOfViewChanged(e, editView, miniView);
        }

        public void DisableAnalyzeButton()
        {
            editView.AnalyzeButton.Visibility = Visibility.Hidden;
        }
    }
}
