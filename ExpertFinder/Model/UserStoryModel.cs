﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Planning;
using ScrumSupporter.Planning.WorkItemViews;
using Microsoft.TeamFoundation.WorkItemTracking.Client;

namespace ScrumSupporter.Model
{
    public class UserStoryModel: WorkItemModel
    {
        private String description;

        public String Description
        {
            get { return description; }
            set { description = value; }
        }


        private double storyPoints;

        public double StoryPoints
        {
            get { return storyPoints; }
            set { storyPoints = value; }
        }

        private String risk;

        public String Risk
        {
            get { return risk; }
            set { risk = value; }
        }

        public override WorkItemViewModel GetViewModelType()
        {
            return new UserStoryViewModel(this);
        }

        /// <summary>
        /// Return the free text of the work item.
        /// </summary>
        /// <returns></returns>
        public override string GetText()
        {
            return this.Title + " " + this.Description;
        }


        public override WorkItem AdaptNewWorkItemToTfs(Project teamProject)
        {
            if (teamProject != null)
            {
                WorkItemType workItemType = teamProject.WorkItemTypes["User Story"];
                WorkItem workItem = new WorkItem(workItemType);

                return SetUserStoryDetails(workItem);
            }
            else throw new ArgumentNullException("teamProject", "is null");
        }

        public override WorkItem AdaptExistingWorkItemToTfs(WorkItem workItem)
        {
            if (workItem != null)
            {
                return SetUserStoryDetails(workItem);
            }
            else throw new ArgumentNullException("workItem", "is null");
        }

        private WorkItem SetUserStoryDetails(WorkItem workItem)
        {
            workItem = SetCommonFields(workItem);
            workItem.Description = this.Description;

            AllowedValuesCollection allowedStoryPoints = workItem.Fields["Story Points"].AllowedValues;
            if (allowedStoryPoints.Contains(this.StoryPoints.ToString()))
            {
                workItem.Fields["Story Points"].Value = this.StoryPoints;
            }
            AllowedValuesCollection allowedRisks = workItem.Fields["Risk"].AllowedValues;
            if (allowedRisks.Contains(this.Risk))
            {
                workItem.Fields["Risks"].Value = this.Risk;
            }
            return workItem;
        }

    }


}
