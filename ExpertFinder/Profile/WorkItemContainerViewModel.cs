﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZH.Infrastructure;
using System.Collections.ObjectModel;

namespace ExpertFinder.Profile
{
    public class WorkItemContainerViewModel: BaseViewModel
    {

        private ChangedWorkItemContainerView view;
        private AssignedWorkItemsContainerView assignedWorkItemsView;
        public ObservableCollection<WorkItemViewModel> ChangedWorkitemList { get; set; }

        public WorkItemContainerViewModel(IList<WorkItemViewModel> ChangendWorkitemViewModels)
        {
            ChangedWorkitemList = new ObservableCollection<WorkItemViewModel>();

            foreach (var item in ChangendWorkitemViewModels)
            {
                ChangedWorkitemList.Add((WorkItemViewModel)item);
            }
        }

        public ChangedWorkItemContainerView CreateView()
        {
            var v = view ?? (view = new ChangedWorkItemContainerView());
            v.DataContext = this;

            return v;
        }

        public AssignedWorkItemsContainerView CreateAssignedWorkItemView()
        {
            var v = assignedWorkItemsView ?? (assignedWorkItemsView = new AssignedWorkItemsContainerView());
            v.DataContext = this;

            return v;
        }

        

    }
}
