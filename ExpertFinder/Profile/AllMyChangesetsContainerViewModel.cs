﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using UZH.Infrastructure;

namespace ExpertFinder.Profile
{
    public class AllMyChangesetsContainerViewModel: BaseViewModel
    {

        private AllMyChangesetsContainerView view;
        public ObservableCollection<ChangesetViewModel> ChangesetList { get; set; }

        public AllMyChangesetsContainerViewModel(IList<ChangesetViewModel> ChangesetViewModels)
        {
            ChangesetList = new ObservableCollection<ChangesetViewModel>();

            foreach (var item in ChangesetViewModels)
            {
                ChangesetList.Add((ChangesetViewModel)item);
            }
        }


        public AllMyChangesetsContainerView CreateView()
        {
            var v = view ?? (view = new AllMyChangesetsContainerView());
            v.DataContext = this;

            return v;
        }
    }
}
