﻿using System;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media.Animation;
using System.Windows;
using Microsoft.Surface.Presentation;
using ScrumSupporter.Util;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.Planning;
using ScrumSupporter.CollaborationPlatform.Exceptions;

namespace ScrumSupporter
{
    /// <summary>
    /// Interaktionslogik für WorkItemManagementSlice.xaml
    /// </summary>
    public partial class WorkItemManagementSlice : SurfaceUserControl
    {
        private ScatterView workItemMgmtContainer;

        public WorkItemManagementSlice()
        {
            InitializeComponent();
        }

        public void SetWorkItemMgmtContainer(ScatterView container)
        {
            workItemMgmtContainer = container;
        }

        /// <summary>
        /// Starts the storyboard to indicate to the user that the gesture was recognized. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slice_ContactHoldGesture(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            Storyboard sb = new Storyboard();
            sb.Completed += new EventHandler(sbCompleted);
            Animation animation = new Animation();
            sb = animation.GetTextAnimation(workItemManagementText, sb) as Storyboard;

            workItemManagementSlice.BeginStoryboard(sb);
        }

        /// <summary>
        /// Loads the data of the work item management view and opens it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sbCompleted(object sender, EventArgs e)
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;

            var workItemManagementModel = new WorkItemContainerViewModel();
            var view = workItemManagementModel.CreateView();
            view.backButton.Click += BackButtonOnClick;

            ScatterViewItem sviWIMgmtView = GetWiMgmtScatterViewItem();
            sviWIMgmtView.Content = view;

            if (platform.IsCollaborationPlatformAvailable())
            {
                try
                {
                    workItemManagementModel.LoadData(platform);
                }
                catch (CollaborationPlatformUnavailableException e1)
                {
                    System.Console.WriteLine(e1.Message);
                }
                catch (ArgumentNullException e1)
                {
                    System.Console.WriteLine(e1.Message);
                }
            }
            else
            {
                //load data from local Database
                workItemManagementModel.LoadLocalData();
            }
            workItemMgmtContainer.Items.Clear();
            workItemMgmtContainer.Items.Add(sviWIMgmtView);
        }


        private ScatterViewItem GetWiMgmtScatterViewItem()
        {
            ScatterViewItem sviWIMgmtView = new ScatterViewItem();
            sviWIMgmtView.CanScale = false;
            sviWIMgmtView.Orientation = 0;
            sviWIMgmtView.Center = new Point(Constants.TBLWIDTH / 2.0, Constants.TBLHEIGHT / 2.0);

            sviWIMgmtView.Height = 768;
            sviWIMgmtView.Width = 1024;
            sviWIMgmtView.Style = FindResource("sviStyle") as Style;
            sviWIMgmtView.SingleInputRotationMode = SingleInputRotationMode.Disabled;
            sviWIMgmtView.CanMove = false;
            return sviWIMgmtView;
        }

        private void BackButtonOnClick(object sender, RoutedEventArgs e)
        {
            workItemMgmtContainer.Items.Clear();
        }
    }
}
