﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// Interaktionslogik für Slice.xaml
    /// </summary>
    /// <remarks>
    /// author: Katja Kevic
    /// date (yyyy mm dd): 2012 08 11
    /// </remarks>
    public partial class Slice : SurfaceUserControl
    {
        List<DependencyObject> hitResultsList = new List<DependencyObject>();

        public Slice()
        {
            InitializeComponent();
        }

        private void slice_ContactHoldGesture(object sender, ContactEventArgs e)
        {
            //System.Console.WriteLine("IN HOLD EVENT");
           
            //Point pt = e.GetPosition((UIElement)sender);

            //hitResultsList.Clear();

            //VisualTreeHelper.HitTest(slice, null, new HitTestResultCallback(HitResult),
            //    new PointHitTestParameters(pt));

            //foreach (Visual v in hitResultsList)
            //{
            //    System.Console.WriteLine(v);
            //}
            
           
        }

        //public HitTestResultBehavior HitResult(HitTestResult result)
        //{
        //    hitResultsList.Add(result.VisualHit);
        //    return HitTestResultBehavior.Continue;
        //}

    }
}
