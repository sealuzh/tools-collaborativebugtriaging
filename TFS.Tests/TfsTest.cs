﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using System.Collections.ObjectModel;
using TfsLink;

namespace TfsLink.Tests
{
    [TestFixture]
    public class TfsTest
    {
        private Tfs tfs;

        [TestFixtureSetUp]
        public void SetUp()
        {
            tfs = new Tfs("mp1201-mona", "Reverse!", "arktos", new Uri("http://arktos.ifi.uzh.ch:8080/tfs"), "MP1201");
        }

        [Test]
        public void IsConnectionEstablishedTest()
        {
            Assert.True(tfs.IsConnectionEstablished());
        }

        [Test]
        public void IsConnectionNotEstablishedTest()
        {
            Tfs notConnectedTfs = new Tfs("notRegisteredUserName", "Reverse!", "arktos", new Uri("http://arktos.ifi.uzh.ch:8080/tfs"), "MP1201");
            Assert.False(notConnectedTfs.IsConnectionEstablished());
        }
        [Test]
        public void IsTFSAvailableTest()
        {
            Assert.True(tfs.IsTFSAvailable());
        }

        [Test]
        public void IsTFSNotAvailableTest()
        {
            Tfs notConnectedTfs = new Tfs("notRegisteredUserName", "Reverse!", "arktos", new Uri("http://arktos.ifi.uzh.ch:8080/tfs"), "MP1201");
            Assert.False(notConnectedTfs.IsTFSAvailable());
        }

        [Test]
        public void GetWorkItemTest()
        {
            WorkItem w = tfs.GetWorkItem(26);
            Assert.AreEqual("Profile Creation", w.Title);
            Assert.AreEqual("Active", w.State);
            Assert.AreEqual("New", w.Reason);
            Assert.AreEqual("MP1201\\ScrumSupporter", w.AreaPath);
            Assert.AreEqual("MP1201\\Iteration 1", w.IterationPath);
            Assert.AreEqual("Given the User has a TFS account, a profile needs to be created. All available information have to be retrieved from the TFS. The information, which is not stored on the TFS has to be queried from the Database. In case the TFS is not responding all the information needs to be available from a local source.", w.Description);
            Assert.AreEqual(2, (int)w.Fields["Priority"].Value);
            Assert.AreEqual(0, (Double)w.Fields["Stack Rank"].Value);
            Assert.AreEqual("mp1201-sam", (String)w.Fields["Assigned To"].Value);
            Assert.AreEqual(0, (double)w.Fields["Original Estimate"].Value);
            Assert.AreEqual(0, (double)w.Fields["Remaining Work"].Value);
            Assert.AreEqual(0, (double)w.Fields["Completed Work"].Value);
            Assert.AreEqual("", (String)w.Fields["Activity"].Value);
        }
        [Test]
        public void GetWorkItemTestWithInvalidId()
        {
            Assert.Throws(typeof(WorkItemTypeDeniedOrNotExistException), new TestDelegate(CreateGetWorkItemException));
        }
        private void CreateGetWorkItemException()
        {
            tfs.GetWorkItem(1000);
        }



        [Test]
        public void GetPersonsTest()
        {
            IList<String> p = tfs.GetPersons();
            CollectionAssert.AllItemsAreUnique(p);
            Assert.AreEqual(10, p.Count);
        }

        [Test]
        public void GetAllChangesetsFromDeveloperTest()
        {
            ICollection<Changeset> result = tfs.GetAllChangesetsFromDeveloper("mp1201-mona");
            Assert.AreEqual(25, result.Count);
            Changeset first = result.First();
            DateTime creationDateFirst = first.CreationDate;
            Assert.AreEqual(17, creationDateFirst.Day);
            Assert.AreEqual(12, creationDateFirst.Month);
        }

        [Test]
        public void GetAllChangesetsFromUnkownDeveloperTest()
        {
            ICollection<Changeset> result = tfs.GetAllChangesetsFromDeveloper("mp1201-monaa");
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void GetAllChangesetsFromNullTest()
        {
            ICollection<Changeset> result = tfs.GetAllChangesetsFromDeveloper(null);
            Assert.AreEqual(0, result.Count);
        }
        [Test]
        public void GetIterationsTest()
        {
            IList<Node> iterations = tfs.GetIterations();
            Assert.AreEqual(4, iterations.Count);
        }

        [Test]
        public void GetAssignedWorkItemsTest()
        {
            ICollection<WorkItem> items = tfs.GetAssignedWorkItems("mp1201-mona");
            Assert.AreEqual(16, items.Count);
        }

        [Test]
        public void GetAssignedWorkItemsUnkownDeveloperTest()
        {
            ICollection<WorkItem> items = tfs.GetAssignedWorkItems("mp1201-monaa");
            Assert.AreEqual(0, items.Count);
        }

        [Test]
        public void GetAssignedWorkItemsNullTest()
        {
            ICollection<WorkItem> items = tfs.GetAssignedWorkItems(null);
            Assert.AreEqual(0, items.Count);
        }

        [Test]
        public void GetWorkItemsOfIterationTest()
        {
            ICollection<WorkItem> result =  tfs.GetWorkItemsOfIteration("MP1201\\Iteration 1");
            Assert.AreEqual(23, result.Count);
        }

        [Test]
        public void GetWorkItemsOfIterationWithUnknownIterationNameTest()
        {
            ICollection<WorkItem> result = tfs.GetWorkItemsOfIteration("MP1201\\Iteration 100");
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void GetWorkItemsOfIterationWithnNullNameTest()
        {
            ICollection<WorkItem> result = tfs.GetWorkItemsOfIteration(null);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void GetBacklogWorkItemsTest()
        {
            ICollection<WorkItem> result = tfs.GetBacklogWorkItems();
            Assert.AreEqual(6, result.Count);
        }

        [Test]
        public void GetWorkItemsContainingStringInTitleTest()
        {
            ICollection<WorkItem> result = tfs.GetWorkItemsContainingStringInTitle("Scatter");
            Assert.AreEqual(1, result.Count);
        }


        [Test]
        public void GetChangesetsOfWorkItemTest()
        {
            ICollection<Changeset> result = tfs.GetChangesetsOfWorkItem(18);
            Assert.AreEqual(6, result.Count);
        }

        [Test]
        public void GetAllWorkItemsTest()
        {
            ICollection<WorkItem> items = tfs.GetAllWorkItems();
            Assert.AreEqual(65, items.Count);
        }

    }
}
