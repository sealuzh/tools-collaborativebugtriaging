﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using TfsLink.ProjectDashboard;

namespace TfsLink
{
    /// <summary>
    /// Interface for all interaction with the TFS
    /// </summary>
    public interface ITfs
    {
        /// <summary>
        /// Queries all work items on the TFS.
        /// </summary>
        /// <returns></returns>
        ICollection<WorkItem> GetAllWorkItems();

        /// <summary>
        /// Queries all work items in the backlog. 
        /// </summary>
        /// <returns></returns>
        ICollection<WorkItem> GetBacklogWorkItems();

        /// <summary>
        /// Queries all upcoming events. 
        /// </summary>
        /// <returns></returns>
        Collection<CalendarItem> GetCalendarItems();

        /// <summary>
        /// Queries the changesets of the logged in person. 
        /// </summary>
        /// <returns></returns>
        ICollection<Changeset> GetAllChangesetsFromDeveloper(String developersName);

        /// <summary>
        /// Queries the work items, which are assigned to a specific person.
        /// </summary>
        /// <returns></returns>
        ICollection<WorkItem> GetAssignedWorkItems(String developersName);

        /// <summary>
        /// Gets all people from the project.
        /// </summary>
        /// <returns></returns>
        IList<String> GetPersons();

        /// <summary>
        /// Queries all iterations of the project.
        /// </summary>
        /// <returns></returns>
        IList<Node> GetIterations();

        /// <summary>
        /// Gets the WorkItemStore.
        /// </summary>
        /// <returns></returns>
        WorkItemStore GetWorkItemStore();

        /// <summary>
        /// Checks if the connection is established.
        /// </summary>
        /// <returns>True, if the connection is established.</returns>
        bool IsConnectionEstablished();

        /// <summary>
        /// Uploads work items to the TFS.
        /// </summary>
        /// <param name="workItems"></param>
        /// <returns>true, if successfully uploaded.</returns>
        Boolean UploadWorkItems(ICollection<WorkItem> workItems);

        /// <summary>
        /// Queries the work item with the corresponding ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WorkItem GetWorkItem(int id);

        /// <summary>
        /// Determines if the TFS is available.
        /// </summary>
        /// <returns></returns>
        bool IsTFSAvailable();

        /// <summary>
        /// Queries all work items, which are assigned to the iteration in question.
        /// </summary>
        /// <param name="iterationName"></param>
        /// <returns></returns>
        ICollection<WorkItem> GetWorkItemsOfIteration(String iterationName);

        /// <summary>
        /// Queries all the work items, which contain the parameter String.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        ICollection<WorkItem> GetWorkItemsContainingStringInTitle(String title);

        /// <summary>
        /// Queries all changesets which are linked to the work item with the parameter id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ICollection<Changeset> GetChangesetsOfWorkItem(int id);

        String GetChangesOfChangeset(int id);

        IList<String> DownloadCurrAndPreviousFile(int changesetId, String fileName);

        String DownloadFile(int changesetId, String className);
    }
}
