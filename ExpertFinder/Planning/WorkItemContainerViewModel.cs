﻿using System;
using System.Collections.Generic;
using UZH.Infrastructure;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Timers;
using Microsoft.Surface.Presentation.Controls;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;
using ScrumSupporter.CollaborationPlatform.Exceptions;
using ScrumSupporter.Planning.WorkItemViews;
using ScrumSupporter.LocalStorage;

namespace ScrumSupporter.Planning
{
    /// <summary>
    /// ViewModel of the UserStoryContainerView.
    /// </summary>
    public class WorkItemContainerViewModel : BaseViewModel
    {
        private WorkItemContainerView view;
        private Timer timer;


        public ObservableCollection<WorkItemsOfIterationContainerViewModel> WorkItemsOfIterationContainerList { get; set; } //getter and setter needed because of the Binding Mechanism.
        public ObservableCollection<WorkItemViewModel> BacklogList { get; set; }//getter and setter needed because of the Binding Mechanism.

        private String loggedInPerson = String.Empty;

        private IList<String> availableIterations;

        public IList<String> AvailableIterations
        {
            get { return availableIterations; }
        }

        private IList<IterationModel> availableIterationsModels;

        public IList<IterationModel> AvailableIterationsModels
        {
            get { return availableIterationsModels; }
        }

        private ScatterView triagingContainer;

        public ScatterView TriagingContainer
        {
            get { return triagingContainer; }
            set { triagingContainer = value; }
        }


        public WorkItemContainerViewModel()
        {
            BacklogList = new ObservableCollection<WorkItemViewModel>();
            WorkItemsOfIterationContainerList = new ObservableCollection<WorkItemsOfIterationContainerViewModel>();


            timer = new Timer(3000);
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Enabled = false;

            availableIterations = new List<String>();
            availableIterationsModels = new List<IterationModel>();
        }


        private void GetAvailableIterations(ICollaborationPlatform platform)
        {
            ICollection<string> iterations = platform.GetIterationService().GetAllIterationNames();
            foreach (string iteration in iterations)
            {
                availableIterations.Add(iteration);
                availableIterationsModels.Add(new IterationModel() { Name = iteration });
            }
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            UploadWorkItems();
        }

        /// <summary>
        /// Loads the name of the logged in person and the work items from the collaboration platform and into the Database.
        /// </summary>
        /// <param name="platform"></param>
        public void LoadData(ICollaborationPlatform platform)
        {
            if (platform != null)
            {
                loggedInPerson = platform.GetLoggedInPerson();

                //Load the backlog
                ICollection<WorkItemModel> backlog = platform.GetWorkItemService().GetBacklogWorkItems();
                foreach (var model in backlog)
                {
                    BacklogList.Add(model.CreateWorkItemViewModel());
                }
                DBConnection dbConn = GetDBConnection();
                if (dbConn.IsOpenSuccessful())
                {
                    dbConn.LoadWorkItemModelsIntoDB(this.BacklogList);
                }
                dbConn.Dispose();

                foreach (WorkItemViewModel workItemViewModel in BacklogList)
                {
                    workItemViewModel.TriagingContainer = triagingContainer;
                }

                //Load the iterations and the according work items
                GetAvailableIterations(platform);
                foreach (var iteration in availableIterationsModels)
                {
                    WorkItemsOfIterationContainerViewModel containerModel = new WorkItemsOfIterationContainerViewModel(GetWorkItemsOfThisIteration(iteration.Name, platform));
                    WorkItemsOfIterationContainerList.Add(containerModel);
                }
            }
            else throw new CollaborationPlatformUnavailableException("ICollaborationPlatform is null");
        }

        /// <summary>
        /// Loads the available data from a local source.
        /// </summary>
        public void LoadLocalData()
        {
            DBConnection dbConn = GetDBConnection();
            if (dbConn.IsOpenSuccessful())
            {
                ICollection<WorkItemModel> backlogItems = dbConn.QueryBacklogItems();
                foreach (WorkItemModel workItemModel in backlogItems)
                {
                    this.BacklogList.Add(workItemModel.GetViewModelType());
                }
                ICollection<WorkItemsOfIterationContainerViewModel> iterationsItems = dbConn.QueryItemsOfIterations();
                foreach (WorkItemsOfIterationContainerViewModel workItemsOfIterationContainerVm in iterationsItems)
                {
                    WorkItemsOfIterationContainerList.Add(workItemsOfIterationContainerVm);
                }
            }
            dbConn.Dispose();
        }


        private DBConnection GetDBConnection()
        {
            DBConnection conn = new Db4oConnection();
            if (conn.IsOpenSuccessful()) return conn;
            else return new NullDatabaseConnection();
        }


        private IList<WorkItemViewModel> GetWorkItemsOfThisIteration(String iteration, ICollaborationPlatform platform)
        {
            Collection<WorkItemViewModel> workItemViewModels = new Collection<WorkItemViewModel>();

            ICollection<WorkItemModel> workItems = platform.GetWorkItemService().GetWorkItemsOfIteration(iteration);
            foreach (WorkItemModel workItemModel in workItems)
            {
                workItemViewModels.Add(workItemModel.CreateWorkItemViewModel());
            }

            foreach (WorkItemViewModel workItemViewModel in workItemViewModels)
            {
                workItemViewModel.TriagingContainer = triagingContainer;
            }

            DBConnection dbConn = GetDBConnection();
            if (dbConn.IsOpenSuccessful())
            {
                dbConn.LoadWorkItemModelsIntoDB(workItemViewModels);
            }
            dbConn.Dispose();

            return workItemViewModels;
        }

        /// <summary>
        /// Creates the WorkItemContainerView and adds itself as DataContext.
        /// </summary>
        /// <returns></returns>
        public WorkItemContainerView CreateView()
        {
            var v = view ?? (view = new WorkItemContainerView());
            v.DataContext = this;
            triagingContainer = v.triagingContainer;

            return v;
        }

        /// <summary>
        /// Adds a bug with initial values to the backlog.
        /// </summary>
        public ICommand AddBugToBacklog
        {
            get { return new RelayCommand(AddBugToBacklogExecute, CanAddWorkItemBacklogExecute); }
        }

        private void AddBugToBacklogExecute()
        {
            WorkItemModel model = new BugModel()
            {
                Title = "new Bug",
                Area = "MP1201\\ScrumSupporter",
                Iteration = "MP1201",
                State = "Active",
                Reason = "New",
                Priority = 2,
                Severity = "3 - Medium",
                AssignedTo = ""
            };
            WorkItemViewModel workItemViewModel = new BugViewModel(model);
            workItemViewModel.TriagingContainer = triagingContainer;
            BacklogList.Add(workItemViewModel);
        }

        private bool CanAddWorkItemBacklogExecute()
        {
            return true;
        }

        /// <summary>
        /// Adds a task with initial values to the backlog.
        /// </summary>
        public ICommand AddTaskToBacklog
        {
            get { return new RelayCommand(AddTaskToBacklogExecute, CanAddWorkItemBacklogExecute); }
        }

        private void AddTaskToBacklogExecute()
        {
            WorkItemModel model = new TaskModel()
            {
                Title = "new Task",
                Area = "MP1201\\ScrumSupporter",
                Iteration = "MP1201",
                State = "Active",
                Reason = "New",
                Priority = 2,
                AssignedTo = "",
            };
            WorkItemViewModel workItemViewModel = new TaskViewModel(model);
            workItemViewModel.TriagingContainer = triagingContainer;
            BacklogList.Add(workItemViewModel);
        }


        /// <summary>
        /// Adds a user story with initial values to the backlog.
        /// </summary>
        public ICommand AddUserStoryToBacklog
        {
            get { return new RelayCommand(AddUserStoryToBacklogExecute, CanAddWorkItemBacklogExecute); }
        }

        private void AddUserStoryToBacklogExecute()
        {
            WorkItemModel model = new UserStoryModel()
            {
                Title = "new User Story",
                Area = "MP1201\\ScrumSupporter",
                Iteration = "MP1201",
                State = "Active",
                Reason = "New",
                AssignedTo = loggedInPerson,
                Description = "As a <type of user> I want <some goal> so that <some reason>"
            };
            WorkItemViewModel workItemViewModel = new UserStoryViewModel(model);
            workItemViewModel.TriagingContainer = triagingContainer;
            BacklogList.Add(workItemViewModel);
        }

        /// <summary>
        /// Adds an issue with initial values to the backlog. The issue is assigned to the person which is currently logged in.
        /// </summary>
        public ICommand AddIssueToBacklog
        {
            get { return new RelayCommand(AddIssueToBacklogExecute, CanAddWorkItemBacklogExecute); }
        }

        private void AddIssueToBacklogExecute()
        {
            WorkItemModel model = new IssueModel()
            {
                Title = "new Issue",
                Area = "MP1201\\ScrumSupporter",
                Iteration = "MP1201",
                State = "Active",
                Reason = "New",
                AssignedTo = loggedInPerson,
                Priority = 2
            };
            WorkItemViewModel workItemViewModel = new IssueViewModel(model);
            workItemViewModel.TriagingContainer = triagingContainer;
            BacklogList.Add(workItemViewModel);
        }

        /// <summary>
        /// Uploads all work items to the Collabration Platform.
        /// </summary>
        public void UploadWorkItems()
        {
            CollaborationPlatformFactory factory = new CollaborationPlatformFactory();
            ICollaborationPlatform platform = factory.CollaborationPlatform;
            if (platform.IsCollaborationPlatformAvailable())
            {
                //upload all work items from the backlog
                ICollection<WorkItemModel> backlogModels = new List<WorkItemModel>();
                foreach (WorkItemViewModel viewModel in BacklogList)
                {
                    backlogModels.Add(viewModel.GetModel());
                }
                platform.GetWorkItemService().UploadWorkItems(backlogModels);

                DBConnection dbConn = GetDBConnection();
                if (dbConn.IsOpenSuccessful())
                {
                    dbConn.LoadWorkItemModelsIntoDB(this.BacklogList);
                }


                //upload all work items from iterations
                ICollection<WorkItemModel> sprintModels = new List<WorkItemModel>();
                foreach (WorkItemsOfIterationContainerViewModel iterationViewModel in WorkItemsOfIterationContainerList)
                {
                    foreach (WorkItemViewModel viewModel in iterationViewModel.WorkItemList)
                    {
                        sprintModels.Add(viewModel.GetModel());
                    }
                    if (dbConn.IsOpenSuccessful())
                    {
                        dbConn.LoadWorkItemModelsIntoDB(iterationViewModel.WorkItemList);
                    }
                }

                platform.GetWorkItemService().UploadWorkItems(sprintModels);
                dbConn.Dispose();
            }
        }


    }
}
