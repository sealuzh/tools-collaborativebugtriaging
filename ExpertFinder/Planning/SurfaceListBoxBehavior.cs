﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.Planning
{
    public static class SurfaceListBoxBehavior
    {

        public static readonly DependencyProperty ContactDownCommand =
            EventBehaviourFactory.CreateCommandExecutionEventBehaviour(SurfaceListBox.PreviewContactDownEvent, "ContactDownCommand", typeof(SurfaceListBoxBehavior));

        public static void SetContactDownCommand(DependencyObject o, ICommand value)
        {
            o.SetValue(ContactDownCommand, value);
        }

        public static ICommand GetContactDownCommand(DependencyObject o)
        {
            return o.GetValue(ContactDownCommand) as ICommand;
        }


    }
}
