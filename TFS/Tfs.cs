﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.TeamFoundation.Server;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation;
using System.Collections;
using Microsoft.TeamFoundation.Client;
using System.IO;
using Microsoft.TeamFoundation.VersionControl.Common;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace TfsLink
{
    /// <summary>
    /// Implements the Interface ITfs.
    /// </summary>
    public class Tfs : ITfs, IDisposable
    {

        private IConnection connection;
        private PDSharepoint sharepoint;
        private Boolean connectionEstablished;
        private String projectName;

        private TfsTeamProjectCollection teamProjectCollection;

        /// <summary>
        /// Constructor, which establishes the connection to the Team Foundation Server.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="domain"></param>
        /// <param name="uri"></param>
        /// <param name="project"></param>
        public Tfs(string username, string password, string domain, Uri uri, String project)
        {
            projectName = project;

            connection = new Connection(username, password, domain, uri);
            connectionEstablished = connection.Connect();

            sharepoint = new PDSharepoint(username, password);
        }


        /// <summary>
        /// Checks if the WorkItemStore and the VersionControlServer are not null.
        /// </summary>
        /// <returns>true, if the WorkItemStore and the VersionControlServer are not null.</returns>
        public bool IsTFSAvailable()
        {
            try
            {
                teamProjectCollection = connection.GetTeamProjectCollection();
                if (teamProjectCollection == null) return false;
                teamProjectCollection.Authenticate();
                WorkItemStore workItemStore = teamProjectCollection.GetService<WorkItemStore>();
                VersionControlServer versionControlServer = teamProjectCollection.GetService<VersionControlServer>();
                if (workItemStore != null && versionControlServer != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (TeamFoundationServiceUnavailableException serviceUnavailableException)
            {
                System.Console.WriteLine(serviceUnavailableException.Message);
                return false;
            }
            catch (TeamFoundationServerUnauthorizedException serverUnauthorizedException)
            {
                System.Console.WriteLine(serverUnauthorizedException.Message);
                return false;
            }
            finally
            {
                Dispose();
            }
        }


        /// <summary>
        /// Checks if the connection is established.
        /// </summary>
        /// <returns>True, if the connection is established.</returns>
        public bool IsConnectionEstablished()
        {
            return connectionEstablished;
        }

        /// <summary>
        /// Queries the WorkItemStore with the id of the work item. If the work item is not found or the access is 
        /// denied an exception is thrown.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WorkItem GetWorkItem(int id)
        {
            ICollection<WorkItem> items = QueryWorkItemStore("Select [Title] From WorkItems Where [Id] = '" + id.ToString() + "' And [Area Path] = 'MP1201\\ScrumSupporter' ");
            if (items.Count != 0)
            {
                return items.First();
            }
            else throw new WorkItemTypeDeniedOrNotExistException("work item with id: " + id.ToString() + " not found or access denied");
        }

        private Project GetProject()
        {
            try
            {
                WorkItemStore workItemStore = connection.GetTeamProjectCollection().GetService<WorkItemStore>();
                ProjectCollection projects = workItemStore.Projects;
                foreach (Project p in projects)
                {
                    if (p.Name.Equals(projectName))
                    {
                        return p;
                    }
                }
            }
            catch (TeamFoundationServiceUnavailableException e)
            {
                System.Console.WriteLine(e.Message);
            }
            return null;
        }

        private IList<Node> AddChildNodes(Node node, IList<Node> lstIterations)
        {
            foreach (Node node1 in node.ChildNodes)
            {
                lstIterations.Add(node1);
                AddChildNodes(node1, lstIterations);
            }
            return lstIterations;
        }

        /// <summary>
        /// Uploads the WorkItems to the TFS.
        /// </summary>
        /// <param name="workItems"></param>
        /// <returns></returns>
        public bool UploadWorkItems(ICollection<WorkItem> workItems)
        {
            if (workItems != null)
            {
                foreach (WorkItem w in workItems)
                {
                    try
                    {
                        w.Save();
                    }
                    catch (ValidationException exception)
                    {
                        Console.WriteLine("The work item threw a validation exception.");
                        Console.WriteLine(exception.Message);
                        return false;
                    }
                }
                return true;
            }
            else throw new ArgumentNullException("workItems", "is null");
        }

        /// <summary>
        /// Returns the WorkItemStore.
        /// </summary>
        /// <returns></returns>
        WorkItemStore ITfs.GetWorkItemStore()
        {
            return connection.GetTeamProjectCollection().GetService<WorkItemStore>();
        }

        /// <summary>
        /// Queries all iterations of the project.
        /// </summary>
        /// <returns></returns>
        public IList<Node> GetIterations()
        {
            IList<Node> lstIterations = new List<Node>();
            Project project = GetProject();

            if (project != null)
            {
                foreach (Node node in project.IterationRootNodes)
                {
                    lstIterations.Add(node);
                    lstIterations.Concat(AddChildNodes(node, lstIterations));
                }
            }
            return lstIterations;
        }

        /// <summary>
        /// Queries the GroupSecurityService about the project members.
        /// </summary>
        /// <returns></returns>
        public IList<String> GetPersons()
        {
            IList<String> persons = new List<String>();
            IGroupSecurityService gss = (IGroupSecurityService)connection.GetTeamProjectCollection().GetService(typeof(IGroupSecurityService));

            Identity sids = gss.ReadIdentity(SearchFactor.AccountName, projectName, QueryMembership.Expanded);

            Identity[] users = gss.ReadIdentities(SearchFactor.Sid, sids.Members, QueryMembership.Expanded);

            foreach (Identity ident in users)
            {
                persons.Add(ident.AccountName);
            }
            return persons;
        }

        /// <summary>
        /// Queries all Changesets which were commited by the Person with the developersName.
        /// </summary>
        /// <param name="developersName"></param>
        /// <returns>A collection with the latest Changeset first.</returns>
        public ICollection<Changeset> GetAllChangesetsFromDeveloper(String developersName)
        {
            Collection<Changeset> changesets = new Collection<Changeset>();
            try
            {
                teamProjectCollection = connection.GetTeamProjectCollection();
                if (developersName != null && teamProjectCollection != null)
                {
                    VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();
                    IEnumerable result = vcs.QueryHistory("$/", VersionSpec.Latest, 0, RecursionType.Full, developersName, null, null, int.MaxValue, false, false);
                    foreach (Changeset c in result)
                    {
                        changesets.Add(c);
                    }
                    return changesets;
                }
                else return changesets;
            }
            catch (IdentityNotFoundException e)
            {
                System.Console.WriteLine(e.Message);
                return changesets;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        /// Looks up the calendar items on the ProjectDashboard.
        /// </summary>
        /// <returns></returns>
        public Collection<TfsLink.ProjectDashboard.CalendarItem> GetCalendarItems()
        {
            return sharepoint.GetCalenderItems();
        }

        /// <summary>
        /// Queries all work items which are assignes to the person with the developersName.
        /// </summary>
        /// <param name="developersName"></param>
        /// <returns></returns>
        public ICollection<WorkItem> GetAssignedWorkItems(string developersName)
        {
            if (developersName != null)
            {
                return QueryWorkItemStore("Select [Title] From WorkItems Where [Assigned To] = '" + developersName + "' Order by [Changed Date] [desc]");
            }
            else return new List<WorkItem>();
        }

        /// <summary>
        /// Queries all WorkItems of the Project.
        /// </summary>
        /// <returns></returns>
        public ICollection<WorkItem> GetAllWorkItems()
        {
            return QueryWorkItemStore("Select [Title] From WorkItems Where [Area Path] = 'MP1201\\ScrumSupporter'");
        }

        /// <summary>
        /// Queries all the changesets which are linked to the iteration given as parameter.
        /// </summary>
        /// <param name="iterationName"></param>
        /// <returns></returns>
        public ICollection<WorkItem> GetWorkItemsOfIteration(String iterationName)
        {
            try
            {
                return QueryWorkItemStore("Select [Title] From WorkItems Where [Iteration Path] = '" + iterationName + "' And [Area Path] = 'MP1201\\ScrumSupporter' Order by [Stack Rank] [desc]");
            }
            catch (ValidationException e)
            {
                System.Console.WriteLine(e.Message);
                return new List<WorkItem>();
            }
        }

        /// <summary>
        /// Queries the WorkItems which are not assiged yet to an iteration. 
        /// </summary>
        /// <returns></returns>
        public ICollection<WorkItem> GetBacklogWorkItems()
        {
            return QueryWorkItemStore("Select [Title] From WorkItems Where [Iteration Path] = '" + projectName + "' And [Area Path] = 'MP1201\\ScrumSupporter' Order by [Stack Rank] [desc]");
        }

        /// <summary>
        /// Queries the WorkItems which contain the String given as parameter in their title.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public ICollection<WorkItem> GetWorkItemsContainingStringInTitle(string title)
        {
            return QueryWorkItemStore("Select [Title] From WorkItems Where " +
                "[Area Path] = 'MP1201\\ScrumSupporter' And" +
                "[Title] Contains '" + title + "'");
        }


        private ICollection<WorkItem> QueryWorkItemStore(String query)
        {
            try
            {
                teamProjectCollection = connection.GetTeamProjectCollection();
                if (teamProjectCollection == null) return new List<WorkItem>();

                WorkItemStore workItemStore = teamProjectCollection.GetService<WorkItemStore>();
                WorkItemCollection result = workItemStore.Query(query);

                ICollection<WorkItem> items = new List<WorkItem>();
                foreach (WorkItem workItem in result)
                {
                    items.Add(workItem);
                }
                return items;
            }
            finally
            {
                Dispose();
            }
        }


        /// <summary>
        /// Queries the Changesets which are associated to a WorkItem.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ICollection<Changeset> GetChangesetsOfWorkItem(int id)
        {
            try
            {
                teamProjectCollection = connection.GetTeamProjectCollection();
                if (teamProjectCollection == null) return new List<Changeset>();

                var versionControlServer = teamProjectCollection.GetService<VersionControlServer>();
                var artifactProvider = versionControlServer.ArtifactProvider;

                WorkItem w = GetWorkItem(id);
                ICollection<Changeset> changesetsOfWorkItem = new List<Changeset>();
                foreach (var changeset in w.Links.OfType<ExternalLink>().Select(link => artifactProvider.GetChangeset(new Uri(link.LinkedArtifactUri))))
                {
                    changesetsOfWorkItem.Add(changeset);
                }
                return changesetsOfWorkItem;
            }
            finally
            {
                Dispose();
            }
        }

        public void Dispose()
        {
            if (teamProjectCollection != null) teamProjectCollection.Dispose();
        }


        public ICollection<Changeset> TestChangeset()
        {
            ICollection<Changeset> changesets = new List<Changeset>();
            teamProjectCollection = connection.GetTeamProjectCollection();
            VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();

            IEnumerable result = vcs.QueryHistory("$/", VersionSpec.Latest, 0, RecursionType.Full, "mp1201-tom", null, null, int.MaxValue, true, false, true);
            foreach (Changeset c in result)
            {
                System.Console.WriteLine("*********************new changeset******************************");
                changesets.Add(c);

                Change[] changes = vcs.GetChangesForChangeset(c.ChangesetId, true, int.MaxValue, null);

                foreach (Change theChange in changes)
                {
                    System.Console.WriteLine("-----------------new change----------");
                    //int changeId = (theChange.Item.DeletionId != 0) ?
                    //theChange.Item.ChangesetId - 1 :
                    // theChange.Item.ChangesetId;

                    Item newItem = theChange.Item;


                    ChangesetVersionSpec version = new
                            ChangesetVersionSpec(theChange.Item.ChangesetId);
                    ChangesetVersionSpec versionFrom = new
                                          ChangesetVersionSpec(1);
                    string path = theChange.Item.ServerItem;

                    //Query History Command
                    IEnumerable histories = vcs.QueryHistory(path,
                             version, 0, RecursionType.None, null,
                             versionFrom, LatestVersionSpec.Latest,
                             int.MaxValue, true, false);

                    Item oldItem = null;
                    foreach (Changeset history in histories)
                    {
                        Change[] changesOfOldChangeset = history.Changes;
                        foreach (Change oldChange in changesOfOldChangeset)
                        {
                            if (oldChange.Item.ServerItem.Equals(newItem.ServerItem))
                            {
                                oldItem = oldChange.Item;
                            }
                        }
                        break;
                    }

                    int minRevision = 185;
                    int maxRevision = 331;

                    var before = new DiffItemVersionedFile(newItem, VersionSpec.ParseSingleSpec(minRevision.ToString(), null));
                    var after = new DiffItemVersionedFile(oldItem, VersionSpec.ParseSingleSpec(maxRevision.ToString(), null));

                    //Difference.VisualDiffItems(vcs, before, after);
                    var winmerge = Process.Start(@"C:\Program Files (x86)\WinMerge\WinMergeU.exe",
                                        String.Format("{0}{1} {0}{2}", System.IO.Path.GetTempPath(),
                                                      @"\36TfsRepository.cs", ""));





                    using (var stream = new MemoryStream())
                    using (var writer = new StreamWriter(stream))
                    {
                        var options = new DiffOptions();
                        options.Flags = DiffOptionFlags.None;
                        options.OutputType = DiffOutputType.Unified;
                        options.TargetEncoding = Encoding.UTF8;
                        options.SourceEncoding = Encoding.UTF8;
                        options.StreamWriter = writer;
                        options.UseThirdPartyTool = true;


                        Difference.DiffFiles(vcs, before, after, options, path, true);


                        //Difference.VisualDiffFiles(vcs, path, VersionSpec.ParseSingleSpec(minRevision.ToString(), null), path, VersionSpec.Latest);
                        //Difference.VisualDiffItems(vcs, before, after);


                        //File.WriteAllBytes("C:/Users/Katja/Documents/TEST.vss", stream.ToArray());
                        writer.Flush();

                        var diff = Encoding.UTF8.GetString(stream.ToArray());
                        System.Console.WriteLine(diff);
                        System.Console.ReadLine();

                    }


                    break;
                }
                break;
            }
            return changesets;

        }

        public static void Main()
        {
            Tfs tfs = new Tfs("kevic", "Reverse!", "arktos", new Uri("http://arktos.ifi.uzh.ch:8080/tfs"), "MP1201");

            //tfs.GetChangesOfChangeset(90);

            //ICollection<Changeset> result = tfs.TestChangeset();

            tfs.DownloadFilesOfChangesets(55);

            System.Console.ReadLine();
        }


        public void DownloadFilesOfChangesets(int id)
        {

            //Get the changeset
            teamProjectCollection = connection.GetTeamProjectCollection();
            VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();
            Changeset theChangeset = vcs.GetChangeset(id, true, true);

            Change[] allChanges = vcs.GetChangesForChangeset(theChangeset.ChangesetId, true, int.MaxValue, null);
            IList<Change> codeChanges = new List<Change>();
            foreach (Change ch in allChanges)
            {
                if (ch.Item.ServerItem.EndsWith(".cs"))
                {
                    codeChanges.Add(ch);
                }
            }


            foreach (Change change in codeChanges)
            {
                if (change.ChangeType.ToString().Equals("Edit"))
                {
                    Change previouVersionOfChange = GetPreviousVersionOfChange(change, vcs);

                    System.Console.WriteLine(System.IO.Path.GetTempPath());

                    change.Item.DownloadFile(System.IO.Path.GetTempPath() + change.Item.ChangesetId +
                                      change.Item.ServerItem.Split('/')[change.Item.ServerItem.Split('/').Length - 1]);

                    previouVersionOfChange.Item.DownloadFile(System.IO.Path.GetTempPath() + previouVersionOfChange.Item.ChangesetId +
                                      previouVersionOfChange.Item.ServerItem.Split('/')[previouVersionOfChange.Item.ServerItem.Split('/').Length - 1]);


                }

            }

        }





        public string GetChangesOfChangeset(int id)
        {
            String changesString = "";

            //Get the changeset
            teamProjectCollection = connection.GetTeamProjectCollection();
            VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();
            Changeset theChangeset = vcs.GetChangeset(id, true, true);

            Change[] allChanges = vcs.GetChangesForChangeset(theChangeset.ChangesetId, true, int.MaxValue, null);
            IList<Change> codeChanges = new List<Change>();
            foreach (Change ch in allChanges)
            {
                if (ch.Item.ServerItem.EndsWith(".cs"))
                {
                    codeChanges.Add(ch);
                }
            }


            foreach (Change change in codeChanges)
            {
                if (change.ChangeType.ToString().Equals("Edit"))
                {
                    Item newItem = change.Item;
                    Item previouVersionOfItem = GetPreviousVersionOfItem(change, vcs);

                    int minRevision = newItem.ItemId;
                    int maxRevision = previouVersionOfItem.ItemId;

                    var laterChange = new DiffItemVersionedFile(newItem, VersionSpec.ParseSingleSpec(minRevision.ToString(), null));
                    var previousChange = new DiffItemVersionedFile(previouVersionOfItem, VersionSpec.ParseSingleSpec(maxRevision.ToString(), null));



                    using (var stream = new MemoryStream())
                    using (var writer = new StreamWriter(stream))
                    {
                        var options = new DiffOptions();
                        options.Flags = DiffOptionFlags.EnablePreambleHandling;
                        options.OutputType = DiffOutputType.Unified;
                        options.TargetEncoding = Encoding.UTF8;
                        options.SourceEncoding = Encoding.UTF8;
                        options.StreamWriter = writer;
                        Difference.DiffFiles(vcs, previousChange, laterChange, options, change.Item.ServerItem, true);


                        writer.Flush();

                        var diff = Encoding.UTF8.GetString(stream.ToArray());
                        changesString += diff;

                    }
                }

            }

            return changesString;
        }


        private Item GetPreviousVersionOfItem(Change change, VersionControlServer vcs)
        {
            Item newItem = change.Item;

            ChangesetVersionSpec version = new
                    ChangesetVersionSpec(change.Item.ChangesetId);
            ChangesetVersionSpec versionFrom = new
                                  ChangesetVersionSpec(1);
            ChangesetVersionSpec versionTo = new
                    ChangesetVersionSpec(change.Item.ChangesetId - 1);


            string path = change.Item.ServerItem;

            //Query History Command
            IEnumerable histories = vcs.QueryHistory(path,
                     version, 0, RecursionType.None, null,
                     versionFrom, versionTo,
                     int.MaxValue, true, false);

            Item oldItem = null;
            foreach (Changeset history in histories)
            {
                Change[] changesOfOldChangeset = history.Changes;
                foreach (Change oldChange in changesOfOldChangeset)
                {
                    if (oldChange.Item.ServerItem.Equals(newItem.ServerItem))
                    {
                        oldItem = oldChange.Item;
                    }
                }
                break;
            }

            return oldItem;
        }

        private Change GetPreviousVersionOfChange(Change change, VersionControlServer vcs)
        {
            Item newItem = change.Item;

            ChangesetVersionSpec version = new
                    ChangesetVersionSpec(change.Item.ChangesetId);
            ChangesetVersionSpec versionFrom = new
                                  ChangesetVersionSpec(1);
            ChangesetVersionSpec versionTo = new
                    ChangesetVersionSpec(change.Item.ChangesetId - 1);


            string path = change.Item.ServerItem;

            //Query History Command
            IEnumerable histories = vcs.QueryHistory(path,
                     version, 0, RecursionType.None, null,
                     versionFrom, versionTo,
                     int.MaxValue, true, false);

            Change oldChange = null;
            foreach (Changeset history in histories)
            {
                Change[] changesOfOldChangeset = history.Changes;
                foreach (Change ch in changesOfOldChangeset)
                {
                    if (ch.Item.ServerItem.Equals(newItem.ServerItem))
                    {
                        return ch;
                    }
                }
                break;
            }

            return oldChange;
        }



        public IList<String> DownloadCurrAndPreviousFile(int changesetId, string fileName)
        {
            IList<String> location = new List<String>();

            //Get the changeset
            teamProjectCollection = connection.GetTeamProjectCollection();
            VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();
            Changeset theChangeset = vcs.GetChangeset(changesetId, true, true);

            Change[] allChanges = vcs.GetChangesForChangeset(theChangeset.ChangesetId, true, int.MaxValue, null);

            foreach (Change ch in allChanges)
            {
                if (ch.ChangeType.ToString().Equals("Edit"))
                {
                    if (ch.Item.ServerItem.EndsWith(fileName))
                    {
                        Change previouVersionOfChange = GetPreviousVersionOfChange(ch, vcs);


                        ch.Item.DownloadFile(System.IO.Path.GetTempPath() + ch.Item.ChangesetId +
                                          ch.Item.ServerItem.Split('/')[ch.Item.ServerItem.Split('/').Length - 1]);

                        location.Add(ch.Item.ChangesetId +
                                          ch.Item.ServerItem.Split('/')[ch.Item.ServerItem.Split('/').Length - 1]);

                        previouVersionOfChange.Item.DownloadFile(System.IO.Path.GetTempPath() + previouVersionOfChange.Item.ChangesetId +
                                          previouVersionOfChange.Item.ServerItem.Split('/')[previouVersionOfChange.Item.ServerItem.Split('/').Length - 1]);

                        location.Add(previouVersionOfChange.Item.ChangesetId +
                                          previouVersionOfChange.Item.ServerItem.Split('/')[previouVersionOfChange.Item.ServerItem.Split('/').Length - 1]);
                        break;
                    }
                }
            }
            return location;

        }


        public string DownloadFile(int changesetId, string fileName)
        {
            String location = "";

            //Get the changeset
            teamProjectCollection = connection.GetTeamProjectCollection();
            VersionControlServer vcs = teamProjectCollection.GetService<VersionControlServer>();
            Changeset theChangeset = vcs.GetChangeset(changesetId, true, true);

            Change[] allChanges = vcs.GetChangesForChangeset(theChangeset.ChangesetId, true, int.MaxValue, null);

            foreach (Change ch in allChanges)
            {
                if (ch.Item.ServerItem.EndsWith(fileName))
                {
                    System.Console.WriteLine(System.IO.Path.GetTempPath());
                    ch.Item.DownloadFile(System.IO.Path.GetTempPath() + ch.Item.ChangesetId +
                                      ch.Item.ServerItem.Split('/')[ch.Item.ServerItem.Split('/').Length - 1]);

                    location = System.IO.Path.GetTempPath()+ ch.Item.ChangesetId +
                                      ch.Item.ServerItem.Split('/')[ch.Item.ServerItem.Split('/').Length - 1];
                    break;
                }

            }
            return location;
        }
    }
}
