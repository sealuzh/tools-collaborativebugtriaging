﻿using System.Windows;
using Microsoft.Surface.Presentation.Controls;
using System;
using Microsoft.Surface.Presentation.Manipulations;
using Microsoft.Surface.Presentation;
using ScrumSupporter.Planning;
using System.IO;
using System.Windows.Ink;

namespace ScrumSupporter.Planning.WorkItemViews
{
    /// <summary>
    /// Interaktionslogik für BugEditingView.xaml
    /// </summary>
    public partial class BugEditingView : SurfaceUserControl
    {
        public BugEditingView()
        {
            InitializeComponent();
            
        }

        private void OnOpenTriagingView(object sender, RoutedEventArgs e)
        {
            WorkItemViewModel dataContext = this.DataContext as WorkItemViewModel;
            dataContext.OnOpenTriaging(FindResource("sviStyle") as Style);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            String strokesPath = "Resources/Strokes/" + (this.DataContext as BugViewModel).Id + ".isf";
            StrokesHandler strokesHandler = new StrokesHandler(strokesPath, inkCanvas, SaveInkButton, EraseInkButton, BackButton);
            try
            {
                strokesHandler.LoadStrokes();
            }
            catch (FileLoadException e1)
            {
                System.Console.WriteLine(e1.Message);
            }

            OpenInkCanvasButton.Click += strokesHandler.OnOpenInkCanvas;
        }



    }
}
