﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;
using ScrumSupporter.Planning.WorkItemViews;

namespace ScrumSupporter.Planning
{
    /// <summary>
    /// Interaktionslogik für SingleIterationView.xaml. Containes event handlers for drag and drop operations, as well as methods to 
    /// change the appearance of the Listbox according to the size of the user control. 
    /// </summary>
    public partial class SingleIterationView : SurfaceUserControl
    {
        /// <summary>
        /// Constructor, which initializes the components. 
        /// </summary>
        public SingleIterationView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the visibility property of the three different shapes of the ListBox. 
        /// </summary>
        public void OnSizeChangedToBig()
        {
            BigListBox.Visibility = Visibility.Visible;
            SmallListBox.Visibility = Visibility.Hidden;
            VeryBigListBox.Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// Sets the visibility property of the three different shapes of the ListBox. 
        /// </summary>
        public void OnSizeChangedToSmall()
        {

            BigListBox.Visibility = Visibility.Hidden;
            SmallListBox.Visibility = Visibility.Visible;
            VeryBigListBox.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Sets the visibility property of the three different shapes of the ListBox. 
        /// </summary>
        public void OnSizeChangedToVeryBig()
        {
            BigListBox.Visibility = Visibility.Hidden;
            SmallListBox.Visibility = Visibility.Hidden;
            VeryBigListBox.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Determines the selected workitem through the SelectedItem property of ListBox. 
        /// </summary>
        /// <returns>the selected work item.</returns>
        public WorkItemViewModel OnClickedOnWorkItem()
        {
            if (SmallListBox.IsVisible)
            {
                object item = SmallListBox.SelectedItem;
                if (item is WorkItemViewModel)
                {
                    return item as WorkItemViewModel;
                }
                else
                {
                    InvalidOperationException invalidOperationException = new InvalidOperationException("No Work Item selected");
                    throw invalidOperationException;
                }
            }
            else if (BigListBox.IsVisible)
            {
                object item = SmallListBox.SelectedItem;
                if (item is WorkItemViewModel)
                {
                    return item as WorkItemViewModel;
                }
                else
                {
                    InvalidOperationException invalidOperationException = new InvalidOperationException("No Work Item selected");
                    throw invalidOperationException;
                }
            }
            else if (VeryBigListBox.IsVisible)
            {
                object item = SmallListBox.SelectedItem;
                if (item is WorkItemViewModel)
                {
                    return item as WorkItemViewModel;
                }
                else
                {
                    InvalidOperationException invalidOperationException = new InvalidOperationException("No Work Item selected");
                    throw invalidOperationException;
                }
            }
            else
            {
                InvalidOperationException invalidOperationException = new InvalidOperationException("No Work Item selected");
                throw invalidOperationException;
            }


        }

        /// <summary>
        /// Initializes the drag and drop operation of work items in the single iteration view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartDragAndDrop(object sender, Microsoft.Surface.Presentation.ContactEventArgs e)
        {
            SurfaceListBox parent = (SurfaceListBox)sender;

            DependencyObject depO = parent.ItemContainerGenerator.ContainerFromItem(parent.SelectedItem);
            UIElement draggedElement = depO as UIElement;
            FrameworkElement frElement = draggedElement as FrameworkElement;
            if (frElement == null)
            {
                return;
            }

            WorkItemViewModel data = parent.SelectedItem as WorkItemViewModel;

            if (data == null)
            {
                return;
            }

            // Set the dragged element. This is needed in case the drag operation is canceled.
            data.DraggedElement = draggedElement;

            ContentControl cursorVisual = GetCursor(data);

            // Create a list of input devices, 
            ///and add the device passed to this event handler.
            List<InputDevice> devices = new List<InputDevice>();
            devices.Add(e.Device);

            // If there are touch devices captured within the element,
            // add them to the list of input devices.
            foreach (InputDevice device in draggedElement.TouchesCapturedWithin)
            {
                if (device != e.Device)
                {
                    devices.Add(device);
                }
            }

            //Get the drag source object.
            ItemsControl dragSource = ItemsControl.ItemsControlFromItemContainer(draggedElement);

            // Start the drag-and-drop operation.
            SurfaceDragDrop.BeginDragDrop(
              dragSource,
              frElement,
              cursorVisual,
              data,
              devices,
              DragDropEffects.Move);

            draggedElement.Visibility = Visibility.Hidden;
            parent.SelectedIndex = -1;
            e.Handled = true;
        }

        /// <summary>
        /// Loads the cursor style from the dictionary depending on the work item type. 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ContentControl GetCursor(WorkItemViewModel model)
        {

            if (model is BugViewModel)
            {
                ContentControl cursorVisual = new ContentControl()
                            {
                                Content = model,
                                Style = FindResource("CursorStyleBugSingleIterationView") as Style
                            };
                return cursorVisual;
            }
            else if (model is IssueViewModel)
            {
                ContentControl cursorVisual = new ContentControl()
                {
                    Content = model,
                    Style = FindResource("CursorStyleIssueSingleIterationView") as Style
                };
                return cursorVisual;
            }
            else if (model is TaskViewModel)
            {
                ContentControl cursorVisual = new ContentControl()
                {
                    Content = model,
                    Style = FindResource("CursorStyleTaskSingleIterationView") as Style
                };
                return cursorVisual;
            }
            else if (model is UserStoryViewModel)
            {
                ContentControl cursorVisual = new ContentControl()
                {
                    Content = model,
                    Style = FindResource("CursorStyleUserStorySingleIterationView") as Style
                };
                return cursorVisual;
            }
            else
            {
                throw new TypeLoadException();
            }

        }

        /// <summary>
        /// Handles the cancelling of a drag operations (sets the visibility of the visual back to visible).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragCanceledListbox(object sender, SurfaceDragDropEventArgs e)
        {
            WorkItemViewModel data = e.Cursor.Data as WorkItemViewModel;

            SurfaceListBox listbox = e.Cursor.DragSource as SurfaceListBox;

            DependencyObject depO = listbox.ItemContainerGenerator.ContainerFromItem(data);
            UIElement draggedElement = depO as UIElement;
            draggedElement.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Inidicates that it is allowed to drop the work item at the position of the cursor. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropTargetDragEnter(object sender, SurfaceDragDropEventArgs e)
        {
            e.Cursor.Visual.Tag = "DragEnter";
        }
        /// <summary>
        /// Inidicates that it is not allowed to drop the work item at the position of the cursor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropTargetDragLeave(object sender, SurfaceDragDropEventArgs e)
        {
            e.Cursor.Visual.Tag = null;
        }

        /// <summary>
        /// Handles the event if a work item is dropped on the single iteration view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropWorkItemOnListbox(object sender, SurfaceDragDropEventArgs e)
        {
            SurfaceListBox listbox = null;
            if (e.Cursor.DragSource is ScatterView)
            {
                if (e.Cursor.CurrentTarget is SurfaceListBox)
                {
                    listbox = e.Cursor.CurrentTarget as SurfaceListBox;
                }
                else
                {
                    WorkItemViewModel data = e.Cursor.Data as WorkItemViewModel;
                    ScatterViewItem item = data.DraggedElement as ScatterViewItem;
                    if (item != null)
                    {
                        item.Visibility = Visibility.Visible;
                        item.Orientation = e.Cursor.GetOrientation(this);
                        item.Center = e.Cursor.GetPosition(this);
                    }
                }
                
            }
            else 
            {
                if (e.Cursor.DragSource is SurfaceListBox)
                {
                    listbox = e.Cursor.DragSource as SurfaceListBox;
                }
                else
                {
                    DragCanceledListbox(sender, e);
                }
                
            }

                bool inserted = false;
                for (int index = 0; index < listbox.Items.Count; index++)
                {
                    var model = listbox.Items.GetItemAt(index);
                    DependencyObject depO = listbox.ItemContainerGenerator.ContainerFromItem(model);
                    UIElement el = depO as UIElement;
                    FrameworkElement frEl = el as FrameworkElement;

                    Rect bounds = VisualTreeHelper.GetDescendantBounds(frEl);
                    Point position = e.Cursor.GetPosition((UIElement)frEl);
                    if (frEl != null && bounds.Contains(position))
                    {
                        System.Console.WriteLine((model as WorkItemViewModel).Title);
                        int indexOfUndelyingItem = listbox.Items.IndexOf(model);
                        (this.DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Remove(e.Cursor.Data as WorkItemViewModel);
                        (this.DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Insert(indexOfUndelyingItem, e.Cursor.Data as WorkItemViewModel);
                        inserted = true;
                    }
                }
                if (!inserted)
                {
                    (this.DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Remove(e.Cursor.Data as WorkItemViewModel);
                    (this.DataContext as WorkItemsOfIterationContainerViewModel).WorkItemList.Add(e.Cursor.Data as WorkItemViewModel);
                }

                (this.DataContext as WorkItemsOfIterationContainerViewModel).SetStackRank();

        }

    }
}
