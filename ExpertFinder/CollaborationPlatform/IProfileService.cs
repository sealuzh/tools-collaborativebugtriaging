﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the profiles of the collaboration platorm.
    /// </summary>
    public interface IProfileService
    {
        /// <summary>
        /// Queries all profiles which are associated to the project.
        /// </summary>
        /// <returns></returns>
        ICollection<ProfileModel> GetAllProfiles();
    }
}
