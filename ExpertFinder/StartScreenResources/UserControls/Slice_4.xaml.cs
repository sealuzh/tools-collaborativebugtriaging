﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;

namespace ExpertFinder.StartScreenResources.UserControls
{
    /// <summary>
    /// Interaktionslogik für Slice_4.xaml
    /// </summary>
    /// <remarks>
    /// author: Katja Kevic
    /// date (yyyy mm dd): 2012 08 09
    /// </remarks>
    public partial class Slice_4 : SurfaceUserControl
    {
        public Slice_4()
        {
            InitializeComponent();
        }
    }
}
