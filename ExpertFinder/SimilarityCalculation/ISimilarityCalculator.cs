﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScrumSupporter.Model;
using ScrumSupporter.CollaborationPlatform;

namespace ScrumSupporter
{
    interface ISimilarityCalculator
    {
        /// <summary>
        /// Returns a ordered list (strongest similarity first) of all other work items.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IList <SimilarWorkItemModel> GetSimilarWorkItems(WorkItemModel model, ICollaborationPlatform platform);


    }

}   
