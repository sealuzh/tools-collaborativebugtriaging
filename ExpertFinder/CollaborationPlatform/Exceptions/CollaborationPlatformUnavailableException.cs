﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.CollaborationPlatform.Exceptions
{
    /// <summary>
    /// This exception is thrown when the collaboration platform is not available.
    /// </summary>
    [Serializable]
    public class CollaborationPlatformUnavailableException: ApplicationException
    {
        public CollaborationPlatformUnavailableException() { }
        public CollaborationPlatformUnavailableException(string message): base(message) { }
        public CollaborationPlatformUnavailableException(string message, System.Exception inner):base(message, inner) { }

        // Constructor needed for serialization 
        protected CollaborationPlatformUnavailableException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context): base(info, context) { }
    }
}
