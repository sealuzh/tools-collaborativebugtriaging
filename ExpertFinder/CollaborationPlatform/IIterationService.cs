﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrumSupporter.CollaborationPlatform
{
    /// <summary>
    /// Interface to the iterations of the collaboration platorm.
    /// </summary>
    public interface IIterationService
    {
        /// <summary>
        /// Queries the names of all existing iterations. 
        /// </summary>
        /// <returns></returns>
        ICollection<String> GetAllIterationNames();
    }
}
